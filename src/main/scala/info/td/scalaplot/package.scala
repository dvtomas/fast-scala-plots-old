package info.td

import java.util.Locale

import com.typesafe.scalalogging.LazyLogging
import info.td.common.gettext.PoLoader.PoDomain
import info.td.common.gettext.{Po, PoKey, PoLoader}

package object scalaplot extends LazyLogging {

  class FastScalaPlotsPo(body: Map[PoKey, Array[String]]) extends Po(body, true)

  case class ValueFormatterLocale(locale: Locale)

  implicit val fastScalaPlotsPo = {
    val poWithMessages = PoLoader.load(PoDomain("FastScalaPlotsOld"), autoEscapeSingleQuotes = true)
    poWithMessages.messages foreach (_.log(logger))
    new FastScalaPlotsPo(poWithMessages.value.body)
  }
}
