package info.td

import java.util.Locale

import com.typesafe.scalalogging.LazyLogging
import info.td.common.gettext.{Po, PoKey, PoLoader}

package object scalaplot extends LazyLogging {

  class FastScalaPlotsPo(body: Map[PoKey, Array[String]]) extends Po(body, true)

  case class ValueFormatterLocale(locale: Locale)

  implicit val fastScalaPlotsPo: FastScalaPlotsPo = {
    PoLoader.loadPo("FastScalaPlotsOld", new FastScalaPlotsPo(_), autoEscapeSingleQuotes = true) match {
      case Left(error) => {
        logger.error(error)
        new FastScalaPlotsPo(Map.empty)
      }
      case Right(po) => po
    }
  }
}
