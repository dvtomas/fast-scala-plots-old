package info.td.scalaplot.utils

object MathUtils {
  def decadicExponent(value: Double): Int = Math.floor(Math.log10(value.abs)).toInt

  def minimumWithNaNs(a: Double, b: Double) = if (a.isNaN) b else if (b.isNaN) a else a min b

  def maximumWithNaNs(a: Double, b: Double) = if (a.isNaN) b else if (b.isNaN) a else a max b
}