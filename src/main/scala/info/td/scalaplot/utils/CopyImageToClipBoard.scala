package info.td.scalaplot.utils

import java.awt._
import java.awt.datatransfer._
import java.awt.image.BufferedImage

import com.typesafe.scalalogging.LazyLogging

class CopyImageToClipBoard extends ClipboardOwner with LazyLogging {
  def copyScreenToClipBoard(): Unit = {
    try {
      val robot = new Robot()
      val screenSize = Toolkit.getDefaultToolkit.getScreenSize
      val screen = new Rectangle(screenSize)
      val image = robot.createScreenCapture(screen)
      copyImageToClipBoard(image)
    } catch {
      case exception: AWTException => {
        logger.warn("copyScreenToClipBoard", exception)
      }
    }
  }

  def copyImageToClipBoard(image: Image): Unit = {
    try {
      val trans = new TransferableImage(image)
      val c = Toolkit.getDefaultToolkit.getSystemClipboard
      c.setContents(trans, this)
    } catch {
      case exception: AWTException => {
        logger.warn("copyImageToClipBoard", exception)
      }
    }
  }

  def copyComponentToClipBoard(component: Component, maybeRegion: Option[Rectangle] = None): Unit = {
    val image: BufferedImage = getImageFromComponent(component, maybeRegion)
    copyImageToClipBoard(image)
  }

  def getImageFromComponent(component: Component, maybeRegion: Option[Rectangle] = None): BufferedImage = {
    val wholeImage = new BufferedImage(component.getWidth, component.getHeight, BufferedImage.TYPE_INT_ARGB_PRE)
    val g = wholeImage.getGraphics
    g.setColor(component.getForeground)
    g.setFont(component.getFont)
    component.paintAll(g)
    val image = maybeRegion match {
      case None => wholeImage
      case Some(region) => wholeImage.getSubimage(region.x, region.y, region.width, region.height)
    }
    image
  }

  def lostOwnership(clip: Clipboard, transferable: Transferable): Unit = {
    logger.warn("lostOwnership")
  }

  private class TransferableImage(val image: Image) extends Transferable {
    def getTransferData(flavor: DataFlavor): AnyRef = {
      if (flavor.equals(DataFlavor.imageFlavor) && image != null) {
        image
      } else {
        throw new UnsupportedFlavorException(flavor)
      }
    }

    def getTransferDataFlavors: Array[DataFlavor] = (DataFlavor.imageFlavor :: Nil).toArray

    def isDataFlavorSupported(flavor: DataFlavor): Boolean = {
      getTransferDataFlavors.contains(flavor)
    }
  }

}