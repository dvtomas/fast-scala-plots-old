package info.td.scalaplot.utils

import java.awt.Component
import java.awt.image.BufferedImage
import java.io.{File, FileOutputStream, IOException}

import info.td.scalaplot._
import javax.imageio.ImageIO
import javax.swing.JFileChooser
import javax.swing.filechooser.FileNameExtensionFilter


object FileDialogUtils {
  def saveImageToFile(parent: Component, image: => BufferedImage): Unit = {
    val fileChooser = new JFileChooser()
    val imageExtension = "png"
    val imagesFileFilter = new FileNameExtensionFilter(fastScalaPlotsPo.t("Image files") + " (*." + imageExtension + ")", imageExtension)
    fileChooser.addChoosableFileFilter(imagesFileFilter)
    fileChooser.setFileFilter(imagesFileFilter)
    if (fileChooser.showSaveDialog(parent) == JFileChooser.APPROVE_OPTION) {
      val chosenFile = fileChooser.getSelectedFile
      val file: File = chosenFile.getName.contains('.') match {
        case true => chosenFile
        case false => new File(chosenFile.getPath + "." + imageExtension)
      }
      try {
        // dont use ImageIO.write directly to file, it is buggy.
        // http://stackoverflow.com/questions/12074858/java-imageio-exception-weirdness
        val outputStream = new FileOutputStream(file, false)
        if (!ImageIO.write(image, imageExtension, outputStream)) {
          ErrorDialog.show(parent, fastScalaPlotsPo.t("Error saving image"))
        }
      } catch {
        case exception: IOException => {
          ErrorDialog.show(parent, fastScalaPlotsPo.t("Error saving image"), exception)
        }
      }
    }
  }
}