package info.td.scalaplot.utils

import java.awt.event
import java.beans.PropertyChangeEvent

import javax.swing._
import rx.lang.scala.{Observable, Subject}

object RichPopupMenu {
  def newMenuItem(
    name: String,
    enabled: Boolean,
    action: () => Unit,
  ): JMenuItem = {
    val menuItem = new JMenuItem(name)
    menuItem.addActionListener((_: event.ActionEvent) => action())
    menuItem setEnabled enabled
    menuItem
  }

  def newCheckMenuItem(action: BooleanRichAction): JMenuItem = new JCheckBoxMenuItem(action)
}

case class ActionEvent(sourceAction: RichAction, event: PropertyChangeEvent) {
  def eventSource: AnyRef = event.getSource
}

class RichAction(title: String, action: () => Unit) extends AbstractAction {
  putValue(javax.swing.Action.NAME, title)

  private[this] val actionEventSubject = Subject[ActionEvent]()

  def actionEventsSource: Observable[ActionEvent] = actionEventSubject

  def selected: Boolean = {
    val selectedValue = getValue(javax.swing.Action.SELECTED_KEY)
    selectedValue == true // this is intentional
  }

  def setSelected(b: Boolean): Unit = {
    putValue(javax.swing.Action.SELECTED_KEY, b)
  }

  addPropertyChangeListener((event: PropertyChangeEvent) => actionEventSubject onNext ActionEvent(this, event))

  def actionPerformed(actionEvent: event.ActionEvent): Unit = {
    action()
  }

  def smallIcon: Icon = getValue(javax.swing.Action.SMALL_ICON).asInstanceOf[Icon]

  def setSmallIcon(i: Icon): Unit = {
    putValue(javax.swing.Action.SMALL_ICON, i)
  }

  def largeIcon: Icon = getValue(javax.swing.Action.LARGE_ICON_KEY).asInstanceOf[Icon]

  def setLargeIcon(i: Icon): Unit = {
    putValue(javax.swing.Action.LARGE_ICON_KEY, i)
  }

  /** Also works as a tooltip */
  def description: String = {
    Option(getValue(javax.swing.Action.SHORT_DESCRIPTION))
      .collect { case s: String => s }
      .getOrElse("")
  }

  /** Also works as a tooltip */
  def setDescription(text: String): Unit = {
    putValue(javax.swing.Action.SHORT_DESCRIPTION, text)
  }
}

class BooleanRichAction(title: String) extends RichAction(title, () => {})