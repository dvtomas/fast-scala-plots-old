package info.td.scalaplot.utils

case class DigitChange(value: Double, changedExponent: Int, changedDigit: Option[Int])

class DigitChangeComputer {
  protected def maximumDigitsToCheck = 8

  def digitChange(a: Double, b: Double): DigitChange = {
    def find(bigger: Double, smaller: Double, digitBeingChecked: Int): DigitChange = {
      val result = if (digitBeingChecked >= maximumDigitsToCheck) {
        DigitChange(0.0, digitBeingChecked, None)
      } else {
        val exponent = -digitBeingChecked.toDouble
        val multiplier = Math.pow(10, exponent)
        val firstDigit = Math.floor(bigger / multiplier)
        val remainderComplement = firstDigit * multiplier
        if (smaller > remainderComplement) {
          val remainderChange = find(bigger - remainderComplement, smaller - remainderComplement, digitBeingChecked + 1)
          DigitChange(remainderChange.value + remainderComplement, remainderChange.changedExponent, remainderChange.changedDigit)
        } else {
          DigitChange(remainderComplement, digitBeingChecked, Some(firstDigit.toInt))
        }
      }
      result
    }

    if (a == b)
      DigitChange(a, -maximumDigitsToCheck - 1, None)
    else if ((a >= 0.0 && b <= 0.0) || (a <= 0.0 && b >= 0.0)) {
      DigitChange(0.0, 0, Some(0))
    } else {
      val (bigger, smaller, negated) = if (a > 0) {
        if (a > b) (a, b, false) else (b, a, false)
      } else {
        if (a > b) (-b, -a, true) else (-a, -b, true)
      }
      val absResult = {
        val exponent = Math.floor(Math.log10(bigger)).toInt
        val multiplier = Math.pow(10.0, exponent)
        val normalizedMagnitudeChange = find(bigger / multiplier, smaller / multiplier, 0)
        new DigitChange(normalizedMagnitudeChange.value * multiplier, exponent - normalizedMagnitudeChange.changedExponent, normalizedMagnitudeChange.changedDigit)
      }
      if (negated) {
        DigitChange(-absResult.value, absResult.changedExponent, absResult.changedDigit)
      } else {
        absResult
      }
    }
  }
}