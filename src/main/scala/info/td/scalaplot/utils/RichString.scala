package info.td.scalaplot.utils

import java.awt._

import info.td.scalaplot.{HorizontalAlignment, ScreenPoint, ScreenRectangle, VerticalAlignment}

trait AbstractRichString {
  def string: String

  def properties: FontProperties

  protected def _drawAt(g: Graphics2D, x: Double, y: Double): Unit

  protected def setFontAndGetWidth(g: Graphics2D): Double

  protected def height: Int
}

abstract class BackgroundProperties {
  def draw(g: Graphics2D, rectangle: ScreenRectangle): Unit

  def fillColor: Color
}

final case class TranslucentRectangleBackground(
  fillColor: Color = Color.GRAY,
  fillAlpha: Float = 0.1f,
  borderColor: Color = Color.BLACK,
  borderAlpha: Float = 0.5f) extends BackgroundProperties {
  /** Draws itself so that the it is drawn just exactly inside rectangle, not outlining it on either side. */
  def draw(g: Graphics2D, rectangle: ScreenRectangle): Unit = {
    g.setStroke(new BasicStroke(1))

    Graphics2DUtils.setColorAndAlpha(g, fillColor, fillAlpha)
    g.fill(rectangle.snappedForFillInside)

    Graphics2DUtils.setColorAndAlpha(g, borderColor, borderAlpha)
    g.draw(rectangle.snappedForDrawInside)
  }
}

final case class SolidRectangleBackground(
  fillColor: Color = Color.YELLOW,
  borderColor: Color = Color.GRAY
) extends BackgroundProperties {
  /** Draws itself so that the it is drawn just exactly inside rectangle, not outlining it on either side. */
  def draw(g: Graphics2D, rectangle: ScreenRectangle): Unit = {
    g.setStroke(new BasicStroke(1))

    g.setColor(fillColor)
    g.fill(rectangle.snappedForFillInside)

    g.setColor(borderColor)
    g.draw(rectangle.snappedForDrawInside)
  }
}

case class FontProperties(
  size: Int = 12,
  style: Int = Font.PLAIN,
  name: String = "Sans-Serif",
  color: Color = Color.BLACK
) {
  lazy val font: Font = new Font(name, style, size)
}

class RichString(val string: String, val properties: FontProperties = FontProperties()) extends AbstractRichString {
  private def topAligned(y: Double) = y + height

  private def bottomAligned(y: Double) = y - 3

  private def vCenterAligned(y: Double) = y + (height / 2)

  def height: Int = properties.size

  def setFontAndGetWidth(g: Graphics2D): Double = {
    g.setFont(properties.font)
    g.getFontMetrics.getStringBounds(string, g).getWidth.toInt
  }

  private def setFontAndLeftAligned(g: Graphics2D, x: Double) = {
    g.setFont(properties.font)
    x
  }

  private def setFontAndHCenterAligned(g: Graphics2D, x: Double) = x - (setFontAndGetWidth(g) / 2)

  private def setFontAndRightAligned(g: Graphics2D, x: Double) = x - setFontAndGetWidth(g)

  protected def _drawAt(g: Graphics2D, x: Double, y: Double): Unit = {
    g.setColor(properties.color)
    g.drawString(string, x.toInt, y.toInt)
  }

  def drawAt(
    g: Graphics2D,
    x: Double,
    y: Double,
    horizontalAlignment: HorizontalAlignment,
    verticalAlignment: VerticalAlignment
  ): Unit = {
    if (!string.isEmpty) {
      val alignedX = horizontalAlignment match {
        case HorizontalAlignment.left => setFontAndLeftAligned(g, x)
        case HorizontalAlignment.center => setFontAndHCenterAligned(g, x)
        case HorizontalAlignment.right => setFontAndRightAligned(g, x)
      }
      verticalAlignment match {
        case VerticalAlignment.top => _drawAt(g, alignedX, topAligned(y))
        case VerticalAlignment.center => _drawAt(g, alignedX, vCenterAligned(y))
        case VerticalAlignment.bottom => _drawAt(g, alignedX, bottomAligned(y))
      }
    }
  }

  def drawAt(g: Graphics2D, position: ScreenPoint, horizontalAlignment: HorizontalAlignment, verticalAlignment: VerticalAlignment): Unit = {
    drawAt(g, position.x, position.y, horizontalAlignment, verticalAlignment)
  }

  /** Renders the string roughly at position aligned in such a way, that it does not overlap the position, but is in the quadrant close to a certain lean point. */
  def drawAtAndLeaningTo(g: Graphics2D, position: ScreenPoint, leanTo: ScreenPoint): Unit = {
    val (xAlignment, xOffset) = if (position.x < leanTo.x) (HorizontalAlignment.left, 5) else (HorizontalAlignment.right, -5)
    val (yAlignment, yOffset) = if (position.y < leanTo.y) (VerticalAlignment.top, 5) else (VerticalAlignment.bottom, -5)
    drawAt(g, position.x + xOffset, position.y + yOffset, xAlignment, yAlignment)
  }
}

class RichStringWithBackground(
  string: String,
  val backgroundProperties: BackgroundProperties,
  properties: FontProperties = FontProperties()
) extends RichString(string, properties) {
  override protected def _drawAt(g: Graphics2D, x: Double, y: Double): Unit = {
    val width = setFontAndGetWidth(g)
    backgroundProperties.draw(g, ScreenRectangle(x - 2, y + 2, x + width + 2, y - height))
    super._drawAt(g, x, y)
  }
}

object RichString {

  /** Mixin that renders numbers in scientific notation (1.2E+05) as 1.2x10 5 with exponent in superscript font */
  trait ScientificNotationRichString extends AbstractRichString {

    private case class MantissaAndExponent(mantissa: String, exponent: String)

    private lazy val mantissaAndExponent = string.split('E') match {
      case Array(mantissa) => MantissaAndExponent(mantissa, "")
      case Array(mantissa, exponent) => MantissaAndExponent(if (mantissa == "1") "10" else mantissa + "×10", exponent.toInt.toString /* get rid of leading '+' */)
    }
    private lazy val superscriptFont = {
      val font = properties.font
      val newSize = Math.round(font.getSize * 0.9).toInt
      new Font(font.getName, font.getStyle, newSize)
    }

    private var mantissaWidthCache: (Graphics2D, Int) = (null, 0)

    private def mantissaWidth(g: Graphics2D) = if (mantissaWidthCache._1 == g) {
      mantissaWidthCache._2
    } else {
      g.setFont(properties.font)
      val mantissaWidth = g.getFontMetrics.getStringBounds(mantissaAndExponent.mantissa, g).getWidth.toInt
      mantissaWidthCache = g -> mantissaWidth
      mantissaWidth
    }

    override def setFontAndGetWidth(g: Graphics2D): Double = {
      if (mantissaAndExponent.exponent.isEmpty) {
        mantissaWidth(g)
      } else {
        g.setFont(superscriptFont)
        mantissaWidth(g) + 1 + g.getFontMetrics.getStringBounds(mantissaAndExponent.exponent, g).getWidth.toInt
      }
    }

    override protected def _drawAt(g: Graphics2D, x: Double, y: Double): Unit = {
      g.setColor(properties.color)
      g.drawString(mantissaAndExponent.mantissa, x.toInt, y.toInt)
      g.setFont(superscriptFont)
      g.drawString(
        mantissaAndExponent.exponent,
        x.toInt + mantissaWidth(g) + 1,
        (y - properties.font.getSize + superscriptFont.getSize).toInt
      )
    }
  }

}