package info.td.scalaplot.utils

import info.td.scalaplot._

object ValueFormatter {
  private def lowExponentForScientificNotation = -3

  private def highExponentForScientificNotation = 5

  def valueString(value: Double, digits: Int)(implicit valueFormatterLocale: ValueFormatterLocale): String = {
    if (value == 0.0) {
      "0"
    } else {
      val decadicExponent: Int = MathUtils.decadicExponent(value)

      val format = if (decadicExponent < lowExponentForScientificNotation) {
        s"%.${digits - 1}E"
      } else if (decadicExponent < highExponentForScientificNotation) {
        s"%.${(digits - decadicExponent - 1) max 0}f"
      } else {
        s"%.${digits - 1}E"
      }
      val result = format.formatLocal(valueFormatterLocale.locale, value)
      result
    }
  }

  def valueString(digitChange: DigitChange)(implicit valueFormatterLocale: ValueFormatterLocale): String = valueString(
    digitChange.value,
    MathUtils.decadicExponent(digitChange.value) - digitChange.changedExponent + 1
  )
}