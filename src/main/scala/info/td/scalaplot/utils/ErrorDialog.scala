package info.td.scalaplot.utils

import javax.swing.JOptionPane
import java.awt.Component
import info.td.scalaplot._

object ErrorDialog {
  def show(component: Component, message: String): Unit = {
    JOptionPane.showMessageDialog(
      component,
      message,
      fastScalaPlotsPo.t("error"),
      JOptionPane.ERROR_MESSAGE)
  }

  def show(component: Component, message: String, error: Throwable): Unit = {
    val completeMessageBuilder = new StringBuilder
    completeMessageBuilder ++= message
    var iteratedError = error
    while (iteratedError != null) {
      val errorDescription = iteratedError.getLocalizedMessage
//      val errorDescription = iteratedError.toString
      if (errorDescription != null) {
        completeMessageBuilder += '\n'
        completeMessageBuilder ++= errorDescription
      }
      iteratedError = iteratedError.getCause
    }

    show(component, completeMessageBuilder.toString())
  }
}