package info.td.scalaplot

import java.awt.geom.Rectangle2D

import info.td.scalaplot.utils.Graphics2DHelpers._

case class ScreenRectangle(x1: Double, y1: Double, x2: Double, y2: Double) {
  def toShape = new Rectangle2D.Double(x1, y1, x2, y2)

  private def rectangle2DSnapped(_left: Double, _right: Double, _top: Double, _bottom: Double) = {
    val (left, right, top, bottom) = (_left.snappedToPixelCenter, _right.snappedToPixelCenter, _top.snappedToPixelCenter, _bottom.snappedToPixelCenter)
    new Rectangle2D.Double(left, top, right - left - 1, bottom - top - 1)
  }

  def snappedForDrawInside: Rectangle2D.Double = rectangle2DSnapped(left, right, top, bottom)

  def snappedForDrawOutside: Rectangle2D.Double = rectangle2DSnapped(left - 1, right + 1, top - 1, bottom + 1)

  /** Will just fill rectangle that has had previously a border drawn with snappedForDrawInside */
  def snappedForFillInside: Rectangle2D.Double = {
    val (_left, _right, _top, _bottom) = (left.floor + 1.0, right.floor, top.floor + 1.0, bottom.floor)
    new Rectangle2D.Double(_left, _top, _right - _left - 1, _bottom - _top - 1)
  }

  def left: Double = x1 min x2

  def right: Double = x1 max x2

  def top: Double = y1 min y2

  def bottom: Double = y1 max y2

  def width: Double = right - left

  def height: Double = bottom - top

  def horizontalCenter: Double = left + ((x1 - x2).abs / 2)

  def verticalCenter: Double = top + ((y1 - y2).abs / 2)

  def topLeft: ScreenPoint = new ScreenPoint(left, top)

  def topRight: ScreenPoint = new ScreenPoint(right, top)

  def bottomLeft: ScreenPoint = new ScreenPoint(left, bottom)

  def bottomRight: ScreenPoint = new ScreenPoint(right, bottom)

  def expanded(expandLeft: Double, expandTop: Double, expandRight: Double, expandBottom: Double): ScreenRectangle =
    ScreenRectangle(
      left - expandLeft,
      top - expandTop,
      right + expandRight,
      bottom + expandBottom
    )

  def union(other: ScreenRectangle): ScreenRectangle = {
    ScreenRectangle(
      left min other.left,
      top min other.top,
      right max other.right,
      bottom max other.bottom
    )
  }

  def intersection(other: ScreenRectangle): ScreenRectangle = {
    def intersectionMin(aMin: Double, aMax: Double, bMin: Double, bMax: Double): Double = {
      if (aMax < bMin || bMax < aMin)
        0 // no overlap at all
      else
      aMin max bMin
    }

    def intersectionMax(aMin: Double, aMax: Double, bMin: Double, bMax: Double): Double = {
      if (aMax < bMin || bMax < aMin)
        0 // no overlap at all
      else
      aMax min bMax
    }

    ScreenRectangle(
      intersectionMin(left, right, other.left, other.right),
      intersectionMin(top, bottom, other.top, other.bottom),
      intersectionMax(left, right, other.left, other.right),
      intersectionMax(top, bottom, other.top, other.bottom)
    )
  }

  def expanded(expandHorizontal: Double, expandVertical: Double): ScreenRectangle =
    ScreenRectangle(
      left - expandHorizontal,
      top - expandVertical,
      right + expandHorizontal,
      bottom + expandVertical
    )

  private def splitOffset(index: Int, total: Int, totalLength: Double): Int = {
    (index * totalLength / total).toInt
  }

  def horizontalSplit(index: Int, total: Int, span: Int = 1): ScreenRectangle =
    ScreenRectangle(
      splitOffset(index, total, width),
      top,
      splitOffset(index + span, total, width),
      bottom
    )

  def verticalSplit(index: Int, total: Int, span: Int = 1): ScreenRectangle =
    ScreenRectangle(
      left,
      splitOffset(index, total, height),
      right,
      splitOffset(index + span, total, height)
    )

  def isHorizontalHit(value: Double): Boolean = value >= left && value <= right

  def isVerticalHit(value: Double): Boolean = value >= top && value <= bottom

  def isHit(point: ScreenPoint): Boolean = isHorizontalHit(point.x) && isVerticalHit(point.y)

  def hasSameDimensionsAs(other: ScreenRectangle): Boolean =
    left == other.left && right == other.right && top == other.top && bottom == other.bottom

  def clippedLine(point1: ScreenPoint, point2: ScreenPoint): Option[(ScreenPoint, ScreenPoint)] = {

    // Thank you so much, http://www.skytopia.com/project/articles/compsci/clipping.html
    def liangBarsky(
      edgeLeft: Double, edgeRight: Double, edgeBottom: Double, edgeTop: Double,
      x0src: Double, y0src: Double, x1src: Double, y1src: Double): Option[(ScreenPoint, ScreenPoint)] = {
      var t0 = 0.0
      var t1 = 1.0
      val xDelta = x1src - x0src
      val yDelta = y1src - y0src
      var p: Double = 0.0
      var q: Double = 0.0
      var r: Double = 0.0

      (0 until 4) foreach { edge =>
        // Traverse through left, right, bottom, top edges.
        if (edge == 0) {
          p = -xDelta
          q = -(edgeLeft - x0src)
        }
        if (edge == 1) {
          p = xDelta
          q = edgeRight - x0src
        }
        if (edge == 2) {
          p = -yDelta
          q = -(edgeBottom - y0src)
        }
        if (edge == 3) {
          p = yDelta
          q = edgeTop - y0src
        }
        r = q / p
        if (p == 0 && q < 0) return None; // Don't draw line at all. (parallel line outside)

        if (p < 0) {
          if (r > t1) return None; // Don't draw line at all.
          else if (r > t0) t0 = r // Line is clipped!
        } else if (p > 0) {
          if (r < t0) return None; // Don't draw line at all.
          else if (r < t1) t1 = r // Line is clipped!
        }
      }

      val x0clip = x0src + t0 * xDelta
      val y0clip = y0src + t0 * yDelta
      val x1clip = x0src + t1 * xDelta
      val y1clip = y0src + t1 * yDelta
      Some((ScreenPoint(x0clip, y0clip), ScreenPoint(x1clip, y1clip)))
    }

    liangBarsky(left, right, top, bottom, point1.x, point1.y, point2.x, point2.y)
  }

  override def toString = s"${this.getClass.getSimpleName}($left, $top, $right, $bottom)"
}

object ScreenRectangle {
  val zero: ScreenRectangle = ScreenRectangle(0, 0, 0, 0)

  def apply(point1: ScreenPoint, point2: ScreenPoint): ScreenRectangle = ScreenRectangle(point1.x, point1.y, point2.x, point2.y)
}