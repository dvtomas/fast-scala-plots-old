package info.td.scalaplot

import info.td.scalaplot.plotparts.PlotPart

import java.awt.Color

case class PickInfo(plotPart: PlotPart, position: ScreenPoint, dataPoint: DataPoint, xValueString: String, yValueString: String, color: Color)