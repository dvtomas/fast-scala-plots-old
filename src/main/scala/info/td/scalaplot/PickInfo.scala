package info.td.scalaplot

import java.awt.Color

import info.td.scalaplot.plotparts.PlotPart

case class PickInfo(plotPart: PlotPart, position: ScreenPoint, dataPoint: DataPoint, xValueString: String, yValueString: String, color: Color)