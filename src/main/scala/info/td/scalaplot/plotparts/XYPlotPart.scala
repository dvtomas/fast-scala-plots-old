package info.td.scalaplot.plotparts

import java.awt.Graphics2D

import info.td.scalaplot._
import info.td.scalaplot.utils._

import scala.Ordering.Double.IeeeOrdering

abstract class XYPlotPartDataProvider extends PlotPartDataProvider {
  def numberOfPoints: Int

  def xValue(index: Int): Double

  def yValue(index: Int): Double

  def iterateValues: Iterator[DataPoint] = new Iterator[DataPoint] {
    private var index: Int = 0

    def hasNext: Boolean = index < numberOfPoints

    def next(): DataPoint = {
      val oldIndex = index
      index += 1
      point(oldIndex)
    }
  }

  def point(index: Int): DataPoint = DataPoint(xValue(index), yValue(index))

  private def dataBounds(retrieveValueByIndexFunc: Int => Double, filterByIndexFunc: Int => Boolean): DataRange = {
    if (numberOfPoints == 0) {
      DataRange.invalid
    } else {
      var minimum: Double = Double.NaN
      var maximum: Double = Double.NaN
      for (index <- 0 until numberOfPoints if filterByIndexFunc(index)) {
        val y = retrieveValueByIndexFunc(index)
        if (!y.isNaN && !y.isInfinity) {
          if (minimum.isNaN)
            minimum = y
          else
            minimum = minimum min y

          if (maximum.isNaN)
            maximum = y
          else
            maximum = maximum max y
        }
      }
      DataRange(minimum, maximum)
    }
  }

  def dataBoundsX: DataRange = dataBounds(index => xValue(index), _ => true)

  def dataBoundsYAt(range: DataRange): DataRange = dataBounds(index => yValue(index), index => range contains xValue(index))
}

class XYPlotPart(val dataProvider: XYPlotPartDataProvider, plot: Plot) extends LinePlotPart(plot) {
  def paintPlotPart(g: Graphics2D): Unit = {
    Graphics2DUtils.withClipDo(g, plot.screenDataBounds) {
      val linePlotter = new ContinuousLinePlotter(g, plot, linePlottingProperties, Some(plot.screenDataBounds))
      for (point <- dataProvider.iterateValues) {
        linePlotter.addPoint(point)
      }
    }
  }

  /** First tries to find closest point on the same pixel column, then looks in the neighbourhood */
  protected[this] def pickVertical(where: ScreenPoint)(implicit valueFormatterLocale: ValueFormatterLocale): Option[PickInfo] = {
    def horizontalDistance(point: DataPoint) = (plot.screenPoint(point).x - where.x).abs

    def verticalDistance(point: DataPoint) = (plot.screenPoint(point).y - where.y).abs

    val yDataRange = plot.axisY.dataRange
    val pointsToExamine = dataProvider.iterateValues.filter(point => yDataRange.contains(point.y))
    val candidatePoints = pointsToExamine.filter(horizontalDistance(_) < 10).toList

    if (candidatePoints.isEmpty) {
      None
    } else {
      val pointsOnTheSameVertical = candidatePoints.filter(horizontalDistance(_) <= 1)
      val closestDataPoint = if (pointsOnTheSameVertical.isEmpty) {
        candidatePoints minBy horizontalDistance
      } else {
        pointsOnTheSameVertical minBy verticalDistance
      }
      Some(pickInfoFor(closestDataPoint))
    }
  }

  /** Picks the closest point to the picker.
    * It is visually best to use this with the XY cross-hair.
    */
  protected[this] def pickClosest(where: ScreenPoint)(implicit valueFormatterLocale: ValueFormatterLocale): Option[PickInfo] = {
    def square(x: Double): Double = x * x

    def squaredDistance(point: DataPoint) = square((plot.screenPoint(point).x - where.x).abs) + square((plot.screenPoint(point).y - where.y).abs)

    val xDataRange = plot.axisX.dataRange
    val yDataRange = plot.axisY.dataRange
    val pointsToExamine = dataProvider.iterateValues.filter(point => (xDataRange contains point.x) && (yDataRange contains point.y))
    if (pointsToExamine.isEmpty)
      None
    else {
      val closestPoint = pointsToExamine minBy squaredDistance
      val distance = Math.sqrt(squaredDistance(closestPoint))
      if (distance < 20)
        Some(pickInfoFor(closestPoint))
      else
        None
    }
  }

  override def pick(where: ScreenPoint)(implicit valueFormatterLocale: ValueFormatterLocale): Option[PickInfo] = pickClosest(where)
}