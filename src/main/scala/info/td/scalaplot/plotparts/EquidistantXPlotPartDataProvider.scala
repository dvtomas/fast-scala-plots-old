package info.td.scalaplot.plotparts

import info.td.scalaplot.{Axis, DataRange}

object EquidistantRange {

  /**
    *
    * @param pixel       pixel at screen, pixel + 0.5 is the center of the pixel
    * @param dataIndices Indices of the data provider that fall in that pixel
    */
  case class DataIndicesAtPixel(pixel: Int, dataIndices: Range)

}

case class EquidistantRange(start: Double, numberOfPoints: Int, step: Double) {

  import info.td.scalaplot.plotparts.EquidistantRange._

  final def value(index: Int): Double = start + step * index

  private def uncheckedIndexOfPointLeftOf(x: Double): Int = ((x - start) / step).toInt

  /** Returns a range of indices of points that are contained in range, and also one point on each side before the start and after the end of the range. If the provider
    * doesn't have enough points, the result will only contain indices of valid points, even though they may not cover the whole requested range. */
  def indicesOfPointsJustAround(range: DataRange): Range = {
    if (numberOfPoints == 0) {
      0 until 0
    } else {
      val index1 = uncheckedIndexOfPointLeftOf(range.minimum)
      val index2 = uncheckedIndexOfPointLeftOf(range.maximum)
      if (index1 < index2) {
        (index1 max 0) to ((index2 + 1) min (numberOfPoints - 1))
      } else {
        (index2 max 0) to ((index1 + 1) min (numberOfPoints - 1))
      }
    }
  }

  /** Just like `indicesOfPointsJustAround`, but doesn't care if the points are contained in the range */
  def uncheckedIndicesOfPointsJustAround(range: DataRange): Range = {
    val index1 = uncheckedIndexOfPointLeftOf(range.minimum)
    val index2 = uncheckedIndexOfPointLeftOf(range.maximum)
    index1 to index2
  }

  def iterateIndices(indices: Range): Iterator[Int] =
    if (indices.isEmpty) {
      Iterator.empty
    } else {
      new Iterator[Int] {
        private var index: Int = indices.min

        def hasNext: Boolean = indices.contains(index)

        def next(): Int = {
          val oldIndex = index
          index += 1
          oldIndex
        }
      }
    }

  def dataBounds: DataRange = if (numberOfPoints == 0) {
    DataRange.invalid
  } else {
    DataRange(start, start + step * (numberOfPoints - 1))
  }

  private def findDataIndicesRangeInPixelAscending(axis: Axis, pixel: Int): Option[Range] = {
    if (axis.screenValue(value(numberOfPoints - 1)) <= pixel)
      return None
    if (axis.screenValue(value(0)) >= pixel + 1)
      return None
    // Now we know that at least one point is included in this pixel. That means we'll be returning a Some.
    val dataLow = axis.dataValue(pixel)
    val dataHigh = axis.dataValue(pixel + 1)
    val potentialLowIndex = uncheckedIndexOfPointLeftOf(dataLow) max 0
    val lowIndex = if (value(potentialLowIndex) < dataLow)
      potentialLowIndex + 1
    else
      potentialLowIndex
    val highIndex = uncheckedIndexOfPointLeftOf(dataHigh) min (numberOfPoints - 1)
    Some(lowIndex to highIndex)
  }

  private def findDataIndicesRangeInPixelDescending(axis: Axis, pixel: Int): Option[Range] = {
    if (axis.screenValue(value(numberOfPoints - 1)) >= pixel + 1)
      return None
    if (axis.screenValue(value(0)) <= pixel)
      return None
    // Now we know that at least one point is included in this pixel. That means we'll be returning a Some.
    val dataLow = axis.dataValue(pixel + 1)
    val dataHigh = axis.dataValue(pixel)
    val potentialLowIndex = uncheckedIndexOfPointLeftOf(dataLow) max 0
    val lowIndex = if (value(potentialLowIndex) < dataLow)
      potentialLowIndex + 1
    else
      potentialLowIndex
    val highIndex = uncheckedIndexOfPointLeftOf(dataHigh) min (numberOfPoints - 1)
    Some(lowIndex to highIndex)
  }

  def findDataIndicesPackedByPixels(axis: Axis): Seq[DataIndicesAtPixel] = {
    if (numberOfPoints == 0) {
      Seq.empty
    } else if (numberOfPoints == 1) {
      Seq(DataIndicesAtPixel(axis.screenValue(value(0)).toInt, 0 to 0))
    } else {
      val wholePixelsRange = if (axis.screenRange.minimum < axis.screenRange.maximum)
        axis.screenRange.minimum.round.toInt until axis.screenRange.maximum.round.toInt
      else
        axis.screenRange.maximum.round.toInt until axis.screenRange.minimum.round.toInt

      if (axis.screenValue(value(1)) > axis.screenValue(value(0))) {
        for (
          pixel <- wholePixelsRange;
            dataIndices <- findDataIndicesRangeInPixelAscending(axis, pixel)
        ) yield DataIndicesAtPixel(pixel, dataIndices)
      } else {
        for (
          pixel <- wholePixelsRange.reverse;
            dataIndices <- findDataIndicesRangeInPixelDescending(axis, pixel)
        ) yield DataIndicesAtPixel(pixel, dataIndices)
      }
    }
  }
}

abstract class EquidistantXPlotPartDataProvider[A] extends PlotPartDataProvider {
  def xRange: EquidistantRange

  def dataBoundsX: DataRange = xRange.dataBounds

  def yValue(index: Int): A
}