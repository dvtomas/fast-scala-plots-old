package info.td.scalaplot.plotparts

import java.awt.Graphics2D

import info.td.scalaplot._
import info.td.scalaplot.utils._
import rx.lang.scala.Observable

trait PlotPartDataProvider {
  def dataBoundsX: DataRange

  def dataBoundsYAt(range: DataRange): DataRange

  def dataBoundsY: DataRange = {
    val boundsX = dataBoundsX
    dataBoundsYAt(DataRange(boundsX.minimum, boundsX.maximum))
  }

  // Should never be Observable.empty or Observable.never, at least Observable.just(())
  def changedSource: Observable[Unit]
}

case class PlotPartChanged(plotPart: PlotPart)


trait PlotPartTrait {
  def dataProvider: PlotPartDataProvider

  def plotTitle: RichString

  def paintPlotPart(g: Graphics2D): Unit

  def paintComponent(g: Graphics2D): Unit

  /** This method is supposed to contribute to drawing to the legend. It should
    * draw e. g. a sample of a line and a description text.
    *
    * @param g              graphics context to draw on
    * @param referencePoint a reference point on the GC to draw at, as of now the top left point where the legend should start
    * @return a point that along with referencePoint forms a rectangle enclosing what has been drawn.
    */
  def plotLegendPart(g: Graphics2D, referencePoint: ScreenPoint): ScreenPoint

  def pick(where: ScreenPoint)(implicit valueFormatterLocale: ValueFormatterLocale): Option[PickInfo]

  def changedSource: Observable[PlotPartChanged]
}

abstract class PlotPart extends PlotPartTrait {
  def dataProvider: PlotPartDataProvider

  def changedSource: Observable[PlotPartChanged] = dataProvider.changedSource
    .map(_ => PlotPartChanged(this))
    .onBackpressureDrop // this is necessary here. If you want to see an error without it, remove it, run demo, select  Changing data tab and lower number of points to minimum, so the refresh rate is really fast. Changed events are emitted really fast, and (probably, I'm not sure) the flatten operator (merge at the end of the day) throws a MissingBackpressureException from it's internal RxRingBuffer, at least as of RxScala 0.22.0

  def plotTitle: RichString = new RichString("")

  def strValue(value: Double)(implicit valueFormatterLocale: ValueFormatterLocale): String = ValueFormatter.valueString(value, 5)

  /** This method is supposed to return a PickInfo about a given point, when user
    * requests a pick info at screen coordinates where. When no data are present at reasonable distance, return None.
    *
    * @param where Screen coordinates where user wants the pick info.
    * @return optionally info about nearest picked point.
    */
  def pick(where: ScreenPoint)(implicit valueFormatterLocale: ValueFormatterLocale): Option[PickInfo] = None

  def paintComponent(g: Graphics2D): Unit = {
    paintPlotPart(g)
  }
}

abstract class LinePlotPart(plot: Plot) extends PlotPart {
  def linePlottingProperties: LinePlottingProperties = LinePlottingProperties()

  protected def pickInfoFor(point: DataPoint)(implicit valueFormatterLocale: ValueFormatterLocale): PickInfo = {
    def valueString(axis: Axis, screenValue: Double) = {
      val digitChange = axis digitChange screenValue
      // Display with precision 2 digits more than the precision of picker
      ValueFormatter.valueString(digitChange.copy(changedExponent = digitChange.changedExponent - 2))
    }

    PickInfo(
      this, plot.screenPoint(point), point,
      valueString(plot.axisX, plot.screenPoint(point).x),
      valueString(plot.axisY, plot.screenPoint(point).y),
      linePlottingProperties.lineStyle.color
    )
  }

  def plotLegendPart(g: Graphics2D, referencePoint: ScreenPoint): ScreenPoint = {
    if (plotTitle.string.isEmpty) {
      referencePoint
    } else {
      val x = referencePoint.x + 2
      val lineY = referencePoint.y + (plotTitle.properties.size / 2) + 2
      val plotter = new ContinuousLinePlotter(g, plot, linePlottingProperties, None)
      linePlottingProperties.lineStyle match {
        case LineStyleNone => plotter.addScreenPoint(new ScreenPoint(x + 10, lineY))
        case _ => {
          plotter.addScreenPoint(new ScreenPoint(x, lineY))
          plotter.addScreenPoint(new ScreenPoint(x + 10, lineY))
          plotter.addScreenPoint(new ScreenPoint(x + 19, lineY))
        }
      }

      val titleOffset = 23
      plotTitle.drawAt(g, x + titleOffset, referencePoint.y, HorizontalAlignment.left, VerticalAlignment.top)
      val result = new ScreenPoint(referencePoint.x + titleOffset + plotTitle.setFontAndGetWidth(g) + 1, referencePoint.y + plotTitle.properties.size + 2)
      result
    }
  }
}
