package info.td.scalaplot.plotparts

import java.awt.Graphics2D

import info.td.scalaplot._
import info.td.scalaplot.utils.Graphics2DHelpers._
import info.td.scalaplot.utils._

import scala.Ordering.Double.IeeeOrdering

abstract class SnakePlotPartDataProvider extends EquidistantXPlotPartDataProvider[Double] {
  def yValue(index: Int): Double

  def dataBoundsYAt(dataRange: DataRange): DataRange = {
    if (xRange.numberOfPoints == 0) {
      DataRange.invalid
    } else {
      var minimum: Double = Double.NaN
      var maximum: Double = Double.NaN
      for (index <- xRange.indicesOfPointsJustAround(dataRange)) {
        val y = yValue(index)
        if (!y.isNaN && !y.isInfinity) {
          if (minimum.isNaN)
            minimum = y
          else
            minimum = minimum min y

          if (maximum.isNaN)
            maximum = y
          else
            maximum = maximum max y
        }
      }
      DataRange(minimum, maximum)
    }
  }

  def iteratePointsWithIndices(indices: Range): Iterator[DataPoint] = xRange.iterateIndices(indices)
    .map(index => DataPoint(xRange.value(index), yValue(index)))
}

/**
  * We can optimize the rendering of the snakes that are zoomed out so more than one data point is displayed on a single vertical point a lot by computing a list of vertical lines to be drawn at each X pixel position. Each vertical line only has a X position, and a minimum and maximum Y value for that X position.
  *
  * However, things are not so simple. It is more often than not the case that neighbouring vertical lines do not overlap, but we don't actually want to see white space between them when they are logically connected. The Vertical lines renderer thus must take care of connecting the neighbouring vertical lines with another lines. Thus, this optimisation starts to pay off after the data density is at least two data points in a single pixel.
  *
  * Another thing we have to care about are the NaN values. That means that sometimes we have to put several vertical lines in the same X coordinate because they are disjointed by NaNs, and sometimes even non-overlapping neighbouring vertical lines are not supposed to be connected, because there may have been a NaN between. We handle the first case by having more MaybeConnectedLine-s with the same x coordinate, and the second case by setting the isConnectedToPreviousLine flag in the MaybeConnectedLine to false.
  **/


/** All coordinates are already in screen coordinate system */
private sealed abstract class MaybeConnectedLineInProgress

private case object WaitingForLineToConnect extends MaybeConnectedLineInProgress

private case object WaitingForDisjointLine extends MaybeConnectedLineInProgress

private final class MaybeConnectedLine(val x: Double, y: Double, val isConnectedToPreviousLine: Boolean) extends MaybeConnectedLineInProgress {
  var minimumY: Double = y
  var maximumY: Double = y

  def extendWithPoint(y: Double): Unit = {
    if (y < minimumY)
      minimumY = y
    if (y > maximumY)
      maximumY = y
  }

  override def toString = s"MaybeConnectedLine ($x, $minimumY - $maximumY)"
}

private class ConnectedLinesComputer {
  def computeCompressedLines(
    xAxis: Axis,
    yAxisTransformation: AxisTransformation,
    dataProvider: SnakePlotPartDataProvider
  ): Seq[MaybeConnectedLine] = {
    val result = collection.mutable.Buffer[MaybeConnectedLine]()
    var verticalLineInProgress: MaybeConnectedLineInProgress = WaitingForDisjointLine

    def maybeAddCurrentLineToResult(): Unit = {
      verticalLineInProgress match {
        case line: MaybeConnectedLine => result += line
        case _ =>
      }
    }

    val dataIndicesPackedByPixels = dataProvider.xRange.findDataIndicesPackedByPixels(xAxis)
    for (dataIndicesAtPixel <- dataIndicesPackedByPixels) {
      val currentPixel = dataIndicesAtPixel.pixel
      for (dataIndex <- dataIndicesAtPixel.dataIndices) {
        val yValue = dataProvider.yValue(dataIndex)
        if (yValue.isNaN) {
          maybeAddCurrentLineToResult()
          verticalLineInProgress = WaitingForDisjointLine
        } else {
          verticalLineInProgress match {
            case line: MaybeConnectedLine => line.extendWithPoint(yAxisTransformation.screenValue(yValue))
            case WaitingForLineToConnect => verticalLineInProgress = new MaybeConnectedLine(Graphics2DPixelHelper(currentPixel).snappedToPixelCenter, yAxisTransformation.screenValue(yValue), true)
            case WaitingForDisjointLine => verticalLineInProgress = new MaybeConnectedLine(Graphics2DPixelHelper(currentPixel).snappedToPixelCenter, yAxisTransformation.screenValue(yValue), false)
          }
        }
      }
      verticalLineInProgress match {
        case line: MaybeConnectedLine => {
          result += line
          verticalLineInProgress = WaitingForLineToConnect
        }
        case _ =>
      }
    }
    maybeAddCurrentLineToResult()
    result.toSeq
  }
}

private class ConnectedLinesPlotter(g: Graphics2D, linePlottingProperties: LinePlottingProperties) {

  def plot(lines: Seq[MaybeConnectedLine]): Unit = {
    var lastLine = new MaybeConnectedLine(0, 0, false)
    for (line <- lines) {
      if (line.isConnectedToPreviousLine) {
        if (lastLine.maximumY < line.minimumY) {
          linePlottingProperties.lineStyle.drawLine(g, ScreenPoint(lastLine.x, lastLine.maximumY), ScreenPoint(line.x, line.minimumY))
        } else if (lastLine.minimumY > line.maximumY) {
          linePlottingProperties.lineStyle.drawLine(g, ScreenPoint(lastLine.x, lastLine.minimumY), ScreenPoint(line.x, line.maximumY))
        }
      }
      val (minimumY, maximumY) = if (line.maximumY - line.minimumY < 0.7) {
        val spaceToAddOnEachSide = (1.0 - line.maximumY + line.minimumY) / 2.0
        (line.minimumY - spaceToAddOnEachSide, line.maximumY + spaceToAddOnEachSide)
      } else {
        (line.minimumY, line.maximumY)
      }
      linePlottingProperties.lineStyle.drawLine(g, ScreenPoint(line.x, minimumY), ScreenPoint(line.x, maximumY))
      lastLine = line
    }
  }
}

class SnakePlotPart(val dataProvider: SnakePlotPartDataProvider, plot: Plot) extends LinePlotPart(plot) {
  def iterateVisiblePoints: Iterator[DataPoint] = dataProvider.iteratePointsWithIndices(dataProvider.xRange.indicesOfPointsJustAround(plot.axisX.dataRange))

  def paintPlotPart(g: Graphics2D): Unit = {
    Graphics2DUtils.withClipDo(g, plot.screenDataBounds) {
      val dataPointsDensity = (plot.axisX.dataRange.length / plot.axisX.screenRange.length) / dataProvider.xRange.step
      if (dataPointsDensity > 2.0) {
        val connectedLines = new ConnectedLinesComputer().computeCompressedLines(
          plot.axisX,
          plot.axisY.transformation,
          dataProvider
        )
        new ConnectedLinesPlotter(g, linePlottingProperties).plot(connectedLines)
      } else {
        val linePlotter = new ContinuousLinePlotter(g, plot, linePlottingProperties, None)
        iterateVisiblePoints foreach linePlotter.addPoint
      }
    }
  }

  /** Picks closest point on the same pixel column. If no point is present at that column, look at nearby columns.
    *
    * It is visually best to use this with the vertical picking line.
    */
  override def pick(where: ScreenPoint)(implicit valueFormatterLocale: ValueFormatterLocale): Option[PickInfo] = {
    def horizontalDistance(point: DataPoint) = (plot.screenPoint(point).x - where.x).abs

    def verticalDistance(point: DataPoint) = (plot.screenPoint(point).y - where.y).abs

    val yDataRange = plot.axisY.dataRange

    def pointsWithinDistance(distance: Int) = {
      val dataRangeWithinDistance = DataRange(plot.axisX.dataValue(where.x - distance), plot.axisX.dataValue(where.x + distance))
      dataProvider
        .iteratePointsWithIndices(dataProvider.xRange.indicesOfPointsJustAround(dataRangeWithinDistance))
        .filter(point => yDataRange.contains(point.y))
    }

    // traversable once -> must be def, not val
    val candidatePoints = {
      val closeCandidatePoints = pointsWithinDistance(1)
      if (closeCandidatePoints.nonEmpty) {
        closeCandidatePoints
      } else {
        pointsWithinDistance(10)
      }
    }.toList

    if (candidatePoints.isEmpty) {
      None
    } else {
      val pointsOnTheSameVertical = candidatePoints.filter(horizontalDistance(_) <= 1)
      val closestDataPoint = if (pointsOnTheSameVertical.isEmpty) {
        candidatePoints minBy horizontalDistance
      } else {
        pointsOnTheSameVertical minBy verticalDistance
      }
      Some(pickInfoFor(closestDataPoint))
    }
  }
}