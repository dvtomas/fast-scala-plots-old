package info.td.scalaplot.plotparts

import java.awt.{Color, Dimension}

import info.td.scalaplot.axispainter.{AxisXPainter, NullAxisXPainter}
import info.td.scalaplot.figure.Figure
import info.td.scalaplot.plotcontroller.NullPlotController
import info.td.scalaplot.{DataRange, Plot, ScreenRectangle, ValueFormatterLocale}
import javax.swing.{JLabel, JPanel}
import net.miginfocom.swing.MigLayout
import rx.lang.scala.Observable

object HeightMapPalette {
  def legendPlotFor(
    screenPlotBoundsSource: Observable[ScreenRectangle],
    palette: LinearPalette,
    screenDataBoundsSourceMapFunc: Observable[ScreenRectangle] => Observable[ScreenRectangle]
  ): Plot = {
    object dataProvider extends HeightMapDataProvider[Int] {
      private[this] val numberOfRows = 30

      val xRange = new EquidistantRange(0.0, 3, 1.0)

      private val step = palette.range.length / numberOfRows
      val yRange = new EquidistantRange(palette.range.minimum, numberOfRows + 2, step)

      def yValue(xIndex: Int): HeightMapColumn[Int] = HeightMapColumn[Int](0, (yIndex: Int) => palette.range.minimum + step * yIndex)

      def changedSource: Observable[Unit] = Observable.just(())
    }

    new Plot(screenPlotBoundsSource) {
      val plotPart = new HeightMapPlotPart(dataProvider, this, palette, CommonAggregateFunctions.takeFirst)
      addPlotPart(plotPart)

      override protected def createAxisXPainter: AxisXPainter = NullAxisXPainter

      override def screenDataBoundsSource: Observable[ScreenRectangle] = screenDataBoundsSourceMapFunc(screenPlotBoundsSource)
    }
  }

  def verticalLegendPanelFor(
    palette: HeightMapPalette,
    screenDataBoundsSourceMapFunc: Observable[ScreenRectangle] => Observable[ScreenRectangle] = _.map(_.expanded(-35, -20, -15, -50))
  )(implicit valueFormatterLocale: ValueFormatterLocale): JPanel = {
    val panel = new JPanel(new MigLayout("fill, insets 0", "[fill]", "[fill]"))

    palette match {
      case linearPalette: LinearPalette => {
        object figure extends Figure() {
          private[this] val plot = legendPlotFor(screenBoundsSource, linearPalette, screenDataBoundsSourceMapFunc)
          private[this] val controller = new NullPlotController
          addPlot(plot, controller)
          plot.axisX.subscribeToDataRangeSource(Observable.just(DataRange(0, 1)))
          plot.axisY.subscribeToDataRangeSource(Observable.just(linearPalette.range))
        }
        figure.setMinimumSize(new Dimension(5, 20))
        panel.add(figure)
      }
      case other => panel.add(new JLabel(s"Don't know how to make a legend of $other"))
    }
    panel
  }
}

abstract class HeightMapPalette {
  /** Should return an int suitable for BufferedImage.setRGB.
    *
    * @param value Value to provide the color for
    * @return int such that it holds: <pre>
    *         int alpha = (rgb >> 24) & 0xFF; <br/>
    *         int red =   (rgb >> 16) & 0xFF; <br/>
    *         int green = (rgb >>  8) & 0xFF; <br/>
    *         int blue =  (rgb      ) & 0xFF;
    *         </pre>
    *         Alpha channel is not used in the current implementation, though.
    */
  def apply(value: Double): Int

  protected def intColor(color: Color): Int = color.getRed << 16 | color.getGreen << 8 | color.getBlue
}

/**
  * A palette consisting of segments of linearly interpolated colors
  *
  * @param sortedColors      - sequence of (value, color) tuples sorted by ascending values.
  * @param belowMinimumColor color to use when the value is below the lowest value in sorted colors
  * @param aboveMaximumColor color to use when the value is above the highest value in sorted colors
  * @param naNColor          color to use for a NaN value
  */
case class LinearPalette(
  sortedColors: Seq[(Double, Color)],
  belowMinimumColor: Color,
  aboveMaximumColor: Color,
  naNColor: Color
) extends HeightMapPalette {
  private val totalMinimum = sortedColors.headOption.map(_._1).getOrElse(Double.NaN)
  private val totalMaximum = sortedColors.lastOption.map(_._1).getOrElse(Double.NaN)
  private val minimumIntColor = intColor(belowMinimumColor)
  private val maximumIntColor = intColor(aboveMaximumColor)
  private val naNIntColor = intColor(naNColor)

  private val lookupTable = new Array[Int](10001) // we need the lookup to be fast, so the size of the lookup table is fixed

  private val k = 1000.0 / (totalMaximum - totalMinimum)

  for (i <- 0 to 1000) {
    lookupTable(i) = color(totalMinimum + (i.toDouble / k))
  }

  private[this] def color(value: Double): Int = {
    if (value.isNaN) {
      naNIntColor
    } else if (value <= totalMinimum)
      minimumIntColor
    else if (value >= totalMaximum)
      maximumIntColor
    else {
      sortedColors
        .sliding(2)
        .collect {
          case Seq((minimum, minimumColor), (maximum, maximumColor)) if value >= minimum && value <= maximum => {
            val range = DataRange(minimum, maximum)
            intColor(new Color(
              range.translatePositionToPositionOn(value, DataRange(minimumColor.getRed, maximumColor.getRed)).toInt,
              range.translatePositionToPositionOn(value, DataRange(minimumColor.getGreen, maximumColor.getGreen)).toInt,
              range.translatePositionToPositionOn(value, DataRange(minimumColor.getBlue, maximumColor.getBlue)).toInt
            ))
          }
        }.toList.headOption.getOrElse(naNIntColor)
    }
  }

  def apply(value: Double): Int = {
    if (value.isNaN) {
      naNIntColor
    } else if (value < totalMinimum) {
      minimumIntColor
    } else if (value > totalMaximum) {
      maximumIntColor
    } else
      lookupTable(((value - totalMinimum) * k).toInt)
  }

  def range: DataRange = DataRange(totalMinimum, totalMaximum)
}