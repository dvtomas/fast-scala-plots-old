package info.td.scalaplot.plotparts

import java.awt.geom.Line2D
import java.awt.{BasicStroke, Color, Graphics2D}

import info.td.scalaplot._
import info.td.scalaplot.utils.Graphics2DHelpers._
import info.td.scalaplot.utils.Graphics2DUtils

private class VerticalLinesPlotter(g: Graphics2D, screenZeroY: Double, linePlottingProperties: LinePlottingProperties) {
  def plot(lines: Seq[MaybeConnectedLine]): Unit = {
    g.setColor(linePlottingProperties.lineStyle.color)
    g.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER))
    for (line <- lines) {
      val (minimumY: Double, maximumY: Double) = (line.minimumY >= screenZeroY, line.maximumY >= screenZeroY, line.maximumY > line.minimumY) match {
        case (true, true, true) => (screenZeroY, line.maximumY)
        case (true, true, false) => (screenZeroY, line.minimumY)
        case (true, false, _) => (line.maximumY, line.minimumY)
        case (false, true, _) => (line.minimumY, line.maximumY)
        case (false, false, true) => (line.minimumY, screenZeroY)
        case (false, false, false) => (line.maximumY, screenZeroY)
      }
      val x = line.x.snappedToPixelCenter
      g.draw(new Line2D.Double(x, minimumY, x, maximumY))
    }
  }
}

/** Line style drawing not lines, but filling area between the line and the zero axis */
private class VerticalLinesLineStyle(originalLineStyle: LineStyle, screenZeroY: Double) extends LineStyle {
  def color: Color = originalLineStyle.color

  val stroke = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER)

  def drawLine(g: Graphics2D, point1: ScreenPoint, point2: ScreenPoint): Unit = {
    g.setColor(color)
    g.setStroke(stroke)

    val (minXPoint, maxXPoint) = if (point1.x < point2.x) (point1, point2) else (point2, point1)
    val ε = 0.01
    val (snappedMinX, snappedMaxX) = ((minXPoint.x - ε).snappedToPixelCenter, (maxXPoint.x + ε).snappedToPixelCenter)
    val dx = snappedMaxX - snappedMinX

    if (dx > 0) {
      val k = (maxXPoint.y - minXPoint.y) / dx
      val q = minXPoint.y - (k * minXPoint.x)
      val snappedMinXPoint = ScreenPoint(snappedMinX, k * snappedMinX + q)
      LazyList
        .iterate(snappedMinXPoint)(point => ScreenPoint(point.x + 1, point.y + k))
        .takeWhile(_.x <= maxXPoint.x + ε)
        .foreach(point => g.draw(new Line2D.Double(point.x, point.y, point.x, screenZeroY)))
    }
  }

  def drawPoint(g: Graphics2D, point: ScreenPoint): Unit = originalLineStyle.drawPoint(g, point)
}

/** This is very similar to SnakePlotPart, but it fills the area between zero Y and the outline of the plot with selected color. */
class VerticalAreaSnakePlotPart(dataProvider: SnakePlotPartDataProvider, plot: Plot) extends SnakePlotPart(dataProvider, plot) {
  override def paintPlotPart(g: Graphics2D): Unit = {
    Graphics2DUtils.withClipDo(g, plot.screenDataBounds) {
      val screenZeroY: Double = plot.axisY.screenValue(0.0)
      val dataPointsDensity = (plot.axisX.dataRange.length / plot.axisX.screenRange.length) / dataProvider.xRange.step

      if (dataPointsDensity > 2.0) {
        val connectedLines = new ConnectedLinesComputer().computeCompressedLines(
          plot.axisX,
          plot.axisY.transformation,
          dataProvider
        )
        new VerticalLinesPlotter(g, screenZeroY, linePlottingProperties).plot(connectedLines)
      } else {
        /* TODO
        My solution to filling the area between the line and zero when there are < 1 points per pixel is to use a special line drawing style, that drops vertical lines from the line to the zero axis. Otherwise it draws the same line as ordinary snake plot would.

        However, that is probably not completely accurate representation, especially on the boundaries between NaN values and valid values.

        For an extreme, but still practical example, consider a single point surrounded by NaNs, and x step between points let's say 10 pixels.
        There would be no line draw, and thus no area filled, because we can't draw a line with just a single point specified.
        But in reality it might be more useful to draw a vertical bar that is 10 pixels wide, centered on the isolated pixel in such case,
        to show that there was something at this point.
        That is especially so when you realize that this line WOULD be plotted with the VerticalLinesPlotter when the density is > 1 line per pixel
        by the *true* part of this `if` clause. So, if you zoom out, you see a vertical line at that point, but if you zoom close enough, it suddenly disappears.
        This is certainly something that needs to be taken care of in the future.
         */
        val plottingPropertiesWithVerticalLines = LinePlottingProperties(
          new VerticalLinesLineStyle(linePlottingProperties.lineStyle, screenZeroY),
          linePlottingProperties.pointStyle
        )
        val linePlotter = new ContinuousLinePlotter(g, plot, plottingPropertiesWithVerticalLines, None)
        iterateVisiblePoints foreach linePlotter.addPoint
      }
    }
  }
}