package info.td.scalaplot.plotparts

import java.awt.Graphics2D

import info.td.scalaplot._
import info.td.scalaplot.utils.Graphics2DHelpers._
import info.td.scalaplot.utils.{Graphics2DUtils, RichStringWithBackground}

case class HorizontalTextBoxAlignment(alignment: VerticalAlignment, pixelsDistance: Int)

case class HorizontalTextBox(
  text: RichStringWithBackground,
  xDataValue: Double,
  alignment: HorizontalTextBoxAlignment = HorizontalTextBoxAlignment(VerticalAlignment.bottom, 30)
)

abstract class HorizontalTextBoxPlotPartDataProvider extends PlotPartDataProvider {
  def textBoxes: Seq[HorizontalTextBox]

  def dataBoundsX: DataRange = textBoxes
    .map(_.xDataValue)
    .foldLeft[DataRange](DataRange.invalid)(_ union _)

  def dataBoundsYAt(range: DataRange): DataRange = DataRange.invalid
}

class HorizontalTextBoxPlotPart(
  val dataProvider: HorizontalTextBoxPlotPartDataProvider,
  plot: Plot,
  verticalLineStyle: LineStyle
) extends LinePlotPart(plot) {
  protected def drawTextBox(g: Graphics2D, textBox: HorizontalTextBox): Unit = {
    val x = plot.axisX.screenValue(textBox.xDataValue)
    val screenTop = plot.screenDataBounds.top
    val screenBottom = plot.screenDataBounds.bottom
    val boxEdgeX = x.snappedToPixelCenter

    verticalLineStyle.drawLine(
      g,
      ScreenPoint(boxEdgeX, screenTop),
      ScreenPoint(boxEdgeX, screenBottom)
    )

    val y = textBox.alignment match {
      case HorizontalTextBoxAlignment(VerticalAlignment.top, distance) => screenTop + distance
      case HorizontalTextBoxAlignment(VerticalAlignment.center, distance) => (screenTop + screenBottom) / 2 + distance
      case HorizontalTextBoxAlignment(VerticalAlignment.bottom, distance) => screenBottom - distance
    }
    textBox.text.drawAt(g, x, y, HorizontalAlignment.center, textBox.alignment.alignment)
  }

  def paintPlotPart(g: Graphics2D): Unit = {
    Graphics2DUtils.withClipDo(g, plot.screenDataBounds) {
      for (textBox <- dataProvider.textBoxes) {
        drawTextBox(g, textBox)
      }
    }
  }
}
