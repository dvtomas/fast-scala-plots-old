package info.td.scalaplot.plotparts

import java.awt.image.BufferedImage
import java.awt.{Color, Graphics2D}

import com.twitter.util.ImmutableLRU
import com.typesafe.scalalogging.LazyLogging
import info.td.scalaplot._
import info.td.scalaplot.plotparts.EquidistantRange.DataIndicesAtPixel
import info.td.scalaplot.utils.{Graphics2DUtils, ValueFormatter}

import scala.collection.immutable.IndexedSeq

/**
  * This class represents a single column of the height map.
  * To speed drawing up, the pre-rendered columns of the height map are cached in BufferedImage-s.
  * It is up to the user of the HeightMap to select the type of the key to the cache. Some of the options are
  * - some index of the column maintained by the DataProvider
  * - create a unique UUID for each column
  *
  * @param key    The unique key of this column
  * @param values A function that returns a value of the height map for given index
  * @tparam CacheKey type of the caching key
  */
case class HeightMapColumn[CacheKey](key: CacheKey, values: HeightMapColumn.Values)

object HeightMapColumn {
  type Values = Int => Double
}

object CommonAggregateFunctions {
  type AggregateFunc = HeightMapColumn.Values => Range => Double

  val takeFirst: AggregateFunc = (heightMapColumnValues: HeightMapColumn.Values) => (dataIndices: Range) =>
    heightMapColumnValues(dataIndices.head)

  val takeMinimum: AggregateFunc = (heightMapColumnValues: HeightMapColumn.Values) => (dataIndices: Range) => {
    var minimum = heightMapColumnValues(dataIndices.head)
    dataIndices /* .drop(1) */ foreach (index => {
      val v = heightMapColumnValues(index)
      if (v < minimum)
        minimum = v
    })
    minimum
  }

  val takeMaximum: AggregateFunc = (heightMapColumnValues: HeightMapColumn.Values) => (dataIndices: Range) => {
    var maximum = heightMapColumnValues(dataIndices.head)
    dataIndices /* .drop(1) */ foreach (index => {
      val v = heightMapColumnValues(index)
      if (v > maximum)
        maximum = v
    })
    maximum
  }

  val takeAverage: AggregateFunc = (heightMapColumnValues: HeightMapColumn.Values) => (dataIndices: Range) => {
    var sum = 0.0
    dataIndices foreach (index => {
      sum += heightMapColumnValues(index)
    })
    sum / dataIndices.size
  }
}

trait HeightMapDataProvider[CacheKey] extends EquidistantXPlotPartDataProvider[HeightMapColumn[CacheKey]] {
  def yRange: EquidistantRange

  override final def dataBoundsY: DataRange = yRange.dataBounds

  final def dataBoundsYAt(range: DataRange): DataRange = dataBoundsY
}

class HeightMapPlotPart[CacheKey](
  val dataProvider: HeightMapDataProvider[CacheKey],
  plot: Plot,
  palette: HeightMapPalette,
  aggregateYFunc: CommonAggregateFunctions.AggregateFunc,
  columnImagesCacheSize: Int = 3000
) extends PlotPart with LazyLogging {

  private var columnImagesCache = ImmutableLRU[Seq[CacheKey], BufferedImage](columnImagesCacheSize)
  private var lastYDataIndicesPerPixels: Seq[DataIndicesAtPixel] = Seq.empty

  private def doYDataIndicesPerPixelMatchTheLastOne(yDataIndicesPerPixels: Seq[DataIndicesAtPixel]) = {
    if (yDataIndicesPerPixels.size == lastYDataIndicesPerPixels.size) {
      val offset = yDataIndicesPerPixels.head.pixel - lastYDataIndicesPerPixels.head.pixel
      val normalizedLastYDataIndicesPerPixels = yDataIndicesPerPixels map (dataIndicesAtPixel => DataIndicesAtPixel(dataIndicesAtPixel.pixel - offset, dataIndicesAtPixel.dataIndices))
      normalizedLastYDataIndicesPerPixels == lastYDataIndicesPerPixels
    } else {
      false
    }
  }

  def paintPlotPart(g: Graphics2D): Unit = {
    case class LowPixelAndHighPixel(lowPixel: Int, highPixel: Int)

    def drawColumn(
      xDataIndicesPerPixel: DataIndicesAtPixel, yDataIndicesPerPixels: Seq[DataIndicesAtPixel],
      yMin: Int, yMax: Int,
      precomputedHoles: Seq[LowPixelAndHighPixel]
    ): BufferedImage = {
      val image = new BufferedImage(1, yMax - yMin + 1, BufferedImage.TYPE_INT_RGB)
      val columnsAtPixel = xDataIndicesPerPixel.dataIndices map dataProvider.yValue

      if (columnsAtPixel.size == 1) {
        // Optimization for just one column at the pixel
        val column = columnsAtPixel.head
        for (yDataIndicesPerPixel <- yDataIndicesPerPixels) {
          if (yDataIndicesPerPixel.dataIndices.nonEmpty) {
            val value = aggregateYFunc(column.values)(yDataIndicesPerPixel.dataIndices)
            image.setRGB(0, yDataIndicesPerPixel.pixel - yMin, palette(value))
          }
        }
      } else {
        val numberOfColumns = columnsAtPixel.indices
        val curriedAggregateYFunctions: IndexedSeq[Range => Double] = columnsAtPixel map (column => aggregateYFunc(column.values))
        for (yDataIndicesPerPixel <- yDataIndicesPerPixels) {
          if (yDataIndicesPerPixel.dataIndices.nonEmpty) {
            val aggregatedYValuesForColumns = curriedAggregateYFunctions map (_ (yDataIndicesPerPixel.dataIndices))
            val value = aggregateYFunc(aggregatedYValuesForColumns)(numberOfColumns)
            image.setRGB(0, yDataIndicesPerPixel.pixel - yMin, palette(value))
          }
        }
      }

      // Fill in the holes
      precomputedHoles foreach (lowPixelAndHighPixel => {
        val color = image.getRGB(0, lowPixelAndHighPixel.lowPixel - yMin)
        (lowPixelAndHighPixel.lowPixel + 1 until lowPixelAndHighPixel.highPixel) foreach { pixel =>
          image.setRGB(0, pixel - yMin, color)
        }
      })

      image
    }

    Graphics2DUtils.withClipDo(g, plot.screenDataBounds) {
      def fixHighestIndexOfDataIndices(indicesAtPixels: Seq[DataIndicesAtPixel], maximumIndex: Int) = {
        def fixRange(range: Range): Range = {
          if (range.isEmpty)
            range
          else if (range.step == 1) {
            assert(range.last <= maximumIndex)
            if (range.last == maximumIndex)
              range.start until maximumIndex
            else
              range
          } else if (range.step == -1) {
            assert(range.start <= maximumIndex)
            if (range.start == maximumIndex)
              range.start - 1 to range.last by -1
            else
              range
          } else {
            ??? // can't handle other steps, but they shouldn't happen anyway
          }
        }

        val result = indicesAtPixels map (indicesAtPixel => indicesAtPixel.copy(dataIndices = fixRange(indicesAtPixel.dataIndices)))
        result
      }

      val yDataIndicesPerPixels: Seq[DataIndicesAtPixel] = fixHighestIndexOfDataIndices(
        dataProvider.yRange.findDataIndicesPackedByPixels(plot.axisY),
        dataProvider.yRange.numberOfPoints - 1
      )

      if (yDataIndicesPerPixels.nonEmpty) {
        if (!doYDataIndicesPerPixelMatchTheLastOne(yDataIndicesPerPixels)) {
          columnImagesCache = ImmutableLRU[Seq[CacheKey], BufferedImage](columnImagesCacheSize)
        }

        lastYDataIndicesPerPixels = yDataIndicesPerPixels
        val y1 = yDataIndicesPerPixels.head.pixel
        val y2 = yDataIndicesPerPixels.last.pixel
        val (yMin, yMax) = if (y1 < y2) (y1, y2) else (y2, y1)
        val dataIndicesPackedByPixels = fixHighestIndexOfDataIndices(
          dataProvider.xRange.findDataIndicesPackedByPixels(plot.axisX),
          dataProvider.xRange.numberOfPoints - 1
        )

        val precomputedYHoles = yDataIndicesPerPixels
          .filter(_.dataIndices.nonEmpty)
          .sliding(2)
          .collect { case Seq(high, low) => LowPixelAndHighPixel(low.pixel, high.pixel) }
          .filter(lowPixelAndHighPixel => lowPixelAndHighPixel.highPixel > lowPixelAndHighPixel.lowPixel + 1)
          .toSeq

        for (xDataIndicesPerPixel <- dataIndicesPackedByPixels) {
          val columns = xDataIndicesPerPixel.dataIndices map dataProvider.yValue
          if (columns.nonEmpty) {
            val imagesCacheKey = columns map (_.key)
            val (maybeCachedImage, newColumnImagesCache) = columnImagesCache.get(imagesCacheKey)
            columnImagesCache = newColumnImagesCache
            val image = maybeCachedImage getOrElse {
              val image = drawColumn(xDataIndicesPerPixel, yDataIndicesPerPixels, yMin, yMax, precomputedYHoles)
              val (_, newColumnImagesCache) = columnImagesCache + (imagesCacheKey, image)
              columnImagesCache = newColumnImagesCache
              image
            }
            g.drawImage(image, xDataIndicesPerPixel.pixel, yMin, null)
          }
        }
        // Fill in the holes
        dataIndicesPackedByPixels.filter(_.dataIndices.nonEmpty).sliding(2) foreach {
          case Seq(low, high) => {
            val columns = low.dataIndices map dataProvider.yValue
            val imagesCacheKey = columns map (_.key)
            val image = columnImagesCache
              .weakGet(imagesCacheKey)
              .getOrElse {
                logger.warn("Cache miss while filling the holes of the height map. You should probably increase cache for solid performance.")
                drawColumn(low, yDataIndicesPerPixels, yMin, yMax, precomputedYHoles)
              }
            (low.pixel + 1 until high.pixel) foreach { pixel =>
              g.drawImage(image, pixel, yMin, null)
            }
          }
          case _ =>
        }
      }
    }
  }

  def plotLegendPart(g: Graphics2D, referencePoint: ScreenPoint): ScreenPoint = referencePoint

  protected def pickInfoFor(point: DataPoint)(implicit valueFormatterLocale: ValueFormatterLocale): PickInfo = {
    def valueString(axis: Axis, screenValue: Double) = {
      val digitChange = axis digitChange screenValue
      // Display with precision 2 digits more than the precision of picker
      ValueFormatter.valueString(digitChange.copy(changedExponent = digitChange.changedExponent - 2))
    }

    PickInfo(
      this, plot.screenPoint(point), point,
      valueString(plot.axisX, plot.screenPoint(point).x),
      valueString(plot.axisY, plot.screenPoint(point).y),
      Color.WHITE
    )
  }

  override def pick(where: ScreenPoint)(implicit valueFormatterLocale: ValueFormatterLocale): Option[PickInfo] = {
    val x = plot.axisX.dataValue(where.x)
    val y = plot.axisY.dataValue(where.y)
    if ((dataProvider.dataBoundsX contains x) && (dataProvider.dataBoundsY contains y)) {
      Some(pickInfoFor(DataPoint(x, y)))
    } else {
      None
    }
  }
}

