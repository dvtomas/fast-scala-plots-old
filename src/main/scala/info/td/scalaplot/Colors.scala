package info.td.scalaplot

import info.td.common.ui.swing.SwingHelpers

import java.awt.Color
import javax.swing.{JLabel, JScrollBar, JTextField}

object Colors {
  private[this] def textField = new JTextField()

  private[this] def label = new JLabel()

  def pickerLine: Color = textField.getForeground

  def pickerBoxBackground: Color = textField.getBackground

  def pickerBoxBorder: Color = plotDataBorder

  def pickerBoxForeground: Color = textField.getForeground

  def plotBackground: Color = textField.getBackground

  def plotDataBorder: Color = if (SwingHelpers.isDarkLaf) Color.GRAY else Color.DARK_GRAY

  def plotBorder: Color = if (SwingHelpers.isDarkLaf) Color.LIGHT_GRAY else Color.BLACK

  def zeroAxis: Color = if (SwingHelpers.isDarkLaf) Color.LIGHT_GRAY else Color.DARK_GRAY

  def legendBackground: Color = label.getBackground

  def legendBorder: Color = plotDataBorder

  def minorTicks: Color = SwingHelpers.colorAdaptedToDarkLightLaf(Color.LIGHT_GRAY)

  def majorTicks: Color = SwingHelpers.colorAdaptedToDarkLightLaf(Color.DARK_GRAY)

  def trampolineBorder: Color = plotBorder

  // Intellij colors
  def trampolineNeutralColor: Color = blendedColor(
    if (SwingHelpers.isDarkLaf) Color.decode("#6E6E6E") else Color.decode("#AFB1B3"),
    new JScrollBar().getBackground,
    0.25,
  )

  def trampolineEdge: Color = blendedColor(
    if (SwingHelpers.isDarkLaf) Color.decode("#3592C4") else Color.decode("#389FD6"),
    new JScrollBar().getBackground,
    0.25,
  )

  def blendedColor(c1: Color, c2: Color, ratio: Double): Color = {
    @inline
    def blendComponent(a: Int, b: Int) = (b - (b - a) * ratio).toInt

    new Color(
      blendComponent(c1.getRed, c2.getRed),
      blendComponent(c1.getGreen, c2.getGreen),
      blendComponent(c1.getBlue, c2.getBlue)
    )
  }
}