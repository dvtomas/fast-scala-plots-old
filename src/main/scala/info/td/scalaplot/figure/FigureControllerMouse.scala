package info.td.scalaplot.figure

import java.awt.event.{MouseEvent, MouseListener}

import info.td.scalaplot._
import javax.swing.{JComponent, SwingUtilities}

class FigureControllerMouse(protected val figure: Figure) extends FigureController with CommonFigureControllerReactions {
  figure.addMouseListener(new MouseListener {
    def mouseExited(mouseEvent: MouseEvent): Unit = {}

    def mouseClicked(mouseEvent: MouseEvent): Unit = {}

    def mouseEntered(mouseEvent: MouseEvent): Unit = {}

    def mousePressed(mouseEvent: MouseEvent): Unit = {
      if (SwingUtilities.isLeftMouseButton(mouseEvent)) {
        figure.requestFocusInWindow()
        controllersForPlotsHitAt(mouseEvent.getPoint).foreach(_.handleZoomRectangleStart(ScreenPoint(mouseEvent.getPoint)))
      }
    }

    def mouseReleased(mouseEvent: MouseEvent): Unit = {}
  })

  def figureControlButtons: Seq[JComponent] = Seq.empty
}