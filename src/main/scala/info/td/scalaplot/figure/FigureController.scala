package info.td.scalaplot.figure

import java.awt.event._
import java.awt.{Component, Point}

import info.td.scalaplot._
import info.td.scalaplot.plotcontroller.PlotController
import info.td.scalaplot.utils.{CopyImageToClipBoard, FileDialogUtils, RichPopupMenu}
import javax.swing.JPopupMenu.Separator
import javax.swing._

abstract class FigureController {
  def controllers: Seq[PlotController]

  def controllersForPlotsHitAt(point: Point): Seq[PlotController]
}

private[scalaplot] case class ControllerAndPickerPosition(controller: PlotController, screenPoint: ScreenPoint)

trait CommonFigureControllerReactions {
  protected def figure: Figure

  private def plotsContainer = figure.plotsContainer

  def controllers: Seq[PlotController] = plotsContainer.controllers

  def controllersForPlotsHitAt(point: Point): Seq[PlotController] =
    plotsContainer.controllersHitAt(ScreenPoint(point))

  protected def pickAt(point: Point): Unit = {
    val maybeControllerAndPosition = controllersForPlotsHitAt(point).headOption map (controller => ControllerAndPickerPosition(controller, ScreenPoint(point)))
    plotsContainer.controllers foreach (_.setMaybeControllerAndPickerPosition(maybeControllerAndPosition))
  }

  def dragAt(point: Point): Unit = {
    controllersForPlotsHitAt(point).foreach(_.handleDragStart(ScreenPoint(point)))
  }

  figure.addMouseListener(new MouseListener {
    def mouseClicked(mouseEvent: MouseEvent): Unit = {}

    def mouseExited(mouseEvent: MouseEvent): Unit = {
      plotsContainer.controllers foreach (_ setMaybeControllerAndPickerPosition None)
    }

    def mouseEntered(mouseEvent: MouseEvent): Unit = {}

    def mousePressed(mouseEvent: MouseEvent): Unit = {
      if (SwingUtilities.isRightMouseButton(mouseEvent)) {
        controllersForPlotsHitAt(mouseEvent.getPoint).foreach(_.handleCancelAction())
      } else if (SwingUtilities.isMiddleMouseButton(mouseEvent)) {
        dragAt(mouseEvent.getPoint)
      }
    }

    def mouseReleased(mouseEvent: MouseEvent): Unit = {
      if (!SwingUtilities.isRightMouseButton(mouseEvent)) {
        plotsContainer.controllers.foreach(_.handleFinishAction())
      } else {
        val jComponent = figure

        val plotMenuItems: Seq[JMenuItem] = controllersForPlotsHitAt(mouseEvent.getPoint).toList.flatMap(_.menuItems)
        val menuItems: Seq[Component] =
          plotMenuItems ++
            (if (plotMenuItems.isEmpty) Seq.empty else Seq(new JSeparator())) ++
            Seq(
              RichPopupMenu.newMenuItem(
                fastScalaPlotsPo.t("Copy to clipboard"),
                enabled = true,
                () => (new CopyImageToClipBoard).copyComponentToClipBoard(jComponent)
              ),
              RichPopupMenu.newMenuItem(
                fastScalaPlotsPo.t("Save image to file..."),
                enabled = true,
                () => {
                  val image = (new CopyImageToClipBoard).getImageFromComponent(jComponent)
                  FileDialogUtils.saveImageToFile(jComponent, image)
                }
              )
            ) ++ figure.popupMenuItems

        val popupMenu = new JPopupMenu()
        menuItems foreach popupMenu.add
        popupMenu.show(figure, mouseEvent.getPoint.x, mouseEvent.getPoint.y)
      }
    }
  })

  figure.addMouseMotionListener(new MouseMotionListener {
    def mouseMoved(mouseEvent: MouseEvent): Unit = {
      pickAt(mouseEvent.getPoint)
    }

    def mouseDragged(mouseEvent: MouseEvent): Unit = {
      plotsContainer.controllers.foreach(_.handleMouseDragged(ScreenPoint(mouseEvent.getPoint)))
    }
  })

  figure.addMouseWheelListener((event: MouseWheelEvent) => {
    val (shouldZoomX, shouldZoomY) = (event.isShiftDown, event.isControlDown) match {
      case (false, false) => (true, true)
      case (false, true) => (false, true)
      case (true, false) => (true, false)
      case (true, true) => (true, true)
    }

    controllersForPlotsHitAt(event.getPoint).foreach(
      _.handleZoom(
        ScreenPoint(event.getPoint),
        event.getWheelRotation,
        shouldZoomX,
        shouldZoomY
      ))
  })

  figure.addKeyListener(new KeyListener {
    def keyTyped(keyEvent: KeyEvent): Unit = {}

    def keyPressed(keyEvent: KeyEvent): Unit = {}

    def keyReleased(keyEvent: KeyEvent): Unit = {
      plotsContainer.controllers.foreach(_.handleCancelAction())
    }
  })
}