package info.td.scalaplot.figure

import info.td.common.ui.swing.SwingHelpers
import info.td.scalaplot._
import info.td.scalaplot.plotcontroller.PlotController
import info.td.scalaplot.utils._

import java.awt.Component
import java.awt.event._
import java.awt.image.BufferedImage
import javax.swing._

private[scalaplot] case class ControllerAndPickerPosition(controller: PlotController, screenPoint: ScreenPoint)

abstract class FigureController(figure: Figure) {
  private def plotsContainer = figure.plotsContainer

  protected def leftButtonPressedAction(point: ScreenPoint): Unit

  private[this] def plotControllers: Seq[PlotController] = plotsContainer.controllers

  def controllersForPlotsHitAt(point: ScreenPoint): Seq[PlotController] = plotsContainer.controllersHitAt(point)

  protected def pickAt(point: ScreenPoint): Unit = {
    val maybeControllerAndPosition =
      controllersForPlotsHitAt(point)
        .headOption
        .map(controller => ControllerAndPickerPosition(controller, point))

    plotControllers.foreach(_.setMaybeControllerAndPickerPosition(maybeControllerAndPosition))
  }

  def dragAt(point: ScreenPoint): Unit = {
    controllersForPlotsHitAt(point).foreach(_.handleDragStart(point))
  }

  figure.addMouseListener(new MouseListener {
    def mouseClicked(mouseEvent: MouseEvent): Unit = {}

    def mouseExited(mouseEvent: MouseEvent): Unit = {
      plotControllers foreach (_ setMaybeControllerAndPickerPosition None)
    }

    def mouseEntered(mouseEvent: MouseEvent): Unit = {}

    def mousePressed(mouseEvent: MouseEvent): Unit = {
      val point = ScreenPoint(mouseEvent.getPoint)

      if (SwingUtilities.isLeftMouseButton(mouseEvent)) {
        figure.requestFocusInWindow()
        leftButtonPressedAction(point)
      } else if (SwingUtilities.isRightMouseButton(mouseEvent)) {
        controllersForPlotsHitAt(point).foreach(_.handleCancelAction())
      } else if (SwingUtilities.isMiddleMouseButton(mouseEvent)) {
        dragAt(point)
      }
    }

    def mouseReleased(mouseEvent: MouseEvent): Unit = {
      if (!SwingUtilities.isRightMouseButton(mouseEvent)) {
        plotControllers.foreach(_.handleFinishAction())
      } else {
        val jComponent = figure

        val plotMenuItems: Seq[JMenuItem] = controllersForPlotsHitAt(ScreenPoint(mouseEvent.getPoint)).flatMap { controller =>
          controller.createMenuItems(controller.pickerPosition)
        }
        val menuItems: Seq[Component] =
          plotMenuItems ++
            (if (plotMenuItems.isEmpty) Seq.empty else Seq(new JSeparator())) ++
            Seq(
              RichPopupMenu.newMenuItem(
                fastScalaPlotsPo.t("Copy image to clipboard"),
                enabled = true,
                () => (new CopyImageToClipBoard).copyComponentToClipBoard(jComponent)
              ),
              RichPopupMenu.newMenuItem(
                fastScalaPlotsPo.t("Save image to file..."),
                enabled = true,
                () => {
                  val image = (new CopyImageToClipBoard).getImageFromComponent(jComponent)
                  FileDialogUtils.saveImageToFile(jComponent, image)
                }
              )
            ) ++ figure.popupMenuItems

        val popupMenu = new JPopupMenu()
        menuItems foreach popupMenu.add
        popupMenu.show(figure, mouseEvent.getPoint.x, mouseEvent.getPoint.y)
      }
    }
  })

  figure.addMouseMotionListener(new MouseMotionListener {
    def mouseMoved(mouseEvent: MouseEvent): Unit = {
      pickAt(ScreenPoint(mouseEvent.getPoint))
    }

    def mouseDragged(mouseEvent: MouseEvent): Unit = {
      plotControllers.foreach(_.handleMouseDragged(ScreenPoint(mouseEvent.getPoint)))
    }
  })

  figure.addMouseWheelListener((event: MouseWheelEvent) => {
    val (shouldZoomX, shouldZoomY) = (event.isShiftDown, event.isControlDown) match {
      case (false, false) => (true, true)
      case (false, true) => (false, true)
      case (true, false) => (true, false)
      case (true, true) => (true, true)
    }

    controllersForPlotsHitAt(ScreenPoint(event.getPoint)).foreach(
      _.handleZoom(
        ScreenPoint(event.getPoint),
        event.getWheelRotation,
        shouldZoomX,
        shouldZoomY
      ))
  })

  figure.addKeyListener(new KeyListener {
    def keyTyped(keyEvent: KeyEvent): Unit = {}

    def keyPressed(keyEvent: KeyEvent): Unit = {}

    def keyReleased(keyEvent: KeyEvent): Unit = {
      plotControllers.foreach(_.handleCancelAction())
    }
  })
}

class FigureControllerMouse(figure: Figure) extends FigureController(figure) {
  override protected def leftButtonPressedAction(point: ScreenPoint): Unit = {
    controllersForPlotsHitAt(point).foreach(_.handleZoomRectangleStart(point))
  }
}

sealed abstract class TouchMode

object TouchMode {

  case object Drag extends TouchMode

  case object ZoomRectangle extends TouchMode

  /** Direction: 1 - Zoom out, -1: Zoom in */
  case class Zoom(direction: Int, shouldZoomX: Boolean, shouldZoomY: Boolean) extends TouchMode

}


object TouchActions {

  case class TouchActionSpec(
    description: String,
    iconSmall: ImageIcon,
    iconLarge: ImageIcon,
    mode: TouchMode,
  )

  private[this] def touchAction(description: String, iconStem: String, mode: TouchMode): TouchActionSpec = {
    def icon(size: String) = SwingHelpers.imageIcon("/fast-scala-plots-icons/" + iconStem + "-" + size + ".png")

    TouchActionSpec(
      description,
      icon("16"),
      icon("24"),
      mode,
    )
  }

  val dragAction: TouchActionSpec = touchAction(
    fastScalaPlotsPo.t("Drag"),
    "hand",
    TouchMode.Drag
  )

  val zoomRectangleAction: TouchActionSpec = touchAction(
    fastScalaPlotsPo.t("Zoom rectangle"),
    "magnifier-zoom-fit",
    TouchMode.ZoomRectangle
  )

  val zoomInAction: TouchActionSpec = touchAction(
    fastScalaPlotsPo.t("Zoom in"),
    "magnifier-zoom-in",
    TouchMode.Zoom(-1, shouldZoomX = true, shouldZoomY = true)
  )

  val zoomInXAction: TouchActionSpec = touchAction(
    fastScalaPlotsPo.t("Zoom in horizontal axis"),
    "magnifier-zoom-in-x",
    TouchMode.Zoom(-1, shouldZoomX = true, shouldZoomY = false)
  )

  val zoomInYAction: TouchActionSpec = touchAction(
    fastScalaPlotsPo.t("Zoom in vertical axis"),
    "magnifier-zoom-in-y",
    TouchMode.Zoom(-1, shouldZoomX = false, shouldZoomY = true)
  )

  val zoomOutAction: TouchActionSpec = touchAction(
    fastScalaPlotsPo.t("Zoom out"),
    "magnifier-zoom-out",
    TouchMode.Zoom(1, shouldZoomX = true, shouldZoomY = true)
  )

  val zoomOutXAction: TouchActionSpec = touchAction(
    fastScalaPlotsPo.t("Zoom out horizontal axis"),
    "magnifier-zoom-out-x",
    TouchMode.Zoom(1, shouldZoomX = true, shouldZoomY = false)
  )

  val zoomOutYAction: TouchActionSpec = touchAction(
    fastScalaPlotsPo.t("Zoom out vertical axis"),
    "magnifier-zoom-out-y",
    TouchMode.Zoom(1, shouldZoomX = false, shouldZoomY = true)
  )

  val defaultActions = List(
    dragAction,
    zoomRectangleAction,
    zoomInAction,
    zoomOutAction,
  )
}

class TouchActions(actions: List[TouchActions.TouchActionSpec]) {

  def addSetTouchModeCallback(callback: TouchMode => Unit): Unit = {
    setTouchModeCallbacks = setTouchModeCallbacks ++ Seq(callback)
  }

  lazy val richActions: List[RichAction] = actionHolders.map(_.action)

  def getTouchMode: TouchMode = touchMode

  def figureControlButtons: Seq[JButton] = actionHolders.map(_.button)

  protected[this] case class ActionHolder(mode: TouchMode, action: RichAction, button: JButton)

  private[this] var setTouchModeCallbacks: Seq[TouchMode => Unit] = Nil

  private[this] def iconWithSelectedIndication(originalIcon: Icon): ImageIcon = {
    val tickIcon = SwingHelpers.imageIcon("/fast-scala-plots-icons/active-mode-tick.png")

    val combinedIcon = new BufferedImage(
      originalIcon.getIconWidth,
      originalIcon.getIconHeight,
      BufferedImage.TYPE_INT_ARGB
    )
    val g = combinedIcon.createGraphics
    tickIcon.paintIcon(null, g, 0, 0)
    originalIcon.paintIcon(null, g, 0, 0)
    g.dispose()
    new ImageIcon(combinedIcon)
  }

  protected[this] def setIndicationStateForMode(mode: TouchMode, isActive: Boolean): Unit = {
    actionHolders
      .filter(_.mode == mode)
      .foreach { holder =>
        val icon = if (isActive) {
          iconWithSelectedIndication(holder.action.largeIcon)
        } else {
          holder.action.largeIcon
        }
        holder.button.setIcon(icon)
        holder.button.setDisabledIcon(icon)
      }
  }

  private[this] def setTouchMode(newMode: TouchMode): Unit = {
    if (newMode != touchMode) {
      setIndicationStateForMode(touchMode, isActive = false)
      touchMode = newMode
      setIndicationStateForMode(newMode, isActive = true)
    }
    setTouchModeCallbacks.foreach(_.apply(newMode))
  }

  protected[this] lazy val actionHolders: List[ActionHolder] = actions.map { action =>
    val richAction = new RichAction(
      action.description,
      () => setTouchMode(action.mode)
    )
    richAction.setDescription(action.description)
    richAction.setSmallIcon(action.iconSmall)
    richAction.setLargeIcon(action.iconLarge)

    val button = new JButton(richAction)
    button.setHideActionText(true)

    ActionHolder(action.mode, richAction, button)
  }

  private val defaultMode = TouchMode.Drag

  private[this] var touchMode: TouchMode = defaultMode

  setTouchMode(defaultMode)
  setIndicationStateForMode(defaultMode, isActive = true)
}

class FigureControllerTouchDevice(
  protected val figure: Figure,
  touchActions: TouchActions,
) extends FigureController(figure) {

  override protected def leftButtonPressedAction(point: ScreenPoint): Unit = {
    controllersForPlotsHitAt(point).foreach { controller =>
      touchActions.getTouchMode match {
        case TouchMode.Drag => controller.handleDragStart(point)
        case TouchMode.ZoomRectangle => controller.handleZoomRectangleStart(point)
        case TouchMode.Zoom(direction, shouldZoomX, shouldZoomY) => {
          controller.handleZoom(point, direction, shouldZoomX, shouldZoomY)
        }
      }
    }
  }
}

class FigureControllerTouchDevicePicksOnly(protected val figure: Figure) extends FigureController(figure) {
  override protected def leftButtonPressedAction(point: ScreenPoint): Unit = {
    pickAt(point)
  }
}