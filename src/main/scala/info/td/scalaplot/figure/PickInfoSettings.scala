package info.td.scalaplot.figure

case class PickInfoSettings(
  drawPicker: Boolean,
  drawPickerPosition: Boolean,
  highlightPoint: Boolean,
  drawPickValue: Boolean,
  alsoDisplayXValue: Boolean
)