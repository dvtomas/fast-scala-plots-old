package info.td.scalaplot.figure

import info.td.scalaplot._
import info.td.scalaplot.utils.{BooleanRichAction, RichPopupMenu}
import javax.swing.JMenu

case class PickInfoSettings(drawPicker: Boolean, drawPickerPosition: Boolean, highlightPoint: Boolean, drawPickValue: Boolean, alsoDisplayXValue: Boolean)

class PickInfoActions {
  private[this] val drawPickerAction = new BooleanRichAction(fastScalaPlotsPo.t("Draw picker"))

  private[this] val drawPickerPositionAction = new BooleanRichAction(fastScalaPlotsPo.t("Draw picker position"))

  private[this] val highlightPointAction = new BooleanRichAction(fastScalaPlotsPo.t("Highlight point"))

  private[this] val drawPickValueAction = new BooleanRichAction(fastScalaPlotsPo.t("Draw pick value"))

  private[this] val alsoDisplayXValueAction = new BooleanRichAction(fastScalaPlotsPo.t("Also display X value"))

  drawPickerAction setSelected true
  drawPickerPositionAction setSelected true
  highlightPointAction setSelected true
  drawPickValueAction setSelected true
  alsoDisplayXValueAction setSelected true

  def setDrawPicker(value: Boolean): Unit = drawPickerAction setSelected value

  def setDrawPickerPosition(value: Boolean): Unit = drawPickerPositionAction setSelected value

  def setHighlightPoint(value: Boolean): Unit = highlightPointAction setSelected value

  def setDrawPickValue(value: Boolean): Unit = drawPickValueAction setSelected value

  def setAlsoDisplayXValue(value: Boolean): Unit = alsoDisplayXValueAction setSelected value

  def settings: PickInfoSettings = PickInfoSettings(
    drawPickerAction.selected,
    drawPickerPositionAction.selected,
    highlightPointAction.selected,
    drawPickValueAction.selected,
    alsoDisplayXValueAction.selected
  )

  lazy val actionsSubMenu: JMenu = {
    val menu = new JMenu(fastScalaPlotsPo.t("Pick info"))
    Seq(
      drawPickerAction,
      drawPickerPositionAction,
      highlightPointAction,
      drawPickValueAction,
      alsoDisplayXValueAction
    ) foreach (action => menu.add(RichPopupMenu.newCheckMenuItem(action)))
    menu
  }
}