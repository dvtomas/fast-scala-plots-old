package info.td.scalaplot.figure

import java.awt.event.{MouseEvent, MouseListener}
import java.awt.image.BufferedImage

import info.td.common.ui.swing.SwingHelpers
import info.td.scalaplot._
import info.td.scalaplot.utils._
import javax.swing._

sealed abstract class TouchMode

object TouchMode {

  case object Drag extends TouchMode

  case object ZoomRectangle extends TouchMode

  /** Direction: 1 - Zoom out, -1: Zoom in */
  case class Zoom(direction: Int, shouldZoomX: Boolean, shouldZoomY: Boolean) extends TouchMode

}


object TouchActions {

  case class TouchActionSpec(
    description: String,
    iconSmall: ImageIcon,
    iconLarge: ImageIcon,
    mode: TouchMode,
  )

  private[this] def touchAction(description: String, iconStem: String, mode: TouchMode): TouchActionSpec = {
    def icon(size: String) = SwingHelpers.imageIcon("/fast-scala-plots-icons/" + iconStem + "-" + size + ".png")

    TouchActionSpec(
      description,
      icon("16"),
      icon("24"),
      mode,
    )
  }

  val dragAction: TouchActionSpec = touchAction(
    fastScalaPlotsPo.t("Drag"),
    "hand",
    TouchMode.Drag
  )

  val zoomRectangleAction: TouchActionSpec = touchAction(
    fastScalaPlotsPo.t("Zoom rectangle"),
    "magnifier-zoom-fit",
    TouchMode.ZoomRectangle
  )

  val zoomInAction: TouchActionSpec = touchAction(
    fastScalaPlotsPo.t("Zoom in"),
    "magnifier-zoom-in",
    TouchMode.Zoom(-1, shouldZoomX = true, shouldZoomY = true)
  )

  val zoomInXAction: TouchActionSpec = touchAction(
    fastScalaPlotsPo.t("Zoom in horizontal axis"),
    "magnifier-zoom-in-x",
    TouchMode.Zoom(-1, shouldZoomX = true, shouldZoomY = false)
  )

  val zoomInYAction: TouchActionSpec = touchAction(
    fastScalaPlotsPo.t("Zoom in vertical axis"),
    "magnifier-zoom-in-y",
    TouchMode.Zoom(-1, shouldZoomX = false, shouldZoomY = true)
  )

  val zoomOutAction: TouchActionSpec = touchAction(
    fastScalaPlotsPo.t("Zoom out"),
    "magnifier-zoom-out",
    TouchMode.Zoom(1, shouldZoomX = true, shouldZoomY = true)
  )

  val zoomOutXAction: TouchActionSpec = touchAction(
    fastScalaPlotsPo.t("Zoom out horizontal axis"),
    "magnifier-zoom-out-x",
    TouchMode.Zoom(1, shouldZoomX = true, shouldZoomY = false)
  )

  val zoomOutYAction: TouchActionSpec = touchAction(
    fastScalaPlotsPo.t("Zoom out vertical axis"),
    "magnifier-zoom-out-y",
    TouchMode.Zoom(1, shouldZoomX = false, shouldZoomY = true)
  )

  val defaultActions = List(
    dragAction,
    zoomRectangleAction,
    zoomInAction,
    zoomOutAction,
  )
}

class TouchActions(actions: List[TouchActions.TouchActionSpec]) {

  def addSetTouchModeCallback(callback: TouchMode => Unit): Unit = {
    setTouchModeCallbacks = setTouchModeCallbacks ++ Seq(callback)
  }

  lazy val richActions: List[RichAction] = actionHolders.map(_.action)

  def getTouchMode: TouchMode = touchMode

  def figureControlButtons: Seq[JButton] = actionHolders.map(_.button)

  private[this] case class ActionHolder(mode: TouchMode, action: RichAction, button: JButton)

  private[this] var setTouchModeCallbacks: Seq[TouchMode => Unit] = Nil

  private[this] def iconWithSelectedIndication(originalIcon: Icon): ImageIcon = {
    val tickIcon = SwingHelpers.imageIcon("/fast-scala-plots-icons/active-mode-tick.png")

    val combinedIcon = new BufferedImage(
      originalIcon.getIconWidth,
      originalIcon.getIconHeight,
      BufferedImage.TYPE_INT_ARGB
    )
    val g = combinedIcon.createGraphics
    tickIcon.paintIcon(null, g, 0, 0)
    originalIcon.paintIcon(null, g, 0, 0)
    g.dispose()
    new ImageIcon(combinedIcon)
  }

  private[this] def setIndicationStateForMode(mode: TouchMode, isActive: Boolean): Unit = {
    actionHolders
      .filter(_.mode == mode)
      .foreach { holder =>
        val icon = if (isActive) {
          iconWithSelectedIndication(holder.action.largeIcon)
        } else {
          holder.action.largeIcon
        }
        holder.button.setIcon(icon)
        holder.button.setDisabledIcon(icon)
      }
  }

  private[this] def setTouchMode(newMode: TouchMode): Unit = {
    if (newMode != touchMode) {
      setIndicationStateForMode(touchMode, isActive = false)
      touchMode = newMode
      setIndicationStateForMode(newMode, isActive = true)
    }
    setTouchModeCallbacks.foreach(_.apply(newMode))
  }

  private[this] lazy val actionHolders = actions.map { action =>
    val richAction = new RichAction(
      action.description,
      () => setTouchMode(action.mode)
    )
    richAction.setDescription(action.description)
    richAction.setSmallIcon(action.iconSmall)
    richAction.setLargeIcon(action.iconLarge)

    val button = new JButton(richAction)
    button.setHideActionText(true)

    ActionHolder(action.mode, richAction, button)
  }

  private val defaultMode = TouchMode.Drag

  private[this] var touchMode: TouchMode = defaultMode

  setTouchMode(defaultMode)
  setIndicationStateForMode(defaultMode, isActive = true)
}

class FigureControllerTouchDevice(
  protected val figure: Figure,
  touchActions: TouchActions,
) extends FigureController with CommonFigureControllerReactions {

  figure.addMouseListener(new MouseListener {
    def mouseExited(mouseEvent: MouseEvent): Unit = {}

    def mouseClicked(mouseEvent: MouseEvent): Unit = {}

    def mouseEntered(mouseEvent: MouseEvent): Unit = {}

    def mousePressed(mouseEvent: MouseEvent): Unit = {
      val point = mouseEvent.getPoint
      if (SwingUtilities.isLeftMouseButton(mouseEvent)) {
        figure.requestFocusInWindow()
        controllersForPlotsHitAt(point).foreach { controller =>
          touchActions.getTouchMode match {
            case TouchMode.Drag => controller.handleDragStart(ScreenPoint(point))
            case TouchMode.ZoomRectangle => controller.handleZoomRectangleStart(ScreenPoint(point))
            case TouchMode.Zoom(direction, shouldZoomX, shouldZoomY) => {
              controller.handleZoom(ScreenPoint(point), direction, shouldZoomX, shouldZoomY)
            }
          }
        }
      }
    }

    def mouseReleased(mouseEvent: MouseEvent): Unit = {}
  })
}

class FigureControllerTouchDevicePicksOnly(protected val figure: Figure) extends
  FigureController with CommonFigureControllerReactions {
  figure.addMouseListener(new MouseListener {
    def mouseExited(mouseEvent: MouseEvent): Unit = {}

    def mouseClicked(mouseEvent: MouseEvent): Unit = {}

    def mouseEntered(mouseEvent: MouseEvent): Unit = {}

    def mousePressed(mouseEvent: MouseEvent): Unit = {
      val point = mouseEvent.getPoint
      if (SwingUtilities.isLeftMouseButton(mouseEvent)) {
        figure.requestFocusInWindow()
        pickAt(point)
      }
    }

    def mouseReleased(mouseEvent: MouseEvent): Unit = {}
  })
}