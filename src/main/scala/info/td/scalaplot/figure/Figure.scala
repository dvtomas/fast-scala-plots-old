package info.td.scalaplot.figure

import java.awt._

import com.typesafe.scalalogging.LazyLogging
import info.td.common.rx.NonFailingSubscribeErrorHandler
import info.td.scalaplot._
import info.td.scalaplot.plotcontroller.{AutoCADPlotController, PickerPainter, PlotController}
import info.td.scalaplot.utils.{BooleanRichAction, Graphics2DUtils, RichPopupMenu}
import javax.swing.{JComponent, JMenuItem}
import rx.lang.scala.subjects.BehaviorSubject
import rx.lang.scala.{Observable, Subject}

abstract sealed class PlotsContainerChanged

case class PlotAdded(plot: Plot) extends PlotsContainerChanged

case class PicksChanged(picks: Seq[PickInfo])

private case class PlotAndController(plot: Plot, controller: PlotController) {
  def isHit(point: ScreenPoint): Boolean = plot.screenDataBounds isHit point
}

protected class PlotContainer {
  private[this] val changedSubject = Subject[PlotsContainerChanged]()

  def changedSource: Observable[PlotsContainerChanged] = changedSubject

  private[this] var plotsAndControllers: Seq[PlotAndController] = Seq.empty

  def controllersHitAt(point: ScreenPoint): Seq[PlotController] = plotsAndControllers.filter(_.isHit(point)).map(_.controller)

  def controllerForPlot(plot: Plot): PlotController = plotsAndControllers.toList.find(_.plot == plot).map(_.controller).get

  def controllers: Seq[PlotController] = plotsAndControllers.map(_.controller)

  def plots: Seq[Plot] = plotsAndControllers.map(_.plot)

  def addPlot(plot: Plot, controller: PlotController): Unit = {
    plotsAndControllers +:= PlotAndController(plot, controller)
    changedSubject onNext PlotAdded(plot)
  }
}

class Figure()(implicit valueFormatterLocale: ValueFormatterLocale) extends JComponent with LazyLogging {
  private val picksChangedSubject = Subject[PicksChanged]()

  def picksChangedSource: Observable[PicksChanged] = picksChangedSubject

  // TODO this looks ugly.
  def linkPickers(): Unit = {
    plotsContainer.controllers.collect { case controller: AutoCADPlotController => controller } foreach { targetController =>
      targetController.setLinkedControllers(plotsContainer.controllers filter (_ ne targetController))
    }
  }

  setFocusable(true)

  setMinimumSize(new Dimension(100, 100))

  createController()

  protected[figure] val plotsContainer: PlotContainer = new PlotContainer

  private var needsRepainting: Map[Plot, Boolean] = Map.empty

  private val screenBoundsSubject = BehaviorSubject[ScreenRectangle](ScreenRectangle.zero)

  def screenBoundsSource: Observable[ScreenRectangle] = screenBoundsSubject

  private def screenBounds = screenBoundsSubject.first.toBlocking.head

  val paintLock = new Object()

  private var tripleBufferImage = Graphics2DUtils.createCompatibleImage(1, 1)

  protected def createController(): FigureController = new FigureControllerMouse(this)

  val antiAliasedRenderingAction = new BooleanRichAction(fastScalaPlotsPo.t("Antialiased rendering"))
  antiAliasedRenderingAction setSelected true

  protected def antiAliasedRendering: Boolean = antiAliasedRenderingAction.selected

  val pickInfoActions = new PickInfoActions

  // If you see this color, there's probably something wrong going on with plots drawn over figure
  protected def backgroundColor: Color = Color.ORANGE

  protected def paintComponent(g: Graphics2D, forceRepaint: Boolean = false): Unit = {
    Graphics2DUtils.setAntiAliasedRendering(g, antiAliasedRendering)

    var repaintFigure = forceRepaint

    if (screenBounds.width != getWidth || screenBounds.height != getHeight) {
      screenBoundsSubject.onNext(ScreenRectangle(0, 0, getWidth, getHeight))
      tripleBufferImage = Graphics2DUtils.createCompatibleImage(screenBounds.width.toInt, screenBounds.height.toInt)
      plotsContainer.plots.foreach(plot => needsRepainting = needsRepainting.updated(plot, true))
      repaintFigure = true
    }

    val tripleBufferG = tripleBufferImage.createGraphics()
    if (repaintFigure) {
      tripleBufferG.setColor(backgroundColor)
      tripleBufferG.setComposite(AlphaComposite.Src)
      tripleBufferG.fillRect(0, 0, tripleBufferImage.getWidth, tripleBufferImage.getHeight)
    }

    Graphics2DUtils.setAntiAliasedRendering(tripleBufferG, antiAliasedRendering)
    for (plot <- plotsContainer.plots if needsRepainting.getOrElse(plot, false)) {
      plot.paintComponent(tripleBufferG)
      needsRepainting = needsRepainting.updated(plot, false)
    }

    g.drawImage(tripleBufferImage, null, 0, 0)
    plotsContainer.controllers.foreach(_.paintControllerOverlay(g, pickInfoActions.settings))
  }

  override protected def paintComponent(g: Graphics): Unit = {
    paintLock.synchronized {
      paintComponent(g.asInstanceOf[Graphics2D], forceRepaint = false)
    }
    val picks = for (
      plot <- plotsContainer.plots;
        pickerPosition <- plotsContainer.controllerForPlot(plot).pickerPosition.toList;
        part <- plot.parts;
        pick <- part.pick(plot.screenPoint(pickerPosition))
    ) yield pick
    picksChangedSubject.onNext(PicksChanged(picks))
  }

  def repaintAllPlots(): Unit = {
    needsRepainting = plotsContainer.plots.map(_ -> true).toMap
    repaint()
  }

  def repaintPlot(plot: Plot): Unit = {
    needsRepainting = needsRepainting.updated(plot, true)
    repaint()
  }

  private[figure] def popupMenuItems: Seq[JMenuItem] = Seq(
    RichPopupMenu.newCheckMenuItem(antiAliasedRenderingAction),
    pickInfoActions.actionsSubMenu
  )

  def controllerForPlot(plot: Plot): PlotController = plotsContainer.controllerForPlot(plot)

  plotsContainer.changedSource.subscribe(what =>
    what match {
      case PlotAdded(plot) =>
        controllerForPlot(plot).plotOverlayChangedSource.subscribe(_ => repaint(), NonFailingSubscribeErrorHandler)
        repaintPlot(plot)
        plot.changedSource.subscribe(_ => repaintPlot(plot))
    },
    NonFailingSubscribeErrorHandler
  )

  antiAliasedRenderingAction.actionEventsSource.subscribe(_ => repaintAllPlots(), NonFailingSubscribeErrorHandler)

  def addPlotCreateAutoCADControllerAndSubscribeAxes(plot: Plot, pickerPainter: PickerPainter): AutoCADPlotController = {
    val controller = new AutoCADPlotController(plot, pickerPainter)
    plot.axisX.subscribeToDataRangeSource(controller.axisXDataRangeSource)
    plot.axisY.subscribeToDataRangeSource(controller.axisYDataRangeSource)
    plotsContainer.addPlot(plot, controller)
    controller
  }

  def addPlot(plot: Plot, controller: PlotController): Unit = {
    plotsContainer.addPlot(plot, controller)
  }
}