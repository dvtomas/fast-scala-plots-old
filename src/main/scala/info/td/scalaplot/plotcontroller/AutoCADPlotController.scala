package info.td.scalaplot.plotcontroller

import java.awt.{Color, Graphics2D}

import info.td.scalaplot._
import info.td.scalaplot.figure.{ControllerAndPickerPosition, PickInfoSettings}
import info.td.scalaplot.utils._
import javax.swing.{DefaultBoundedRangeModel, JMenuItem}
import rx.lang.scala.subjects.BehaviorSubject
import rx.lang.scala.{Observable, Subject}

import scala.concurrent.duration.Duration

private sealed class ActionInProgress

private case object NoActionInProgress extends ActionInProgress

private case class DragActionInProgress(
  plot: Plot,
  originalDataRangeX: DataRange,
  originalTransformationX: AxisTransformation,
  dataX: Double,
  originalDataRangeY: DataRange,
  originalTransformationY: AxisTransformation,
  dataY: Double
) extends ActionInProgress

private case class ZoomRectangleActionInProgress(plot: Plot, origin: ScreenPoint, corner: ScreenPoint) extends ActionInProgress {
  def zoomRectangleSource: Observable[ScreenRectangle] = plot.screenDataBoundsSource map (_ intersection ScreenRectangle(origin, corner))

  def zoomRectangle: ScreenRectangle = zoomRectangleSource.first.toBlocking.head
}

class AutoCADPlotController(plot: Plot, pickerPainter: PickerPainter) extends PlotController {
  private[this] var keepSquareAspectRatio: Boolean = false

  private[this] var squareAspectRatioSubscriptionActivated = false

  def keepsSquareAspectRatio: Boolean = keepSquareAspectRatio

  /** This only makes sense with Linear/MirroredLinear axis ! */
  def setKeepSquareAspectRatio(value: Boolean): Unit = {
    keepSquareAspectRatio = value
    if (keepSquareAspectRatio) {
      if (!squareAspectRatioSubscriptionActivated) {
        squareAspectRatioSubscriptionActivated = true
        // This will force data ranges recalculation to have square aspect ratio on screen bounds change
        // The delay and throttle is basically a hack. The more important part is delaying the recomputation a bit.
        // We want the recomputation to happen AFTER the values have propagated to both axes, and as I was
        // too lazy to do a big reorganization of the code to make this reliably happen, I just delay the recomputation
        // by an ad-hoc duration.
        axisX.screenBoundsSource
          .delay(Duration.fromNanos(100000000))
          .throttleLast(Duration.fromNanos(100000000))
          .subscribe(_ => setDataRanges(axisX.dataRange, axisY.dataRange))
      }

      setDataRanges(axisX.dataRange, axisY.dataRange)
    }
  }

  // TODO this looks ugly.
  def setLinkedControllers(controllers: Seq[PlotController]): Unit = {
    linkedControllers = controllers
  }

  private val actionInProgressSubject = BehaviorSubject[ActionInProgress](NoActionInProgress)

  private[scalaplot] def setMaybeControllerAndPickerPosition(maybeControllerAndPickerPosition: Option[ControllerAndPickerPosition]): Unit = {
    maybeControllerAndPickerPositionSubject onNext maybeControllerAndPickerPosition
  }

  private[this] val maybeControllerAndPickerPositionSubject = BehaviorSubject[Option[ControllerAndPickerPosition]](None)

  private[this] var linkedControllers: Seq[PlotController] = Seq.empty

  private[this] def isPickForMe(controllerAndPickerPosition: ControllerAndPickerPosition) = {
    (controllerAndPickerPosition.controller == this) || (linkedControllers contains controllerAndPickerPosition.controller)
  }

  def pickerPositionSource: Observable[Option[DataPoint]] = maybeControllerAndPickerPositionSubject.map(maybeControllerAndPickerPosition =>
    maybeControllerAndPickerPosition.flatMap(controllerAndPickerPosition =>
      if (isPickForMe(controllerAndPickerPosition))
        Some(plot dataPoint controllerAndPickerPosition.screenPoint)
      else
        None
    ))

  private val axisXDataRangeSubject = Subject[DataRange]()

  private val axisYDataRangeSubject = Subject[DataRange]()

  def axisXDataRangeSource: Observable[DataRange] = axisXDataRangeSubject

  def axisYDataRangeSource: Observable[DataRange] = axisYDataRangeSubject

  def plotOverlayChangedSource: Observable[PlotOverlayChanged] =
    (pickerPositionSource merge actionInProgressSubject)
      .map(_ => PlotOverlayChanged(plot))

  private def axisX = plot.axisX

  private def axisY = plot.axisY

  private val zoomHistory = new ZoomHistory

  private def rememberZoom(): Unit = {
    zoomHistory.rememberZoom(axisX.dataRange, axisY.dataRange)
  }

  def deleteZoomHistory(): Unit = {
    zoomHistory.deleteZoomHistory()
  }

  private def setActionInProgress(action: ActionInProgress): Unit = {
    actionInProgressSubject onNext action
  }

  private def actionInProgress = actionInProgressSubject.first.toBlocking.head

  def handleZoom(point: ScreenPoint, direction: Int, shouldZoomX: Boolean, shouldZoomY: Boolean): Unit = {
    val zoom = if (direction > 0) 0.2 else -0.2
    val zoomX = if (shouldZoomX) zoom else 0.0
    val zoomY = if (shouldZoomY) zoom else 0.0

    val newDataRangeX = axisX.zoomed(point.x, zoomX)
    val newDataRangeY = axisY.zoomed(point.y, zoomY)

    axisXDataRangeSubject.onNext(newDataRangeX)
    axisYDataRangeSubject.onNext(
      if (keepSquareAspectRatio) {
        val idealNewAxisYTransformation = axisY.transformation.update(axisY.screenRange, newDataRangeY)
        otherDataRangeToKeepSquareAspectRatio(
          axisX.dataRange, axisX.screenRange, idealNewAxisYTransformation, axisY.screenRange, point.y
        )
      } else {
        newDataRangeY
      }
    )
  }


  def redraw(): Unit = {
    setXDataRange(axisX.dataRange)
  }

  def handleZoomRectangleStart(point: ScreenPoint): Unit = {
    setActionInProgress(ZoomRectangleActionInProgress(plot, point, point))
  }

  def handleDragStart(point: ScreenPoint): Unit = {
    setActionInProgress(DragActionInProgress(
      plot,
      axisX.dataRange,
      axisX.transformation,
      axisX.dataValue(point.x),
      axisY.dataRange,
      axisY.transformation,
      axisY.dataValue(point.y)
    ))
  }

  def handleFinishAction(): Unit = {
    actionInProgress match {
      case zoomAction@ZoomRectangleActionInProgress(_, _, _) => {
        val zoomRectangle = zoomAction.zoomRectangle
        handleCancelAction()
        if (zoomRectangle.width > 5 && zoomRectangle.height > 5) {
          rememberZoom()
          setDataRanges(
            DataRange(axisX.dataValue(zoomRectangle.left), axisX.dataValue(zoomRectangle.right)).ascending,
            DataRange(axisY.dataValue(zoomRectangle.bottom), axisY.dataValue(zoomRectangle.top)).ascending
          )
        }
      }
      case _ => handleCancelAction()
    }
  }

  def handleCancelAction(): Unit = {
    setActionInProgress(NoActionInProgress)
  }

  def handleMouseDragged(point: ScreenPoint): Unit = {
    actionInProgress match {
      case ZoomRectangleActionInProgress(_, origin, _) => {
        setActionInProgress(ZoomRectangleActionInProgress(plot, origin, point))
      }
      case drag: DragActionInProgress => {
        setDataRanges(
          axisX.dragged(drag.originalDataRangeX, drag.dataX, drag.originalTransformationX.dataValue(point.x)),
          axisY.dragged(drag.originalDataRangeY, drag.dataY, drag.originalTransformationY.dataValue(point.y))
        )
      }
      case _ =>
    }
  }

  final def pickerPosition: Option[DataPoint] = pickerPositionSource.first.toBlocking.head

  protected[this] def undoZoom(): Unit = {
    zoomHistory.getAndForgetLastZoom foreach { zoom => setDataRanges(zoom.xBounds, zoom.yBounds) }
  }

  def menuItems: List[JMenuItem] = List(
    RichPopupMenu.newMenuItem(
      fastScalaPlotsPo.t("Toggle legend"),
      enabled = true,
      () => {
        plot.setLegendEnabled(!plot.isLegendEnabled)
        redraw()
      }
    ),
    RichPopupMenu.newMenuItem(fastScalaPlotsPo.t("Fit graph"), enabled = true, () => fitDataToScreen()),
    RichPopupMenu.newMenuItem(fastScalaPlotsPo.t("Fit graph along Y axis"), enabled = true, () => fitYToScreen()),
    RichPopupMenu.newMenuItem(fastScalaPlotsPo.t("Undo zoom"), zoomHistory.canUndoZoom, () => undoZoom()),
  )

  private[this] def maybeExpandedRange(shouldExpand: Boolean)(range: DataRange) = {
    if (range.length == 0) { // If the range is empty, make it one data unit long, so that all the Fit/Fit Y commands work sensibly.
      DataRange(range.minimum - 0.5, range.maximum + 0.5)
    } else {
      if (shouldExpand)
        range.expandToNearestTenth
      else
        range
    }
  }

  /** Better use expandToNearestTenth with linear axis only. It may not do what you want with e. g. log axis. */
  def fitXToScreen(expandToNearestTenth: Boolean = false): Unit = {
    for (range <- plot.totalXDataRange) {
      rememberZoom()
      setXDataRange(maybeExpandedRange(expandToNearestTenth)(range))
    }
  }

  /** Better use expandToNearestTenth with linear axis only. It may not do what you want with e. g. log axis. */
  def fitYToScreen(expandToNearestTenth: Boolean = false): Unit = {
    for (range <- plot.yDataRangeAt(axisX.dataRange)) {
      rememberZoom()
      setYDataRange(maybeExpandedRange(expandToNearestTenth)(range))
    }
  }

  /** Better use expandToNearestTenth with linear axis only. It may not do what you want with e. g. log axis. */
  def fitDataToScreen(expandToNearestTenth: Boolean = false): Unit = {
    for (
      rangeX <- plot.totalXDataRange;
        rangeY <- plot.totalYDataRange
    ) {
      rememberZoom()
      val maybeExpand: DataRange => DataRange = maybeExpandedRange(expandToNearestTenth)
      setDataRanges(maybeExpand(rangeX), maybeExpand(rangeY))
    }
  }

  private[this] def otherDataRangeToKeepSquareAspectRatio(
    referenceDataRange: DataRange,
    referenceScreenRange: ScreenRange,
    otherAxis: AxisTransformation,
    otherScreenRange: ScreenRange,
    otherReferenceScreenPoint: Double,
  ): DataRange = {
    val k = -referenceDataRange.length / referenceScreenRange.length

    val result =
      if (k.isFinite) {
        val otherReferenceData = otherAxis.dataValue(otherReferenceScreenPoint)
        DataRange(
          otherReferenceData - k * (otherReferenceScreenPoint - otherScreenRange.minimum),
          otherReferenceData + k * (otherScreenRange.maximum - otherReferenceScreenPoint)
        )
      } else {
        DataRange(
          otherAxis.dataValue(otherScreenRange.minimum),
          otherAxis.dataValue(otherScreenRange.maximum)
        )
      }
    if (result.minimum > result.maximum) {
      DataRange(result.maximum, result.minimum)
    } else {
      result
    }
  }

  def setXDataRange(dataRange: DataRange): Unit = {
    axisXDataRangeSubject.onNext(dataRange)
    if (keepSquareAspectRatio) {
      axisYDataRangeSubject.onNext(otherDataRangeToKeepSquareAspectRatio(
        axisX.dataRange, axisX.screenRange, axisY.transformation, axisY.screenRange, axisY.screenRange.center
      ))
    }
  }

  def setYDataRange(dataRange: DataRange): Unit = {
    axisYDataRangeSubject.onNext(dataRange)
    if (keepSquareAspectRatio) {
      axisXDataRangeSubject.onNext(otherDataRangeToKeepSquareAspectRatio(
        dataRange, axisY.screenRange, axisX.transformation, axisX.screenRange, axisX.screenRange.center
      ))
    }
  }

  def setDataRanges(xDataRange: DataRange, yDataRange: DataRange): Unit = {
    axisXDataRangeSubject.onNext(xDataRange)
    axisYDataRangeSubject.onNext(
      if (keepSquareAspectRatio) {
        val idealNewAxisYTransformation = axisY.transformation.update(axisY.screenRange, yDataRange)
        otherDataRangeToKeepSquareAspectRatio(
          axisX.dataRange, axisX.screenRange, idealNewAxisYTransformation, axisY.screenRange, axisY.screenRange.center
        )
      } else {
        yDataRange
      }
    )
  }

  def paintControllerOverlay(g: Graphics2D, pickInfoSettings: PickInfoSettings)(implicit valueFormatterLocale: ValueFormatterLocale): Unit = {
    maybeDrawZoomRectangle(g)
    maybeDrawPickInfo(g, pickInfoSettings)
  }

  private def maybeDrawZoomRectangle(g: Graphics2D): Unit = {
    actionInProgress match {
      case action: ZoomRectangleActionInProgress => {
        def rectangleProperties = TranslucentRectangleBackground(Color.BLUE, 0.1f)

        rectangleProperties.draw(g, action.zoomRectangle)
      }
      case _ =>
    }
  }

  protected[this] def pickStringFor(pickInfo: PickInfo, alsoDisplayXValue: Boolean)(implicit valueFormatterLocale: ValueFormatterLocale): String = {
    def plotTitle = pickInfo.plotPart.plotTitle.string

    (plotTitle.isEmpty, alsoDisplayXValue) match {
      case (false, false) => s"$plotTitle: ${pickInfo.yValueString}"
      case (false, true) => s"$plotTitle(${pickInfo.xValueString})=${pickInfo.yValueString}"
      case (true, false) => s"${pickInfo.yValueString}"
      case (true, true) => s"y(${pickInfo.xValueString})=${pickInfo.yValueString}"
    }
  }

  protected[this] def pickRichStringFor(pickInfo: PickInfo, alsoDisplayX: Boolean)(implicit valueFormatterLocale: ValueFormatterLocale): RichStringWithBackground = {
    val alpha = 0.8f
    val fontColor = Graphics2DUtils.readableForegroundColorForBackground(pickInfo.color, 1.0f / alpha)
    new RichStringWithBackground(
      pickStringFor(pickInfo, alsoDisplayX),
      TranslucentRectangleBackground(pickInfo.color, alpha, Color.BLACK, 1.0f),
      FontProperties(color = fontColor)
    )
  }

  private[this] def maybeDrawPickInfo(g: Graphics2D, pickInfoSettings: PickInfoSettings)(implicit valueFormatterLocale: ValueFormatterLocale): Unit = {
    pickerPosition match {
      case Some(position) => {
        if (pickInfoSettings.drawPicker) {
          pickerPainter.drawPicker(g, plot, plot.screenPoint(position), pickInfoSettings.drawPickerPosition)
        }
        val picks = for (part <- plot.parts; pick <- part.pick(plot.screenPoint(position))) yield pick
        Graphics2DUtils.withClipDo(g, plot.screenDataBounds) {
          picks.foreach(pick => {
            if (pickInfoSettings.highlightPoint) {
              PointStyleDiamond(Color.RED, 2).drawPoint(g, pick.position)
            }
            if (pickInfoSettings.drawPickValue) {
              pickRichStringFor(pick, pickInfoSettings.alsoDisplayXValue).drawAtAndLeaningTo(
                g,
                pick.position,
                ScreenPoint(plot.axisX.screenRange.center, plot.axisY.screenRange.center)
              )
            }
          })
        }
      }
      case None =>
    }
  }

  lazy val subscribeAxisXToAndGetBoundedRangeModel: DefaultBoundedRangeModel = {
    val bridge = new HorizontalBoundedRangeModelBridge(axisX.dataRangeSource, plot.totalXDataRangeSource)
    axisX.subscribeToDataRangeSource(bridge.outputVisibleRangeSource merge axisXDataRangeSource)
    bridge.boundedRangeModel
  }

  lazy val subscribeAxisYToAndGetBoundedRangeModel: DefaultBoundedRangeModel = {
    val bridge = new VerticalBoundedRangeModelBridge(axisY.dataRangeSource, plot.totalYDataRangeSource)
    axisY.subscribeToDataRangeSource(bridge.outputVisibleRangeSource merge axisYDataRangeSource)
    bridge.boundedRangeModel
  }
}