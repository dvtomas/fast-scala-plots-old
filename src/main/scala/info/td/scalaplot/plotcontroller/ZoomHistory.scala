package info.td.scalaplot.plotcontroller

import info.td.scalaplot.DataRange

case class ZoomSetting(xBounds: DataRange, yBounds: DataRange)

class ZoomHistory {

  private var zoomHistory: List[ZoomSetting] = Nil

  def rememberZoom(axisXDataRange: DataRange, axisYDataRange: DataRange): Unit = {
    zoomHistory ::= ZoomSetting(axisXDataRange, axisYDataRange)
  }

  def deleteZoomHistory(): Unit = {
    zoomHistory = Nil
  }

  def canUndoZoom: Boolean = zoomHistory.nonEmpty

  def getAndForgetLastZoom: Option[ZoomSetting] = {
    zoomHistory match {
      case zoom :: rest => {
        zoomHistory = rest
        Some(zoom)
      }
      case _ => None
    }
  }
}