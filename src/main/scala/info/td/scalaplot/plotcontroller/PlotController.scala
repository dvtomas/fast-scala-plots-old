package info.td.scalaplot.plotcontroller

import info.td.scalaplot._
import info.td.scalaplot.figure.{ControllerAndPickerPosition, PickInfoSettings}
import rx.lang.scala.Observable

import java.awt.Graphics2D
import javax.swing.JMenuItem

case class PlotOverlayChanged(plot: Plot)

// TODO the whole PlotController seems to be an ugly mess of things that don't belong to the superclass......
abstract class PlotController {
  def paintControllerOverlay(g: Graphics2D, pickInfoSettings: PickInfoSettings)(implicit valueFormatterLocale: ValueFormatterLocale): Unit

  def pickerPosition: Option[DataPoint]

  def plotOverlayChangedSource: Observable[PlotOverlayChanged]

  def handleZoom(point: ScreenPoint, direction: Int, shouldZoomX: Boolean, shouldZoomY: Boolean): Unit

  def handleZoomRectangleStart(point: ScreenPoint): Unit

  def handleDragStart(point: ScreenPoint): Unit

  def handleMouseDragged(point: ScreenPoint): Unit

  def handleCancelAction(): Unit

  def handleFinishAction(): Unit

  def createMenuItems(pickerPosition: Option[DataPoint]): Seq[JMenuItem]

  private[scalaplot] def setMaybeControllerAndPickerPosition(maybeControllerAndPickerPosition: Option[ControllerAndPickerPosition]): Unit
}



