package info.td.scalaplot.plotcontroller

import java.awt.geom.Line2D
import java.awt.{BasicStroke, Color, Graphics2D}

import info.td.scalaplot._
import info.td.scalaplot.utils.Graphics2DHelpers._
import info.td.scalaplot.utils._

abstract class PickerPainter {
  def drawPicker(g: Graphics2D, plot: Plot, position: ScreenPoint, drawPickerPosition: Boolean)(implicit valueFormatterLocale: ValueFormatterLocale): Unit

  protected[this] def richStringFor(string: String): RichString = {
    val alpha = 0.8f
    val backgroundColor = new Color(220, 220, 220)
    val fontColor = Graphics2DUtils.readableForegroundColorForBackground(backgroundColor, 1.0f / alpha)
    new RichStringWithBackground(
      string,
      TranslucentRectangleBackground(backgroundColor, alpha, Color.BLACK, 1.0f),
      FontProperties(color = fontColor)
    )
  }

  private[this] def drawDataValue(g: Graphics2D, digitChange: DigitChange, plot: Plot, position: ScreenPoint)(implicit valueFormatterLocale: ValueFormatterLocale): Unit = {
    val valueString = ValueFormatter valueString digitChange
    richStringFor(valueString).drawAtAndLeaningTo(
      g,
      position,
      ScreenPoint(plot.axisX.screenRange.center, plot.axisY.screenRange.center)
    )
  }

  protected[this] def drawXDataValue(g: Graphics2D, plot: Plot, x: Double)(implicit valueFormatterLocale: ValueFormatterLocale): Unit = {
    drawDataValue(g, plot.axisX digitChange x, plot, ScreenPoint(x, plot.screenDataBounds.bottom))
  }

  protected[this] def drawYDataValue(g: Graphics2D, plot: Plot, y: Double)(implicit valueFormatterLocale: ValueFormatterLocale): Unit = {
    drawDataValue(g, plot.axisY digitChange y, plot, ScreenPoint(plot.screenDataBounds.left, y))
  }

  protected[this] def drawPickerLine(g: Graphics2D, x1: Double, y1: Double, x2: Double, y2: Double): Unit = {
    g.setColor(Color.GRAY)
    g.setStroke(new BasicStroke(1))
    g.draw(new Line2D.Double(x1, y1, x2, y2))
  }
}

object CrossHairPickerPainter extends PickerPainter {
  def drawPicker(g: Graphics2D, plot: Plot, position: ScreenPoint, drawPickerPosition: Boolean)(implicit valueFormatterLocale: ValueFormatterLocale): Unit = {
    val bounds = plot.screenDataBounds
    val x = position.x.snappedToPixelCenter
    val y = position.y.snappedToPixelCenter
    if (bounds.isHorizontalHit(x)) {
      drawPickerLine(g, x, bounds.top, x, bounds.bottom)
      if (drawPickerPosition) {
        drawXDataValue(g, plot, x)
      }
    }
    if (bounds.isVerticalHit(y)) {
      drawPickerLine(g, bounds.left, y, bounds.right, y)
      if (drawPickerPosition) {
        drawYDataValue(g, plot, y)
      }
    }
  }
}

object VerticalPickerPainter extends PickerPainter {
  def drawPicker(g: Graphics2D, plot: Plot, position: ScreenPoint, drawPickerPosition: Boolean)(implicit valueFormatterLocale: ValueFormatterLocale): Unit = {
    val bounds = plot.screenDataBounds
    val x = position.x.snappedToPixelCenter
    if (bounds.isHorizontalHit(x)) {
      drawPickerLine(g, x, bounds.top, x, bounds.bottom)
      if (drawPickerPosition) {
        drawXDataValue(g, plot, x)
      }
    }
  }
}

object NullPickerPainter extends PickerPainter {
  def drawPicker(g: Graphics2D, plot: Plot, position: ScreenPoint, drawPickerPosition: Boolean)(implicit valueFormatterLocale: ValueFormatterLocale): Unit = {}
}
