package info.td.scalaplot.plotcontroller

import java.awt.Graphics2D

import info.td.scalaplot.figure.{ControllerAndPickerPosition, PickInfoSettings}
import info.td.scalaplot.{DataPoint, ScreenPoint, ValueFormatterLocale}
import javax.swing.JMenuItem
import rx.lang.scala.Observable

class NullPlotController extends PlotController {
  def paintControllerOverlay(g: Graphics2D, pickInfoSettings: PickInfoSettings)(implicit valueFormatterLocale: ValueFormatterLocale): Unit = {}

  final def pickerPosition: Option[DataPoint] = None

  def plotOverlayChangedSource: Observable[PlotOverlayChanged] = Observable.empty

  def handleZoom(point: ScreenPoint, direction: Int, shouldZoomX: Boolean, shouldZoomY: Boolean): Unit = {}

  def handleZoomRectangleStart(point: ScreenPoint): Unit = {}

  def handleDragStart(point: ScreenPoint): Unit = {}

  def handleMouseDragged(point: ScreenPoint): Unit = {}

  def handleCancelAction(): Unit = {}

  def handleFinishAction(): Unit = {}

  def menuItems: List[JMenuItem] = List.empty

  private[scalaplot] def setMaybeControllerAndPickerPosition(maybeControllerAndPickerPosition: Option[ControllerAndPickerPosition]): Unit = {}
}