package info.td.scalaplot.trampoline

import java.awt._
import java.awt.event._
import java.util.Date

import javax.swing._
import javax.swing.plaf.basic.BasicArrowButton
import net.miginfocom.swing.MigLayout

private abstract sealed class TrampolineState

private case object TrampolineStateIdle extends TrampolineState

private case class TrampolineStateMoving(pagesPerSecond: Double, lastUpdateMillisecond: Long) extends TrampolineState

protected[this] trait BoundedRangeModelHelpers {

  implicit protected[this] class BoundedRangeModelWrapper(boundedRangeModel: BoundedRangeModel) {
    def valueWithSafelyAddedOffset(offset: Int): Int = {
      val longValue = boundedRangeModel.getValue.toLong
      val longOffset = offset.toLong
      val longMinimum = boundedRangeModel.getMinimum
      val longMaximum = boundedRangeModel.getMaximum.toLong - boundedRangeModel.getExtent.toLong
      (((longValue + longOffset) max longMinimum) min longMaximum).toInt
    }

    def setValueWithSafelyAddedOffset(offset: Int): Unit = {
      boundedRangeModel.setValue(valueWithSafelyAddedOffset(offset))
    }
  }

}

protected[this] trait TrampolineHelpers extends BoundedRangeModelHelpers {
  protected[this] def boundedRangeModel: BoundedRangeModel

  protected[this] val borderColor: Color = Color.BLACK
  protected[this] val edgeColor: Color = Color.ORANGE
  protected[this] val neutralColor: Color = Color.CYAN

  protected[this] def blendedColor(c1: Color, c2: Color, ratio: Double, isEnabled: Boolean): Color = {
    def blendComponent(a: Int, b: Int) = ((b - a) * ratio + a).toInt

    val blended = new Color(blendComponent(c1.getRed, c2.getRed), blendComponent(c1.getGreen, c2.getGreen), blendComponent(c1.getBlue, c2.getBlue))
    if (isEnabled) {
      blended
    } else {
      val bw = (blended.getRed * 0.21 + blended.getGreen * 0.71 + blended.getBlue * 0.07).toInt
      new Color(bw, bw, bw)
    }
  }
}

object TrampolineWithTapButtons {

  case class ArrowButtonCallbackParameters(direction: ArrowButtonDirection, boundedRangeModel: BoundedRangeModel) extends BoundedRangeModelHelpers {
    def valueWithSafelyAddedOffset(offset: Int): Int = boundedRangeModel.valueWithSafelyAddedOffset(offset)
  }

  abstract sealed class ArrowButtonDirection

  object Left extends ArrowButtonDirection

  object Right extends ArrowButtonDirection

  private def stepByOneArrowButtonCallback(parameters: ArrowButtonCallbackParameters): Int = {
    parameters.direction match {
      case Left => parameters.valueWithSafelyAddedOffset(-1)
      case Right => parameters.valueWithSafelyAddedOffset(1)
    }
  }
}

/** Trampoline with TapButtons with customizable step
  *
  * @param boundedRangeModel   Bounded range model to control
  * @param arrowButtonCallback Please, be very careful when computing new values inside the callback. boundedRangeModel.extent can easily be 0 .. MaxInt, so when you add some offset to the current value, you could overflow easily. Use `TrampolineWithTapButtons.ArrowButtonCallbackParameters` helper functions.
  */
class TrampolineWithTapButtons(
  protected[this] val boundedRangeModel: BoundedRangeModel,
  arrowButtonCallback: TrampolineWithTapButtons.ArrowButtonCallbackParameters => Int = TrampolineWithTapButtons.stepByOneArrowButtonCallback
) extends JPanel with TrampolineHelpers {
  private[this] def buttonBackgroundColor = blendedColor(neutralColor, edgeColor, 0.3, isEnabled = true)

  private[this] val initialAutoRepeatDelay = 250
  private[this] val autoRepeatDelay = 100

  private[this] class ArrowButton(orientation: Int) extends BasicArrowButton(orientation, buttonBackgroundColor, Color.gray, Color.darkGray, Color.lightGray) {
    // Make the buttons square
    override def getPreferredSize: Dimension = {
      val c = getParent
      val d = if (c != null) {
        c.getSize
      } else {
        super.getPreferredSize
      }
      val w = d.getWidth
      val h = d.getHeight
      val s = (if (w < h) w else h).toInt
      new Dimension(s, s)
    }

    override def getBackground: Color = {
      if (isEnabled)
        super.getBackground
      else
        Option(UIManager.getColor("Button.background")).getOrElse(Color.PINK)
    }
  }

  protected[this] def computeNewPosition(arrowButtonCallbackParameters: TrampolineWithTapButtons.ArrowButtonCallbackParameters): Int = {
    arrowButtonCallback(arrowButtonCallbackParameters)
  }

  private[this] class ScrollListener extends ActionListener {
    var direction: TrampolineWithTapButtons.ArrowButtonDirection = TrampolineWithTapButtons.Left

    def actionPerformed(e: ActionEvent): Unit = {
      val newPosition = computeNewPosition(TrampolineWithTapButtons.ArrowButtonCallbackParameters(direction, boundedRangeModel))
      if (newPosition == boundedRangeModel.getValue) {
        e.getSource.asInstanceOf[Timer].stop()
      } else {
        boundedRangeModel.setValue(newPosition)
      }
    }
  }

  private[this] val scrollListener = new ScrollListener()
  private[this] val scrollTimer = new Timer(autoRepeatDelay, scrollListener)
  scrollTimer.setInitialDelay(initialAutoRepeatDelay)

  private[this] class ArrowButtonListener(direction: TrampolineWithTapButtons.ArrowButtonDirection) extends MouseAdapter {
    override def mousePressed(e: MouseEvent): Unit = {
      if (isEnabled && SwingUtilities.isLeftMouseButton(e)) {
        val newPosition = computeNewPosition(TrampolineWithTapButtons.ArrowButtonCallbackParameters(direction, boundedRangeModel))
        scrollTimer.stop()
        scrollListener.direction = direction
        if (boundedRangeModel.getValue != newPosition) {
          scrollTimer.start()
          boundedRangeModel.setValue(newPosition)
        }
      }
    }

    override def mouseReleased(e: MouseEvent): Unit = {
      scrollTimer.stop()
      boundedRangeModel.setValueIsAdjusting(false)
    }
  }

  private[this] val leftTapButton = new ArrowButton(SwingConstants.WEST)
  private[this] val rightTapButton = new ArrowButton(SwingConstants.EAST)
  private[this] val trampoline = new Trampoline(boundedRangeModel)
  private[this] val components = Seq(leftTapButton, trampoline, rightTapButton)
  setLayout(new MigLayout("fill, insets 0", "[]1[grow, fill]1[]", "[fill]"))
  components foreach add

  leftTapButton.addMouseListener(new ArrowButtonListener(TrampolineWithTapButtons.Left))
  rightTapButton.addMouseListener(new ArrowButtonListener(TrampolineWithTapButtons.Right))

  override def setEnabled(enabled: Boolean): Unit = {
    super.setEnabled(enabled)
    components.foreach(_.setEnabled(enabled))
  }
}

class Trampoline(protected[this] val boundedRangeModel: BoundedRangeModel) extends JComponent with TrampolineHelpers {
  private var state: TrampolineState = TrampolineStateIdle

  protected val neutralAreaWidth = 30
  setMinimumSize(new Dimension(neutralAreaWidth + 2, 10))
  setPreferredSize(new Dimension(100, 20))

  protected def scrollAreaWidth: Double = (getWidth.toDouble - neutralAreaWidth.toDouble) / 2.0

  /** returns -1.0 .. +1.0 if within bounds, but if we are outside the bounds range, can return bigger numbers. */
  private def movementRatioAt(x: Int): Double = {
    val cachedScrollAreaWidth = scrollAreaWidth
    if (x < cachedScrollAreaWidth) {
      -1.0 * ((cachedScrollAreaWidth - x) / cachedScrollAreaWidth)
    } else if (x < cachedScrollAreaWidth + neutralAreaWidth) {
      0.0
    } else {
      (x - cachedScrollAreaWidth - neutralAreaWidth) / cachedScrollAreaWidth
    }
  }

  override protected def paintComponent(graphics: Graphics): Unit = {
    val g = graphics.asInstanceOf[Graphics2D]

    for (x <- 0 until getWidth) {
      val ratio = movementRatioAt(x).abs min 1.0
      g.setColor(blendedColor(neutralColor, edgeColor, ratio, isEnabled))
      g.drawLine(x, 0, x, getHeight)
    }
    g.setColor(borderColor)
    g.drawLine(scrollAreaWidth.toInt, 0, scrollAreaWidth.toInt, getHeight)
    g.drawLine(scrollAreaWidth.toInt + neutralAreaWidth, 0, scrollAreaWidth.toInt + neutralAreaWidth, getHeight)
    g.drawRect(0, 0, getWidth - 1, getHeight - 1)
  }

  private def maybeUpdateBoundedRangeModel(): Unit = {
    state match {
      case TrampolineStateIdle =>
      case TrampolineStateMoving(pagesPerSecond, lastUpdate) => {
        val pagesToMove = (now - lastUpdate) / 1000.0 * pagesPerSecond
        val pageSize = boundedRangeModel.getExtent
        val offset = (pageSize * pagesToMove).toInt
        if (offset != 0) {
          // If the offset is 0, don't change the state - let it accumulate over time until the offset is > 0
          boundedRangeModel.setValueWithSafelyAddedOffset(offset)
          state = TrampolineStateMoving(pagesPerSecond, now)
        }
        scheduleUpdate()
      }
    }
  }

  private def now: Long = new Date().getTime

  private def scheduleUpdate(): Unit = {
    SwingUtilities.invokeLater(() => maybeUpdateBoundedRangeModel())
  }

  protected val leftMouseButtonSpeed = 5.0
  protected val middleMouseButtonSpeed = 2.0
  protected val rightMouseButtonSpeed = 10.0

  private def setTrampolineMovement(speed: Double, point: Point): Unit = {
    val movementRatioOffset = movementRatioAt(point.x)
    val movementRatio = movementRatioOffset * movementRatioOffset * Math.signum(movementRatioOffset) // parabolic curve is better suited for finer control of lower speeds
    state = TrampolineStateMoving(speed * movementRatio, now)
    scheduleUpdate()
  }

  def speedForButton(event: MouseEvent): Double = {
    if (SwingUtilities.isLeftMouseButton(event)) {
      leftMouseButtonSpeed
    } else if (SwingUtilities.isMiddleMouseButton(event)) {
      middleMouseButtonSpeed
    } else if (SwingUtilities.isRightMouseButton(event)) {
      rightMouseButtonSpeed
    } else {
      0.0
    }
  }

  addMouseListener(new MouseListener {
    def mouseExited(mouseEvent: MouseEvent): Unit = {}

    def mouseClicked(mouseEvent: MouseEvent): Unit = {}

    def mouseEntered(mouseEvent: MouseEvent): Unit = {}

    def mousePressed(mouseEvent: MouseEvent): Unit = {
      if (isEnabled) {
        setTrampolineMovement(speedForButton(mouseEvent), mouseEvent.getPoint)
      }
    }

    def mouseReleased(mouseEvent: MouseEvent): Unit = {
      state = TrampolineStateIdle
    }
  })

  addMouseWheelListener((mouseWheelEvent: MouseWheelEvent) => {
    if (isEnabled) {
      boundedRangeModel.setValueWithSafelyAddedOffset((-mouseWheelEvent.getWheelRotation * 0.25 * boundedRangeModel.getExtent).toInt)
    }
  })

  addMouseMotionListener(new MouseMotionListener {
    def mouseMoved(mouseEvent: MouseEvent): Unit = {}

    def mouseDragged(mouseEvent: MouseEvent): Unit = {
      if (isEnabled) {
        setTrampolineMovement(speedForButton(mouseEvent), mouseEvent.getPoint)
      }
    }
  })
}