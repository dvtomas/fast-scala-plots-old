package info.td.scalaplot.axispainter

import java.awt.Graphics2D

import info.td.scalaplot.utils.{DigitChange, DigitChangeComputer, ValueFormatter}
import info.td.scalaplot.{DataRange, Plot, ValueFormatterLocale}

import scala.Ordering.Double.IeeeOrdering

trait DigitChangeTicksPainter extends AxisPainter {
  protected def createDigitChangeComputer = new DigitChangeComputer

  protected def maximalToMinimalTicksDistanceRatio = 2.0

  private val digitChangeComputer = createDigitChangeComputer

  private def findBiggestMagnitudeChange(screenRange: DataRange): Option[DigitChange] = {
    def assessMagnitudeChange(digitChange: DigitChange) = {
      val bonusForImportantDigit = Array(0.9, 0.1, 0.2, 0.0, 0.0, 0.5, 0.0, 0.1, 0.0, 0.0)
      digitChange.changedExponent + digitChange.changedDigit.map(bonusForImportantDigit).getOrElse(0.0)
    }

    (screenRange.minimum.toInt to screenRange.maximum.toInt)
      .map(value => axis.dataValue(value.toDouble))
      .sliding(2)
      .flatMap {
        case IndexedSeq(left, right) => Seq(digitChangeComputer.digitChange(left, right))
        case _ => Seq.empty
      }
      .maxByOption(assessMagnitudeChange)
  }

  protected def findTickPositions(initialScreenPosition: Double, ticksSettings: TicksSettings): List[DigitChange] = {
    if (axis.screenRange.maximum - initialScreenPosition < 3.0) {
      Nil
    } else {
      val tickDistanceLimit = ticksSettings.minimalTicksDistance * maximalToMinimalTicksDistanceRatio
      findBiggestMagnitudeChange(DataRange(
        initialScreenPosition,
        (initialScreenPosition + tickDistanceLimit) min axis.screenRange.maximum)
      ) match {
        case Some(biggestMagnitudeChange) => {
          val newStart = Math.floor(axis.screenValue(biggestMagnitudeChange.value)) + 1.0 + ticksSettings.minimalTicksDistance
          if (newStart == initialScreenPosition) {
            biggestMagnitudeChange :: Nil // SOE protection in case where the precision of the doubles is not enough to handle too tiny increments
          } else {
            biggestMagnitudeChange :: findTickPositions(newStart, ticksSettings)
          }
        }
        case None => Nil
      }
    }
  }
}

trait DigitChangeMajorTicksPainter extends DigitChangeTicksPainter {
  protected def paintMajorTicks(g: Graphics2D)(implicit valueFormatterLocale: ValueFormatterLocale): Unit = {
    val magnitudeChanges = findTickPositions(axis.screenRange.minimum, majorTicksSettings)
    for (magnitudeChange <- magnitudeChanges) {
      paintMajorTick(g, magnitudeChange.value, ValueFormatter.valueString(magnitudeChange))
    }
  }
}

trait DigitChangeMinorTicksPainter extends DigitChangeTicksPainter {
  protected def paintMinorTicks(g: Graphics2D): Unit = {
    val magnitudeChanges = findTickPositions(axis.screenRange.minimum, minorTicksSettings)
    for (magnitudeChange <- magnitudeChanges)
      paintMinorTick(g, magnitudeChange.value)
  }
}

class DigitChangeAxisXPainter(val plot: Plot) extends DigitChangeMajorTicksPainter with NullMinorTicksAxisPainter with AxisXPainter

class DigitChangeAxisYPainter(val plot: Plot) extends DigitChangeMajorTicksPainter with NullMinorTicksAxisPainter with AxisYPainter