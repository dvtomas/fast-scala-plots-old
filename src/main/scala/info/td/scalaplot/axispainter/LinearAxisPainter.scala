package info.td.scalaplot.axispainter

import java.awt.Graphics2D

import info.td.scalaplot.utils.MathUtils
import info.td.scalaplot.{DataRange, Plot, ValueFormatterLocale}

trait LinearAxisPainterHelpers extends AxisPainter {

  protected case class TicksPlacement(private val _start: Double, private val _end: Double, private val _step: Double) {
    def positionOfDigitThatChanges: Int = {
      val result = if (_start.abs == 0.0) {
        0
      } else {
        MathUtils.decadicExponent(_start) - MathUtils.decadicExponent(Math.abs(_step)) + 1
      }
      result
    }

    def foreach(f: Double => Unit): Unit = {
      val absStepTreshold = Math.abs(_step) / 10.0

      def alignedTick(tick: Double): Double = {
        // Used to get pure zero in case of rounding errors
        if (Math.abs(tick) < absStepTreshold) 0.0 else tick
      }

      // We always want to start at _start. Going from _end to _start backwards is forbidden.
      // _start is computed to be THE nice number, _end is just a backstop.
      var tick = _start

      if (_end > _start) {
        while (tick <= _end) {
          f(alignedTick(tick))
          tick += _step
        }
      } else {
        // _step is negative
        while (tick >= _end) {
          f(alignedTick(tick))
          tick += _step
        }
      }
    }
  }

  protected val reasonableTickDistances: List[Double] = 1.0 :: 2.0 :: 5.0 :: 10.0 :: 20.0 :: 50.0 :: 100.0 :: 200.0 :: 500.0 :: 1000.0 :: Nil

  protected def computeTicksPlacement(minimalTicksDistance: Double, dataRange: DataRange): TicksPlacement = {
    val screenRange = axis.screenRange

    if (Math.abs(screenRange.length) <= 1 || !dataRange.isValidDataRange || dataRange.length == 0.0)
      TicksPlacement(0.0, 0.0, 1.0)
    else {
      val k = dataRange.length / screenRange.length
      val decadicKExponent = MathUtils.decadicExponent(Math.abs(k))
      val decadicKBase = k / Math.pow(10.0, decadicKExponent)
      val absMinimalTicksDistance = Math.abs(decadicKBase * minimalTicksDistance)
      val absTicksDistance = reasonableTickDistances
        .find(_ > absMinimalTicksDistance)
        .getOrElse(reasonableTickDistances.last)
      val ticksDistance = if (k < 0) -absTicksDistance else absTicksDistance
      val step = ticksDistance.toDouble * Math.pow(10.0, decadicKExponent)

      val (start, end) = if (k < 0 ^ dataRange.minimum < dataRange.maximum)
        (dataRange.minimum, dataRange.maximum)
      else
        (dataRange.maximum, dataRange.minimum)

      TicksPlacement(
        ((start + step / 2) / Math.abs(step)).round * Math.abs(step),
        end,
        step
      )
    }
  }
}

trait LinearAxisMinorTicksPainter extends LinearAxisPainterHelpers {
  protected def paintMinorTicks(g: Graphics2D): Unit = {
    val ticksPlacement = computeTicksPlacement(minorTicksSettings.minimalTicksDistance, axis.dataRange)
    ticksPlacement.foreach { tickPosition => paintMinorTick(g, tickPosition) }
  }
}

trait LinearAxisMajorTicksPainter extends LinearAxisPainterHelpers {
  protected def paintMajorTicks(g: Graphics2D)(implicit valueFormatterLocale: ValueFormatterLocale): Unit = {
    val ticksPlacement = computeTicksPlacement(majorTicksSettings.minimalTicksDistance, axis.dataRange)
    val digits = ticksPlacement.positionOfDigitThatChanges max 2
    ticksPlacement.foreach { tickPosition =>
      val valueString = paintingProperties.valueString(tickPosition, digits)
      paintMajorTick(g, tickPosition, valueString)
    }
  }
}

class LinearAxisXPainter(val plot: Plot) extends LinearAxisMinorTicksPainter with LinearAxisMajorTicksPainter with AxisXPainter

class LinearAxisYPainter(val plot: Plot) extends LinearAxisMinorTicksPainter with LinearAxisMajorTicksPainter with AxisYPainter

