package info.td.scalaplot.axispainter

import java.awt.geom.Line2D
import java.awt.{BasicStroke, Color, Graphics2D}

import info.td.scalaplot._
import info.td.scalaplot.utils.Graphics2DHelpers._
import info.td.scalaplot.utils.{FontProperties, RichString, ValueFormatter}

class TicksSettings(val minimalTicksDistance: Double, val tickLength: Double, val color: Color) {
  def paintLeftAndRight(g: Graphics2D, _left: Double, _right: Double, _y: Double): Unit = {
    val (left, right, y) = (_left.snappedToPixelCenter, _right.snappedToPixelCenter, _y.snappedToPixelCenter)
    g.setColor(color)
    g.setStroke(new BasicStroke(1))
    g.draw(new Line2D.Double(left, y, left + tickLength - 1, y))
    g.draw(new Line2D.Double(right - tickLength, y, right - 1, y))
  }

  def paintBottomAndTop(g: Graphics2D, _bottom: Double, _top: Double, _x: Double): Unit = {
    val (bottom, top, x) = (_bottom.snappedToPixelCenter, _top.snappedToPixelCenter, _x.snappedToPixelCenter)
    g.setColor(color)
    g.setStroke(new BasicStroke(1))
    g.draw(new Line2D.Double(x, bottom - 1, x, bottom - tickLength))
    g.draw(new Line2D.Double(x, top, x, top + tickLength - 1))
  }
}

case object MinorTicksSettingsX extends TicksSettings(5, 2, Color.LIGHT_GRAY)

case object MajorTicksSettingsX extends TicksSettings(80, 3, Color.BLACK)

case object MinorTicksSettingsY extends TicksSettings(5, 2, Color.LIGHT_GRAY)

case object MajorTicksSettingsY extends TicksSettings(30, 3, Color.BLACK)

case class AxisPaintingProperties() {
  val minorTicksSettingsX: TicksSettings = MinorTicksSettingsX

  val majorTicksSettingsX: TicksSettings = MajorTicksSettingsX

  val minorTicksSettingsY: TicksSettings = MinorTicksSettingsY

  val majorTicksSettingsY: TicksSettings = MajorTicksSettingsY

  val fontProperties: FontProperties = FontProperties()

  def valueString(value: Double, digits: Int)(implicit valueFormatterLocale: ValueFormatterLocale): String =
    ValueFormatter.valueString(value, digits)
}

trait AxisPainter {
  protected def plot: Plot

  protected def axis: Axis

  protected def paintingProperties = new AxisPaintingProperties

  protected def paintMinorTick(g: Graphics2D, value: Double): Unit

  protected def paintMajorTick(g: Graphics2D, value: Double, string: String)(implicit valueFormatterLocale: ValueFormatterLocale): Unit

  protected def minorTicksSettings: TicksSettings

  protected def majorTicksSettings: TicksSettings

  protected def paintMinorTicks(g: Graphics2D): Unit

  protected def paintMajorTicks(g: Graphics2D)(implicit valueFormatterLocale: ValueFormatterLocale): Unit

  def paint(g: Graphics2D)(implicit valueFormatterLocale: ValueFormatterLocale): Unit = {
    paintMinorTicks(g)
    paintMajorTicks(g)
  }

  protected def scientificNotationRichString(string: String) =
    new RichString(string, paintingProperties.fontProperties) with RichString.ScientificNotationRichString
}

trait AxisXPainter extends AxisPainter {
  protected def axis: Axis = plot.axisX

  protected def majorTicksSettings: TicksSettings = paintingProperties.majorTicksSettingsX

  protected def minorTicksSettings: TicksSettings = paintingProperties.minorTicksSettingsX

  protected def paintMinorTick(g: Graphics2D, value: Double): Unit = {
    minorTicksSettings.paintBottomAndTop(
      g,
      plot.screenDataBounds.bottom,
      plot.screenDataBounds.top,
      axis.screenValue(value)
    )
  }

  protected def paintMajorTick(g: Graphics2D, value: Double, string: String)(implicit valueFormatterLocale: ValueFormatterLocale): Unit = {
    val bottom = plot.screenDataBounds.bottom
    val x = axis.screenValue(value)
    majorTicksSettings.paintBottomAndTop(g, bottom, plot.screenDataBounds.top, x)

    scientificNotationRichString(string).drawAt(g, x, bottom, HorizontalAlignment.center, VerticalAlignment.top)
  }
}

trait AxisYPainter extends AxisPainter {
  protected def axis: Axis = plot.axisY

  protected def majorTicksSettings: TicksSettings = paintingProperties.majorTicksSettingsY

  protected def minorTicksSettings: TicksSettings = paintingProperties.minorTicksSettingsY

  protected def paintMinorTick(g: Graphics2D, value: Double): Unit = {
    minorTicksSettings.paintLeftAndRight(
      g,
      plot.screenDataBounds.left,
      plot.screenDataBounds.right,
      axis.screenValue(value)
    )
  }

  protected def paintMajorTick(g: Graphics2D, value: Double, string: String)(implicit valueFormatterLocale: ValueFormatterLocale): Unit = {
    val y = axis.screenValue(value)
    val left = plot.screenDataBounds.left
    majorTicksSettings.paintLeftAndRight(g, left, plot.screenDataBounds.right, y)
    scientificNotationRichString(string).drawAt(g, new ScreenPoint(left - 3, y), HorizontalAlignment.right, VerticalAlignment.center)
  }
}
