package info.td.scalaplot.axispainter

import java.awt.Graphics2D

import info.td.scalaplot.{Plot, ValueFormatterLocale}
import rx.lang.scala.Observable

trait NullAxisPainter extends NullMajorTicksAxisPainter with NullMinorTicksAxisPainter {
  protected def plot = new Plot(Observable.empty)
}

trait NullMajorTicksAxisPainter extends AxisPainter {
  protected def paintMajorTicks(g: Graphics2D)(implicit valueFormatterLocale: ValueFormatterLocale): Unit = {}
}

trait NullMinorTicksAxisPainter extends AxisPainter {
  protected def paintMinorTicks(g: Graphics2D): Unit = {}
}

object NullAxisXPainter extends NullAxisPainter with AxisXPainter

object NullAxisYPainter extends NullAxisPainter with AxisYPainter

