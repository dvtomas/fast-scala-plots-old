package info.td.scalaplot.axispainter

import java.awt.Graphics2D

import info.td.scalaplot.utils.RichString
import info.td.scalaplot.{ValueFormatterLocale, HorizontalAlignment, Plot, VerticalAlignment}

trait LimitsMajorTicksXPainter extends AxisPainter {
  protected def paintMajorTicks(g: Graphics2D)(implicit valueFormatterLocale: ValueFormatterLocale) = {
    val bottom = plot.screenDataBounds.bottom
    val top = plot.screenDataBounds.top

    val dataMinimumX = axis.dataRange.minimum
    val dataMaximumX = axis.dataRange.maximum

    val screenMinimumX = axis.screenValue(dataMinimumX)
    val screenMaximumX = axis.screenValue(dataMaximumX)

    majorTicksSettings.paintBottomAndTop(g, bottom, top, screenMinimumX)
    new RichString(paintingProperties.valueString(dataMinimumX, 2), paintingProperties.fontProperties)
      .drawAt(g, screenMinimumX, bottom, HorizontalAlignment.left, VerticalAlignment.top)

    majorTicksSettings.paintBottomAndTop(g, bottom, top, screenMaximumX)
    new RichString(paintingProperties.valueString(dataMaximumX, 2), paintingProperties.fontProperties)
      .drawAt(g, screenMaximumX, bottom, HorizontalAlignment.right, VerticalAlignment.top)

    if (dataMinimumX <= 0.0 && 0.0 <= dataMaximumX) {
      val screenZeroX = axis.screenValue(0.0)
      majorTicksSettings.paintBottomAndTop(g, bottom, top, screenZeroX)
      new RichString("0", paintingProperties.fontProperties)
        .drawAt(g, screenZeroX, bottom, HorizontalAlignment.center, VerticalAlignment.top)
    }
  }
}

trait LimitsMajorTicksYPainter extends AxisPainter {
  protected def paintMajorTicks(g: Graphics2D)(implicit valueFormatterLocale: ValueFormatterLocale) = {
    val left = plot.screenDataBounds.left
    val right = plot.screenDataBounds.right

    val dataMinimumY = axis.dataRange.minimum
    val dataMaximumY = axis.dataRange.maximum

    val screenMinimumY = axis.screenValue(dataMinimumY)
    val screenMaximumY = axis.screenValue(dataMaximumY)

    majorTicksSettings.paintLeftAndRight(g, left, right, screenMinimumY)
    new RichString(paintingProperties.valueString(dataMinimumY, 2), paintingProperties.fontProperties)
      .drawAt(g, left - 3, screenMinimumY, HorizontalAlignment.right, VerticalAlignment.bottom)

    majorTicksSettings.paintLeftAndRight(g, left, right, screenMaximumY)
    new RichString(paintingProperties.valueString(dataMaximumY, 2), paintingProperties.fontProperties)
      .drawAt(g, left - 3, screenMaximumY, HorizontalAlignment.right, VerticalAlignment.top)

    if (dataMinimumY <= 0.0 && 0.0 <= dataMaximumY) {
      axis.screenRange.expandToNearestTenth
      val screenZeroY = axis.screenValue(0.0)
      majorTicksSettings.paintLeftAndRight(g, left, right, screenZeroY)

      // Paint zero text only if it is not too close to the edges, where it would overlap with the edge text.
      if (axis.screenRange expandBy (-1.5 * paintingProperties.fontProperties.size) contains screenZeroY) {
        new RichString("0", paintingProperties.fontProperties)
          .drawAt(g, left - 3, screenZeroY, HorizontalAlignment.right, VerticalAlignment.center)
      }
    }
  }
}

class LimitsAxisXPainter(val plot: Plot) extends LinearAxisMinorTicksPainter with LimitsMajorTicksXPainter with AxisXPainter

class LimitsAxisYPainter(val plot: Plot) extends LinearAxisMinorTicksPainter with LimitsMajorTicksYPainter with AxisYPainter
