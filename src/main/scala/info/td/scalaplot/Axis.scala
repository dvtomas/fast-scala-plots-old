package info.td.scalaplot

import info.td.common.rx.NonFailingSubscribeErrorHandler
import info.td.scalaplot.utils.{DigitChange, DigitChangeComputer}
import rx.lang.scala.{Observable, Subject}

case class AxisChanged(axis: Axis)

class Axis(
  val screenBoundsSource: Observable[ScreenRectangle],
  orientation: AxisOrientation,
  var transformation: AxisTransformation
) {
  override def toString = s"$getClass($screenRange, $dataRange)"

  protected[this] var lastScreenRange: ScreenRange = ScreenRange(0, 1)

  def screenRange: ScreenRange = lastScreenRange

  private[this] val dataRangeSubject = Subject[DataRange]()

  def dataRangeSource: Observable[DataRange] = dataRangeSubject

  private[this] var lastDataRange = DataRange(0.0, 1.0)

  dataRangeSubject.subscribe(lastDataRange = _, NonFailingSubscribeErrorHandler)

  final def dataRange: DataRange = lastDataRange

  def subscribeToDataRangeSource(inputDataRangeSource: Observable[DataRange]): Unit = {
    inputDataRangeSource.subscribe(
      dataRangeSubject.onNext,
      dataRangeSubject.onError
      /*, dataRangeSubject.onCompleted Do not send onCompleted, in case we subscribe to e.g. Observable.just, we still want to be able to get latest value even though the original source has been completed. */
    )
  }

  def changedSource: Observable[AxisChanged] = dataRangeSubject.map(_ => AxisChanged(this))

  def dataValue(screenValue: Double): Double = transformation.dataValue(screenValue)

  def screenValue(dataValue: Double): Double = transformation.screenValue(dataValue)

  def digitChange(screenValue: Double): DigitChange = {
    new DigitChangeComputer().digitChange(dataValue(screenValue - 0.5), dataValue(screenValue + 0.5))
  }

  def zoomed(screenCenter: Double, zoom: Double): DataRange = {
    val newMinimum = screenCenter - (screenCenter - screenValue(dataRange.minimum)) * (1 + zoom)
    val newMaximum = screenCenter + ((screenValue(dataRange.maximum) - screenCenter) * (1 + zoom))
    DataRange(dataValue(newMinimum), dataValue(newMaximum))
  }

  def dragged(oldDataRange: DataRange, oldData: Double, newData: Double): DataRange = {
    val screenOffset = screenValue(newData) - screenValue(oldData)

    def translatedDataValue(dv: Double) = dataValue(screenValue(dv) - screenOffset)

    DataRange(translatedDataValue(oldDataRange.minimum), translatedDataValue(oldDataRange.maximum))
  }

  screenBoundsSource.subscribe(
    newBounds => {
      lastScreenRange = orientation.screenRange(newBounds)
      transformation = transformation.update(lastScreenRange, lastDataRange)
    },
    NonFailingSubscribeErrorHandler
  )

  dataRangeSubject.subscribe(
    _ => transformation = transformation.update(lastScreenRange, lastDataRange),
    NonFailingSubscribeErrorHandler
  )
}

abstract sealed class AxisOrientation {
  def screenRange(screenBounds: ScreenRectangle): ScreenRange
}

object AxisX extends AxisOrientation {
  def screenRange(screenBounds: ScreenRectangle): ScreenRange = {
    ScreenRange(screenBounds.left, screenBounds.right)
  }
}

object AxisY extends AxisOrientation {
  def screenRange(screenBounds: ScreenRectangle): ScreenRange = {
    ScreenRange(screenBounds.bottom, screenBounds.top)
  }
}

abstract class AxisTransformation {
  def update(screenRange: ScreenRange, dataRange: DataRange): AxisTransformation

  def dataValue(screenValue: Double): Double

  def screenValue(dataValue: Double): Double

  def mirrored: AxisTransformation
}

class LinearAxisTransformation(screenRange: ScreenRange, dataRange: DataRange) extends AxisTransformation {
  private[this] val (k, q) = if (screenRange.length == 0.0 || dataRange.length == 0.0) {
    (1.0, 0.0)
  } else {
    val k = dataRange.length / screenRange.length
    val q = k * screenRange.minimum - dataRange.minimum
    (k, q)
  }

  def update(screenRange: ScreenRange, dataRange: DataRange): AxisTransformation = {
    new LinearAxisTransformation(screenRange, dataRange)
  }

  def dataValue(screenValue: Double): Double = k * screenValue - q

  def mirrored: AxisTransformation = new MirroredLinearAxisTransformation(screenRange, dataRange)

  def screenValue(dataValue: Double): Double = (dataValue + q) / k
}

object LinearAxisTransformation {
  def apply(): LinearAxisTransformation = new LinearAxisTransformation(ScreenRange(0.0, 1.0), DataRange(0.0, 1.0))
}

class MirroredLinearAxisTransformation(screenRange: ScreenRange, dataRange: DataRange) extends AxisTransformation {
  private[this] val (k, q) = if (screenRange.length == 0.0 || dataRange.length == 0.0) {
    (1.0, 0.0)
  } else {
    val mirroredScreenRange = ScreenRange(screenRange.maximum, screenRange.minimum)
    val k = dataRange.length / mirroredScreenRange.length
    val q = k * mirroredScreenRange.maximum - dataRange.maximum
    (k, q)
  }

  def update(screenRange: ScreenRange, dataRange: DataRange): AxisTransformation = {
    new MirroredLinearAxisTransformation(screenRange, dataRange)
  }

  def mirrored: AxisTransformation = new LinearAxisTransformation(screenRange, dataRange)

  def dataValue(screenValue: Double): Double = k * screenValue - q

  def screenValue(dataValue: Double): Double = (dataValue + q) / k
}

object MirroredLinearAxisTransformation {
  def apply(): MirroredLinearAxisTransformation = new MirroredLinearAxisTransformation(ScreenRange(0.0, 1.0), DataRange(0.0, 1.0))
}

class LogAxisTransformation(screenRange: ScreenRange, dataRange: DataRange) extends AxisTransformation {
  private[this] val (k, q) = if (screenRange.length == 0.0 || dataRange.length == 0.0) {
    (1.0, 0.0)
  } else {
    val k = screenRange.length / (Math.log(dataRange.maximum) - Math.log(dataRange.minimum))
    val q = screenRange.minimum - k * Math.log(dataRange.minimum)
    (k, q)
  }

  def mirrored: AxisTransformation = new MirroredLogAxisTransformation(screenRange, dataRange)

  def update(screenRange: ScreenRange, dataRange: DataRange): LogAxisTransformation = {
    new LogAxisTransformation(screenRange, dataRange)
  }

  override def dataValue(screenValue: Double): Double = Math.exp((screenValue - q) / k)

  override def screenValue(dataValue: Double): Double = k * Math.log(dataValue) + q
}

object LogAxisTransformation {
  def apply(): LogAxisTransformation = new LogAxisTransformation(ScreenRange(0.0, 1.0), DataRange(0.0, 1.0))
}

class MirroredLogAxisTransformation(screenRange: ScreenRange, dataRange: DataRange) extends AxisTransformation {
  private[this] val (k, q) = if (screenRange.length == 0.0 || dataRange.length == 0.0) {
    (1.0, 0.0)
  } else {
    val mirroredScreenRange = DataRange(screenRange.maximum, screenRange.minimum)
    val k = mirroredScreenRange.length / (Math.log(dataRange.maximum) - Math.log(dataRange.minimum))
    val q = mirroredScreenRange.minimum - k * Math.log(dataRange.minimum)
    (k, q)
  }

  def mirrored: AxisTransformation = new LogAxisTransformation(screenRange, dataRange)

  def update(screenRange: ScreenRange, dataRange: DataRange): MirroredLogAxisTransformation = {
    new MirroredLogAxisTransformation(screenRange, dataRange)
  }

  override def dataValue(screenValue: Double): Double = Math.exp((screenValue - q) / k)

  override def screenValue(dataValue: Double): Double = k * Math.log(dataValue) + q
}

object MirroredLogAxisTransformation {
  def apply(): MirroredLogAxisTransformation = new MirroredLogAxisTransformation(ScreenRange(0.0, 1.0), DataRange(0.0, 1.0))
}
