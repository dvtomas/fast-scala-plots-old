package info.td.scalaplot

import java.awt.geom.Line2D
import java.awt.image.BufferedImage
import java.awt.{AlphaComposite, BasicStroke, Color, Graphics2D}

import info.td.scalaplot.axispainter.{AxisXPainter, AxisYPainter, LinearAxisXPainter, LinearAxisYPainter}
import info.td.scalaplot.plotparts.{PlotPartChanged, PlotPartTrait}
import info.td.scalaplot.utils.Graphics2DHelpers._
import info.td.scalaplot.utils.{Graphics2DUtils, RichString}
import rx.lang.scala.Observable
import rx.lang.scala.subjects.ReplaySubject

case class PlotChanged(plot: Plot)

trait DataToScreenTransformer {
  def screenPoint(dataPoint: DataPoint): ScreenPoint
}

class Plot(protected val screenPlotBoundsSource: Observable[ScreenRectangle]) extends DataToScreenTransformer {
  private[this] var legendEnabled: Boolean = true

  def setLegendEnabled(enabled: Boolean): Unit = {
    legendEnabled = enabled
  }

  def isLegendEnabled: Boolean = legendEnabled

  final def screenPlotBounds: ScreenRectangle = screenPlotBoundsSource.first.toBlocking.head

  def screenDataBoundsSource: Observable[ScreenRectangle] = screenPlotBoundsSource.map(_.expanded(-80, -20, -20, -50))

  final def screenDataBounds: ScreenRectangle = screenDataBoundsSource.first.toBlocking.head

  protected def title: RichString = new RichString("")

  protected def xLabel: RichString = new RichString("")

  protected def yLabel: RichString = new RichString("")

  def backgroundColor: Color = Color.WHITE

  protected def dataBorderColor: Color = Color.DARK_GRAY

  protected def plotBorderColor: Color = Color.BLACK

  protected def createAxisX(screenBoundsSource: Observable[ScreenRectangle]): Axis = {
    new Axis(screenBoundsSource, AxisX, LinearAxisTransformation())
  }

  lazy final val axisX: Axis = createAxisX(screenDataBoundsSource)

  protected def createAxisY(screenBoundsSource: Observable[ScreenRectangle]): Axis = {
    new Axis(screenBoundsSource, AxisY, LinearAxisTransformation())
  }

  lazy final val axisY: Axis = createAxisY(screenDataBoundsSource)

  def drawPlotBackground(g: Graphics2D): Unit = {
    g.setColor(backgroundColor)
    g.fill(screenPlotBounds.snappedForFillInside)
  }

  def screenPoint(dataPoint: DataPoint): ScreenPoint =
    new ScreenPoint(axisX.screenValue(dataPoint.x), axisY.screenValue(dataPoint.y))

  def dataPoint(screenPoint: ScreenPoint): DataPoint =
    DataPoint(axisX.dataValue(screenPoint.x), axisY.dataValue(screenPoint.y))

  /** The plot border is drawn so that the lines lie inside the plot bounds. We are not allowed to draw outside. */
  def drawPlotBorder(g: Graphics2D): Unit = {
    g.setColor(plotBorderColor)
    g.draw(screenPlotBounds.snappedForDrawInside)
  }

  /** The data border is drawn outside the data bounds. All the place inside is reserved for the plots. */
  def drawDataBorder(g: Graphics2D): Unit = {
    g.setColor(dataBorderColor)
    g.draw(screenDataBounds.snappedForDrawOutside)
  }

  protected def createAxisXPainter: AxisXPainter = new LinearAxisXPainter(this)

  protected def createAxisYPainter: AxisYPainter = new LinearAxisYPainter(this)

  protected val axisXPainter: AxisXPainter = createAxisXPainter

  protected val axisYPainter: AxisYPainter = createAxisYPainter

  def drawAxes(g: Graphics2D)(implicit valueFormatterLocale: ValueFormatterLocale): Unit = {
    axisXPainter.paint(g)
    axisYPainter.paint(g)
  }

  def drawZeroAxisX(g: Graphics2D): Unit = {
    val y = axisY.screenValue(0)
    if (y > screenDataBounds.top && y < screenDataBounds.bottom) {
      g.setColor(Color.gray)
      g.setStroke(new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0f, Array(3f, 2f), 0f))
      g.draw(new Line2D.Double(screenDataBounds.left, y.snappedToPixelCenter, screenDataBounds.right.toInt, y.snappedToPixelCenter))
    }
  }

  def drawLabels(g: Graphics2D): Unit = {
    title.drawAt(g, screenDataBounds.horizontalCenter, screenPlotBounds.top, HorizontalAlignment.center, VerticalAlignment.top)

    xLabel.drawAt(g, screenDataBounds.horizontalCenter, screenPlotBounds.bottom, HorizontalAlignment.center, VerticalAlignment.bottom)

    val oldTransform = g.getTransform
    g.rotate(-Math.PI / 2.0, screenPlotBounds.left, screenDataBounds.verticalCenter)
    yLabel.drawAt(g, screenPlotBounds.left, screenDataBounds.verticalCenter, HorizontalAlignment.center, VerticalAlignment.top)
    g.setTransform(oldTransform)
  }

  var cachedLegendImage: Option[BufferedImage] = None

  def getCachedLegendImage: Option[BufferedImage] = {
    if (parts.forall(_.plotTitle.string.isEmpty)) {
      None
    } else {
      val temporaryImage = Graphics2DUtils.createCompatibleImage(500, 2000)
      val temporaryG = temporaryImage.createGraphics()
      Graphics2DUtils.setAntiAliasedRendering(temporaryG, enabled = true) // legend image is cached, render it nicely

      temporaryG.setColor(new Color(240, 240, 240))
      temporaryG.setComposite(AlphaComposite.Src)
      temporaryG.fillRect(0, 0, temporaryImage.getWidth, temporaryImage.getHeight)

      val legendSize = parts.foldLeft[ScreenRectangle](
        ScreenRectangle(0, 0, 0, 0)
      )((legendSizeSoFar, plotPart) => {
        val referencePoint = new ScreenPoint(0, legendSizeSoFar.bottom + 2)
        val resultPoint = plotPart.plotLegendPart(temporaryG, referencePoint)
        ScreenRectangle(referencePoint, resultPoint) union legendSizeSoFar
      })

      val legendImage = Graphics2DUtils.createCompatibleImage(legendSize.width.toInt + 4, legendSize.height.toInt + 4)
      val legendBufferG = legendImage.createGraphics()
      legendBufferG.drawImage(temporaryImage, 1, 1, null)
      legendBufferG.setColor(Color.DARK_GRAY)
      legendBufferG.drawRect(0, 0, legendImage.getWidth - 1, legendImage.getHeight - 1)
      Some(legendImage)
    }
  }

  def drawLegend(g: Graphics2D): Unit = {
    if (!isLegendEnabled) {
      return
    }

    if (cachedLegendImage.isEmpty) {
      cachedLegendImage = getCachedLegendImage
    }
    for (legendImage <- cachedLegendImage) {
      import java.awt.AlphaComposite
      val alphaComposite = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.9f)
      g.setComposite(alphaComposite)
      g.drawImage(
        legendImage,
        screenDataBounds.left.toInt + 6,
        screenDataBounds.top.toInt + 6,
        null)
    }
    g.setComposite(AlphaComposite.Src)
  }

  def paintComponent(g: Graphics2D)(implicit valueFormatterLocale: ValueFormatterLocale): Unit = {
    // Just to be sure
    g.setComposite(AlphaComposite.Src)

    drawPlotBackground(g)
    g.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER))
    drawPlotBorder(g)
    drawDataBorder(g)
    drawAxes(g)
    drawZeroAxisX(g)
    drawLabels(g)
    parts foreach (_.paintComponent(g))
    drawLegend(g)
  }

  private val plotPartsSubject = ReplaySubject[PlotPartTrait]()

  def addPlotPart(plotPart: PlotPartTrait): Unit = {
    _parts :+= plotPart
    plotPartsSubject onNext plotPart
  }

  protected[this] def plotPartsChangedSource: Observable[PlotPartChanged] = plotPartsSubject.map(_.changedSource).flatten

  protected[this] def axisChangedSource: Observable[AxisChanged] = axisX.changedSource.onBackpressureDrop merge axisY.changedSource.onBackpressureDrop

  def changedSource: Observable[PlotChanged] = (plotPartsChangedSource merge axisChangedSource)
    .map(_ => PlotChanged(this))
    .share

  private def aggregatedDataRanges(dataRanges: Seq[DataRange]): Option[DataRange] = {
    val validRanges = dataRanges.filter(bounds => bounds.isValidDataRange)

    if (validRanges.nonEmpty) {
      Some(validRanges.reduceLeft(_ union _))
    } else {
      None
    }
  }

  def parts: List[PlotPartTrait] = _parts

  private var _parts: List[PlotPartTrait] = Nil

  def totalXDataRange: Option[DataRange] =
    aggregatedDataRanges(parts.map(_.dataProvider.dataBoundsX))

  def totalYDataRange: Option[DataRange] =
    aggregatedDataRanges(parts.map(_.dataProvider.dataBoundsY))

  def yDataRangeAt(xDataRange: DataRange): Option[DataRange] =
    aggregatedDataRanges(parts.map(_.dataProvider.dataBoundsYAt(xDataRange)))

  val totalXDataRangeSource: Observable[DataRange] =
    plotPartsSubject.map(_.changedSource).flatten
      .map(_ => totalXDataRange.getOrElse(DataRange(0, 1)))

  val totalYDataRangeSource: Observable[DataRange] =
    plotPartsSubject.map(_.changedSource).flatten
      .map(_ => totalYDataRange.getOrElse(DataRange(0, 1)))

}
