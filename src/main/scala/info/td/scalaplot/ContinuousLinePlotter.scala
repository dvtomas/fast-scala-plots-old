package info.td.scalaplot

import info.td.common.ui.swing.SwingHelpers

import java.awt.geom.{Ellipse2D, Line2D, Rectangle2D}
import java.awt.{BasicStroke, Color, Graphics2D}

abstract class LineStyle {
  def setupPaintAndStroke(g: Graphics2D): Unit

  def drawLine(g: Graphics2D, point1: ScreenPoint, point2: ScreenPoint): Unit

  def drawPoint(g: Graphics2D, point: ScreenPoint): Unit

  def colorForLightLaf: Color

  def color: Color = SwingHelpers.colorAdaptedToDarkLightLaf(colorForLightLaf)
}

object LineStyleNone extends LineStyle {
  def drawLine(g: Graphics2D, point1: ScreenPoint, point2: ScreenPoint): Unit = {}

  override def setupPaintAndStroke(g: Graphics2D): Unit = {}

  def drawPoint(g: Graphics2D, point: ScreenPoint): Unit = {}

  def colorForLightLaf: Color = Color.BLACK
}

case class LineStyleSimple(width: Float, colorForLightLaf: Color) extends LineStyle {
  val stroke = new BasicStroke(width, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER)

  override def setupPaintAndStroke(g: Graphics2D): Unit = {
    g.setColor(color)
    g.setStroke(stroke)
  }

  def drawLine(g: Graphics2D, point1: ScreenPoint, point2: ScreenPoint): Unit = {
    g.draw(new Line2D.Double(point1.toPoint2D, point2.toPoint2D))
  }

  def drawPoint(g: Graphics2D, point: ScreenPoint): Unit = {
    g.setColor(color)
    g.fill(new Rectangle2D.Double(point.x - (width / 2), point.y - (width / 2), width, width))
  }
}

case class LineStyleDashed(width: Float, colorForLightLaf: Color, dashPattern: Vector[Float], dashPhase: Float = 0.0f) extends LineStyle {
  val stroke = new BasicStroke(
    width,
    BasicStroke.CAP_BUTT,
    BasicStroke.JOIN_MITER,
    10.0f,
    dashPattern.toArray,
    dashPhase
  )

  override def setupPaintAndStroke(g: Graphics2D): Unit = {
    g.setColor(color)
    g.setStroke(stroke)
  }

  def drawLine(g: Graphics2D, point1: ScreenPoint, point2: ScreenPoint): Unit = {
    g.draw(new Line2D.Double(point1.toPoint2D, point2.toPoint2D))
  }

  def drawPoint(g: Graphics2D, point: ScreenPoint): Unit = {
    g.setColor(color)
    g.fill(new Rectangle2D.Double(point.x - (width / 2), point.y - (width / 2), width, width))
  }
}

abstract class PointStyle {
  def drawPoint(g: Graphics2D, point: ScreenPoint): Unit

  def setupPaintAndStroke(g: Graphics2D): Unit

  def colorForLightLaf: Color

  def color: Color = SwingHelpers.colorAdaptedToDarkLightLaf(colorForLightLaf)
}

object PointStyleNone extends PointStyle {
  override def setupPaintAndStroke(g: Graphics2D): Unit = {}

  override def colorForLightLaf: Color = Color.black

  def drawPoint(g: Graphics2D, point: ScreenPoint): Unit = {}
}

case class PointStyleRectangle(colorForLightLaf: Color) extends PointStyle {
  private val stroke = new BasicStroke(1)

  override def setupPaintAndStroke(g: Graphics2D): Unit = {
    g.setColor(color)
    g.setStroke(stroke)
  }

  def drawPoint(g: Graphics2D, point: ScreenPoint): Unit = {
    g.draw(new Rectangle2D.Double(point.x - 1.0, point.y - 1.0, 2.0, 2.0))
  }
}

case class PointStyleCross(colorForLightLaf: Color, r: Double = 1.0) extends PointStyle {
  private val stroke = new BasicStroke(1)

  override def setupPaintAndStroke(g: Graphics2D): Unit = {
    g.setColor(color)
    g.setStroke(stroke)
  }

  def drawPoint(g: Graphics2D, point: ScreenPoint): Unit = {
    g.draw(new Line2D.Double(point.x - r, point.y, point.x + r, point.y))
    g.draw(new Line2D.Double(point.x, point.y - r, point.x, point.y + r))
  }
}


case class PointStyleDiamond(colorForLightLaf: Color, strokeWidth: Int = 1, size: Int = 2) extends PointStyle {
  private val stroke = new BasicStroke(strokeWidth.toFloat)

  override def setupPaintAndStroke(g: Graphics2D): Unit = {
    g.setColor(color)
    g.setStroke(stroke)
  }

  private val xShape = Array[Int](0, -size, 0, size, 0)
  private val yShape = Array[Int](-size, 0, size, 0, -size)

  def drawPoint(g: Graphics2D, point: ScreenPoint): Unit = {
    def xOffset = point.x

    def yOffset = point.y

    for (xy: Array[(Double, Double)] <- (xShape.map(_ + xOffset) zip yShape.map(_ + yOffset)).sliding(2)) {
      g.draw(new Line2D.Double(xy(0)._1, xy(0)._2, xy(1)._1, xy(1)._2))
    }
    //    Old way: g.drawPolyline(xShape.map(_ + xOffset.toInt), yShape.map(_ + yOffset.toInt), xShape.size)
  }
}

case class PointStyleCircle(colorForLightLaf: Color, r: Double, strokeWidth: Double = 1.0) extends PointStyle {
  private val stroke = new BasicStroke(strokeWidth.toFloat)

  def drawPoint(g: Graphics2D, point: ScreenPoint): Unit = {
    g.draw(new Ellipse2D.Double(point.x - r, point.y - r, r * 2.0, r * 2.0))
  }

  override def setupPaintAndStroke(g: Graphics2D): Unit = {
    g.setColor(color)
    g.setStroke(stroke)
  }
}


case class LinePlottingProperties(
  lineStyle: LineStyle = LineStyleSimple(1, Color.GRAY),
  pointStyle: PointStyle = PointStyleRectangle(Color.BLACK),
  overrideAntiAliased: Option[Boolean] = None,
)

class ContinuousLinePlotter(
  g: Graphics2D,
  coordinatesTransformer: DataToScreenTransformer,
  linePlottingProperties: LinePlottingProperties,
  maybeClippingRectangle: Option[ScreenRectangle] // This really helps for very long lines that are either anti-aliased, or dashed.
) {
  private var maybeLastPoint: Option[ScreenPoint] = None
  private var wasPointBeforeLastPoint: Boolean = false

  def startNewLine(): Unit = {
    maybeLastPoint = None
  }

  def addDataLine(point: DataPoint): Unit = {
    val newWasPointBeforeLastPoint = maybeLastPoint.nonEmpty
    if (point.hasNaN) {
      if (!wasPointBeforeLastPoint) {
        // last point was in the middle between two non-points. We need to draw at least a pixel at its position.
        maybeLastPoint.foreach(lastPoint => linePlottingProperties.lineStyle.drawPoint(g, lastPoint))
      }
      maybeLastPoint = None
    } else {
      val screenPoint = coordinatesTransformer.screenPoint(point)
      addScreenLine(screenPoint)
    }
    wasPointBeforeLastPoint = newWasPointBeforeLastPoint
  }

  def addScreenLine(point: ScreenPoint): Unit = {
    maybeLastPoint.foreach { lastPoint =>
      maybeClippingRectangle match {
        case Some(clippingRectangle) => clippingRectangle.clippedLine(lastPoint, point) match {
          case Some((point1, point2)) => linePlottingProperties.lineStyle.drawLine(g, point1, point2)
          case None =>
        }
        case None => linePlottingProperties.lineStyle.drawLine(g, lastPoint, point)
      }
    }
    maybeLastPoint = Some(point)
  }

  def drawDataPoint(point: DataPoint): Unit = {
    linePlottingProperties.pointStyle.drawPoint(g, coordinatesTransformer.screenPoint(point))
  }
}

