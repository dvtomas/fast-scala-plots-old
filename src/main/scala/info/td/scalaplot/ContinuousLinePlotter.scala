package info.td.scalaplot

import java.awt.geom.{Line2D, Rectangle2D}
import java.awt.{BasicStroke, Color, Graphics2D}

abstract class LineStyle {
  def drawLine(g: Graphics2D, point1: ScreenPoint, point2: ScreenPoint): Unit

  def drawPoint(g: Graphics2D, point: ScreenPoint): Unit

  def color: Color
}

object LineStyleNone extends LineStyle {
  def drawLine(g: Graphics2D, point1: ScreenPoint, point2: ScreenPoint): Unit = {}

  def drawPoint(g: Graphics2D, point: ScreenPoint): Unit = {}

  def color: Color = Color.BLACK
}

case class LineStyleSimple(width: Float, color: Color) extends LineStyle {
  val stroke = new BasicStroke(width, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER)

  def drawLine(g: Graphics2D, point1: ScreenPoint, point2: ScreenPoint): Unit = {
    g.setColor(color)
    g.setStroke(stroke)
    g.draw(new Line2D.Double(point1.toPoint2D, point2.toPoint2D))
  }

  def drawPoint(g: Graphics2D, point: ScreenPoint): Unit = {
    g.setColor(color)
    g.fill(new Rectangle2D.Double(point.x - (width / 2), point.y - (width / 2), width, width))
  }
}

case class LineStyleDashed(width: Float, color: Color, dashPattern: Vector[Float], dashPhase: Float = 0.0f) extends LineStyle {
  val stroke = new BasicStroke(
    width,
    BasicStroke.CAP_BUTT,
    BasicStroke.JOIN_MITER,
    10.0f,
    dashPattern.toArray,
    dashPhase
  )

  def drawLine(g: Graphics2D, point1: ScreenPoint, point2: ScreenPoint): Unit = {
    g.setColor(color)
    g.setStroke(stroke)
    g.draw(new Line2D.Double(point1.toPoint2D, point2.toPoint2D))
  }

  def drawPoint(g: Graphics2D, point: ScreenPoint): Unit = {
    g.setColor(color)
    g.fill(new Rectangle2D.Double(point.x - (width / 2), point.y - (width / 2), width, width))
  }
}

abstract class PointStyle {
  def drawPoint(g: Graphics2D, point: ScreenPoint): Unit
}

object PointStyleNone extends PointStyle {
  def drawPoint(g: Graphics2D, point: ScreenPoint): Unit = {}
}

case class PointStyleRectangle(color: Color) extends PointStyle {
  private val stroke = new BasicStroke(1)

  def drawPoint(g: Graphics2D, point: ScreenPoint): Unit = {
    g.setColor(color)
    g.setStroke(stroke)
    g.draw(new Rectangle2D.Double(point.x - 1.0, point.y - 1.0, 2.0, 2.0))
  }
}

case class PointStyleCross(color: Color) extends PointStyle {
  private val stroke = new BasicStroke(1)

  def drawPoint(g: Graphics2D, point: ScreenPoint): Unit = {
    g.setColor(color)
    g.setStroke(stroke)
    g.draw(new Line2D.Double(point.x - 1.0, point.y, point.x + 1.0, point.y))
    g.draw(new Line2D.Double(point.x, point.y - 1.0, point.x, point.y + 1.0))
  }
}

case class PointStyleDiamond(color: Color, width: Int = 1) extends PointStyle {
  private val stroke = new BasicStroke(width)
  private val xShape = Array[Int](0, -2, 0, 2, 0)
  private val yShape = Array[Int](-2, 0, 2, 0, -2)

  def drawPoint(g: Graphics2D, point: ScreenPoint): Unit = {
    g.setColor(color)
    g.setStroke(stroke)

    def xOffset = point.x

    def yOffset = point.y

    for (xy: Array[(Double, Double)] <- (xShape.map(_ + xOffset) zip yShape.map(_ + yOffset)).sliding(2)) {
      g.draw(new Line2D.Double(xy(0)._1, xy(0)._2, xy(1)._1, xy(1)._2))
    }
    //    Old way: g.drawPolyline(xShape.map(_ + xOffset.toInt), yShape.map(_ + yOffset.toInt), xShape.size)
  }
}

case class LinePlottingProperties(lineStyle: LineStyle = LineStyleSimple(1, Color.GRAY), pointStyle: PointStyle = PointStyleRectangle(Color.BLACK))

class ContinuousLinePlotter(
  g: Graphics2D,
  coordinatesTransformer: DataToScreenTransformer,
  linePlottingProperties: LinePlottingProperties,
  maybeClippingRectangle: Option[ScreenRectangle] // This really helps for very long lines that are either antialiased, or dashed.
) {
  private var maybeLastPoint: Option[ScreenPoint] = None
  private var wasPointBeforeLastPoint: Boolean = false

  def startNewLine(): Unit = {
    maybeLastPoint = None
  }

  def addPoint(point: DataPoint): Unit = {
    val newWasPointBeforeLastPoint = maybeLastPoint.nonEmpty
    if (point.hasNaN) {
      if (!wasPointBeforeLastPoint) {
        // last point was in the middle between two non-points. We need to draw at least a pixel at its position.
        maybeLastPoint.foreach(lastPoint => linePlottingProperties.lineStyle.drawPoint(g, lastPoint))
      }
      maybeLastPoint = None
    } else {
      val screenPoint = coordinatesTransformer.screenPoint(point)
      addScreenPoint(screenPoint)
    }
    wasPointBeforeLastPoint = newWasPointBeforeLastPoint
  }

  def addScreenPoint(point: ScreenPoint): Unit = {
    maybeLastPoint.foreach { lastPoint =>
      maybeClippingRectangle match {
        case Some(clippingRectangle) => clippingRectangle.clippedLine(lastPoint, point) match {
          case Some((point1, point2)) => linePlottingProperties.lineStyle.drawLine(g, point1, point2)
          case None =>
        }
        case None => linePlottingProperties.lineStyle.drawLine(g, lastPoint, point)
      }
    }
    linePlottingProperties.pointStyle.drawPoint(g, point)
    maybeLastPoint = Some(point)
  }
}

