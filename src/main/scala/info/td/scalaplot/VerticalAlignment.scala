package info.td.scalaplot

abstract sealed class VerticalAlignment

object VerticalAlignment {

  case object top extends VerticalAlignment

  case object center extends VerticalAlignment

  case object bottom extends VerticalAlignment

}

abstract sealed class HorizontalAlignment

object HorizontalAlignment {

  case object left extends HorizontalAlignment

  case object center extends HorizontalAlignment

  case object right extends HorizontalAlignment

}

