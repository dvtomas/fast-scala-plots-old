package info.td.scalaplot

import info.td.scalaplot.utils.MathUtils

abstract class AbstractRange() {
  type ThisType <: AbstractRange

  def minimum: Double

  def maximum: Double

  def newInstance(minimum: Double, maximum: Double): ThisType

  def length: Double = maximum - minimum

  def isAscending: Boolean = minimum <= maximum

  def -(offset: Double): ThisType = newInstance(minimum - offset, maximum - offset)

  def isValidDataRange: Boolean = !minimum.isNaN && !minimum.isInfinity && !maximum.isNaN && !maximum.isInfinity

  def union(other: ThisType): ThisType =
    if (isAscending) {
      newInstance(
        MathUtils.minimumWithNaNs(minimum, other.minimum),
        MathUtils.maximumWithNaNs(maximum, other.maximum)
      )
    } else {
      unionDescending(other)
    }

  def union(point: Double): ThisType = if (isAscending)
    newInstance(MathUtils.minimumWithNaNs(minimum, point), MathUtils.maximumWithNaNs(maximum, point))
  else
    newInstance(MathUtils.maximumWithNaNs(minimum, point), MathUtils.minimumWithNaNs(maximum, point))

  def unionDescending(other: ThisType): ThisType = newInstance(
    MathUtils.maximumWithNaNs(minimum, other.minimum),
    MathUtils.minimumWithNaNs(maximum, other.maximum)
  )

  def intersectionAscending(other: ThisType): ThisType = {
    // this logic also handles NaN's, change with extreme care!
    if (!(minimum <= other.maximum && other.minimum <= maximum)) {
      newInstance(Double.NaN, Double.NaN)
    } else {
      newInstance(minimum max other.minimum, maximum min other.maximum)
    }
  }

  def contains(value: Double): Boolean = if (isAscending)
    value >= minimum && value <= maximum
  else
    value >= maximum && value <= minimum

  def intersectsWith(that: ThisType): Boolean = {
    val min1 = this.minimum min this.maximum
    val max1 = this.minimum max this.maximum
    val min2 = that.minimum min that.maximum
    val max2 = that.minimum max that.maximum
    val outside = (min1 >= max2) || (max1 < min2)
    !outside
  }

  def translatePositionToPositionOn(value: Double, anotherRange: ThisType): Double = {
    val ratio = (value - minimum) / (maximum - minimum)
    ratio * (anotherRange.maximum - anotherRange.minimum) + anotherRange.minimum
  }

  def center: Double = (maximum + minimum) / 2

  def symmetricValue(value: Double): Double = (2 * center) - value

  def ascending: ThisType = if (minimum <= maximum) {
    newInstance(minimum, maximum)
  } else {
    newInstance(maximum, minimum)
  }

  def map(f: Double => Double): ThisType = newInstance(f(minimum), f(maximum))

  def expandToNearestTenth: ThisType = {
    val exponent = MathUtils.decadicExponent(length.abs)
    val multiplier = Math.pow(10.0, exponent - 1)
    val result = if (isAscending)
      newInstance(Math.floor(minimum / multiplier) * multiplier, Math.ceil(maximum / multiplier) * multiplier)
    else
      newInstance(Math.ceil(minimum / multiplier) * multiplier, Math.floor(maximum / multiplier) * multiplier)
    result
  }

  def expandBy(amount: Double): ThisType = {
    if (isAscending) {
      newInstance(minimum - amount, maximum + amount)
    } else {
      newInstance(minimum + amount, maximum - amount)
    }
  }
}

case class DataRange(minimum: Double, maximum: Double) extends AbstractRange {
  type ThisType = DataRange

  def newInstance(minimum: Double, maximum: Double): DataRange = DataRange(minimum, maximum)
}

object DataRange {
  val invalid: DataRange = DataRange(Double.NaN, Double.NaN)
}

case class ScreenRange(minimum: Double, maximum: Double) extends AbstractRange {
  type ThisType = ScreenRange

  def newInstance(minimum: Double, maximum: Double): ScreenRange = ScreenRange(minimum, maximum)
}

object ScreenRange {
  val invalid: ScreenRange = ScreenRange(Double.NaN, Double.NaN)
}