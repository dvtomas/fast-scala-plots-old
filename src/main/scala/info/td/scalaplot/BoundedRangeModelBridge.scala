package info.td.scalaplot

import info.td.common.logging.TraceHelpers.{DumpFunction, noOpDumpFunction}
import info.td.common.rx.NonFailingSubscribeErrorHandler
import info.td.common.rx.RxHelpers._
import javax.swing.event.ChangeEvent
import javax.swing.{BoundedRangeModel, DefaultBoundedRangeModel}
import rx.lang.scala.{Observable, Subject}

/** This class provides a bridge between my representation of a visible range inside an enclosing range (a pair of data range value holders, one for visible range, one for the enclosing), and a swing-compatible representation - a BoundedRangeModel, that is usable with scroll bars. The bridge is two way, and is lazily initialized.
  * Unless you explicitly ask for a boundedRange, it is not initialized and no computations occur.
  *
  * @param inputVisibleRangeSource visible range source
  * @param totalRangeSource        total enclosing range source
  */

abstract class BoundedRangeModelBridge(
  inputVisibleRangeSource: Observable[DataRange],
  totalRangeSource: Observable[DataRange],
  reverseDirection: Boolean
) {
  /* This is very difficult to get right. We must
   - break the possibly infinite cycle occurring when inputVisibleRange and outputVisible range are the same (emitting the value in output means new input value incoming, recomputing the bounds again and again emit new output value
   - Handle correctly that the visible range may be outside the total range - we must use the union of the two ranges, but how should the scroll bar behave, when we have the visible range outside the total range, and slowly return back? It's handle should grow as more and more of the visible range is inside the total range, but to do that, we would require recomputing the scroll bar values - receiving new input visible ranges, thus introducing the cycle again and breaking previous assumption

    The specs are not complete - they do not test above scenarios. It would be great to enhance them, but that is a future work. For now, if you make ANY experiments with this code, please try the result with Changing plots - try going outside the visible range, returning back, check if the aspect ratio of the circle hasn't changed!, watch the behavior of scroll bars when interleaving dragging and scroll bar handle, or even block increments - there are many subtle problems that can occur. I'm even leaving the dump messages here, just uncomment the following implicit to have them again.
   */
  implicit val dontDumpDebug: DumpFunction = noOpDumpFunction

  protected val scrollBarResolution: DataRange = DataRange(0, Int.MaxValue - 1)

  private[this] case class PositionAndExtent(position: Int, extent: Int)

  /*
    When the user moves the scroll bar, we need to recompute output visible range. However, if output visible range and input visible range are connected, this would in turn generate new input for the scroll bar re-computation. To avoid this, we remember the last output value, and in the scroll bar re-computation we let in only non-equal values.

    I don't use this to break the cycle now, I just use lastPositionAndExtent, but I'm leaving it here for future experiments.
  */
  private var lastOutputVisibleRange: DataRange = null

  // When the user drags the plot thus triggering change in input visible range, we need to re-compute the scroll bar bounds. However, if output visible range and input visible range are connected, re-computed scroll bar position would in turn generate new output visible range, thus triggering a change in input visible range again. To avoid this, we remember the computed scroll bar bounds, and in the input visible range processing let only non-equal values in.
  private var lastPositionAndExtent: PositionAndExtent = null


  lazy val boundedRangeModel: DefaultBoundedRangeModel = {
    val model = new DefaultBoundedRangeModel()
    model.setRangeProperties( // Init with reasonable defaults
      scrollBarResolution.minimum.toInt,
      scrollBarResolution.minimum.toInt + 1,
      scrollBarResolution.minimum.toInt,
      scrollBarResolution.maximum.toInt, false
    )

    inputVisibleRangeSource.filter(_ != lastOutputVisibleRange).dumpOnEach("boundedRange: Input visible")
      .combineLatest(totalRangeSource.dumpOnEach("boundedRange: total"))
      .map({ case (inputVisibleRange, totalRange) => recomputePositionAndExtent(inputVisibleRange, totalRange, model) })
      .dumpOnEach("boundedRange: newPositionAndExtent")
      .observeOn(productionSchedulerProvider.swing) // Don't remove. model.setRangeProperties needs to be performed on the EDT, and invokeLater breaks behavior (compare Demo/changing data...)
      .onBackpressureDrop
      .subscribe(newPositionAndExtent => {
        lastPositionAndExtent = newPositionAndExtent
        model.setRangeProperties(newPositionAndExtent.position, newPositionAndExtent.extent, scrollBarResolution.minimum.toInt, scrollBarResolution.maximum.toInt, false)
      }, NonFailingSubscribeErrorHandler)
    model
  }

  lazy val outputVisibleRangeSource: Observable[DataRange] = {
    val positionAndExtentSubject = Subject[PositionAndExtent]()
    boundedRangeModel.addChangeListener((_: ChangeEvent) => {
      positionAndExtentSubject onNext PositionAndExtent(boundedRangeModel.getValue, boundedRangeModel.getExtent)
    })

    // distinctUntilChanged is necessary, because stateChanged in ChangeListener gets called many (for me 5) times after e.g. click on the scroll bar, probably a feature of Swing I didn't want to dig into.
    val boundedRangeValuesSource = positionAndExtentSubject.serialize.onBackpressureDrop.distinctUntilChanged

    // The union of the data ranges is here to properly support situations, where the visible range is (at least partially) outside the total range.
    val unionTotalRangeSource = totalRangeSource.dumpOnEach("output visible: Total")
      .combineLatest(inputVisibleRangeSource.filter(_ != lastOutputVisibleRange).dumpOnEach("output visible: Input Visible"))
      .map(x => x._1 union x._2)
      .dumpOnEach("output visible: new union total")

    unionTotalRangeSource
      .combinedWithLatestFrom(boundedRangeValuesSource.filter(_ != lastPositionAndExtent).dumpOnEach("output visible: boundedRangeValues"))
      .map({ case (totalRange, boundedRangeValues) => recomputeVisibleRange(boundedRangeValues, totalRange) })
      .dumpOnEach("output visible: new range")
      .onBackpressureDrop
      .share // This is crucial in case multiple plots want to share the same scroll-bar, which can happen quite easily
    // Not breaking the cycle this way now, but leaving here for future experiments.
    //  .doOnNext(range => lastOutputVisibleRange = range )
  }

  private[this] def recomputePositionAndExtent(visibleRange: DataRange, totalRange: DataRange, boundedRangeModel: BoundedRangeModel): PositionAndExtent = {
    // The union of the data ranges is here to properly support situations, where the visible range is (at least partially) outside the total range.
    def value(x: Double): Double = (totalRange union visibleRange).translatePositionToPositionOn(x, scrollBarResolution)

    val _resultMin = value(visibleRange.minimum) max scrollBarResolution.minimum
    val _resultMax = (value(visibleRange.maximum) max (_resultMin + 1)) min scrollBarResolution.maximum
    val (resultMin, resultMax) = reverseDirection match {
      case false => (_resultMin, _resultMax)
      case true => (scrollBarResolution.symmetricValue(_resultMax), scrollBarResolution.symmetricValue(_resultMin))
    }
    PositionAndExtent(resultMin.toInt, (resultMax - resultMin).toInt)
  }

  private[this] def recomputeVisibleRange(rangeValues: PositionAndExtent, totalRange: DataRange): DataRange = {
    def value(x: Double) = scrollBarResolution.translatePositionToPositionOn(x, totalRange)

    val _resultMin = value(rangeValues.position)
    val _resultMax = value(rangeValues.position + rangeValues.extent)
    val (resultMin, resultMax) = reverseDirection match {
      case false => (_resultMin, _resultMax)
      case true => (totalRange.symmetricValue(_resultMax), totalRange.symmetricValue(_resultMin))
    }
    DataRange(resultMin, resultMax)
  }
}

class HorizontalBoundedRangeModelBridge(
  inputVisibleRangeSource: Observable[DataRange],
  totalRangeSource: Observable[DataRange]
) extends BoundedRangeModelBridge(inputVisibleRangeSource, totalRangeSource, false)

class VerticalBoundedRangeModelBridge(
  inputVisibleRangeSource: Observable[DataRange],
  totalRangeSource: Observable[DataRange]
) extends BoundedRangeModelBridge(inputVisibleRangeSource, totalRangeSource, true)
