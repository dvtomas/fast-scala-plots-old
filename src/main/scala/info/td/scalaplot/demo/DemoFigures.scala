package info.td.scalaplot.demo

import java.awt.{Color, Font, Graphics}
import java.util.{Date, Locale}

import info.td.scalaplot._
import info.td.scalaplot.axispainter._
import info.td.scalaplot.figure.{Figure, FigureControllerTouchDevice, TouchActions}
import info.td.scalaplot.plotcontroller.{AutoCADPlotController, CrossHairPickerPainter, VerticalPickerPainter}
import info.td.scalaplot.plotparts._
import info.td.scalaplot.utils.{FontProperties, RichString, RichStringWithBackground, SolidRectangleBackground}
import javax.swing._
import rx.lang.scala.{Observable, Subject}

private object Helpers {
  def setScrollBarModel(scrollBar: JScrollBar, model: DefaultBoundedRangeModel): Unit = {
    scrollBar.setModel(model)
    val range = model.getMaximum - model.getMinimum
    scrollBar.setBlockIncrement(range / 100)
    scrollBar.setUnitIncrement(range / 1000)
  }

  val valueFormatterLocale: ValueFormatterLocale = ValueFormatterLocale(Locale.getDefault)
}

private abstract class DemoSpiralDataProvider extends XYPlotPartDataProvider {
  var step = 0

  def numberOfPoints = 10000

  def radius: Double = 20 + (200 * math.sin(step / 10.0))

  def xValue(index: Int): Double = 100 + (radius + index / 5.0) * math.sin(index / 20.0 + step / 10.0)

  def yValue(index: Int): Double = if (0 == (index % 10)) Double.NaN else 100 + (radius + index / 5.0) * math.cos(index / 20.0 + step / 10.0)

  /* This is not completely accurate, but it is good enough.
  Spiral plot also serves as a benchmark, and we don't want to influence our drawing FPS with computation of bounds, especially when it involves a complicated computation for each point. The bounds are computed each time the plot is changed so that the scrollBars have correct information about the scrolling range.
   In the future we might want to be clever and compute the accurate bounds from the outermost part of the spiral only.
   */
  override def dataBoundsX: DataRange = DataRange(100 - (radius + numberOfPoints / 5.0), 100 + (radius + numberOfPoints / 5.0))

  override def dataBoundsY: DataRange = DataRange(100 - (radius + numberOfPoints / 5.0), 100 + (radius + numberOfPoints / 5.0))
}

private class DemoXYPlot(
  screenPlotBoundsSource: Observable[ScreenRectangle],
  provider: XYPlotPartDataProvider
) extends Plot(screenPlotBoundsSource) {
  private val sampleXYPlotPart: XYPlotPart = new XYPlotPart(provider, this) {
    override val linePlottingProperties: LinePlottingProperties = LinePlottingProperties(LineStyleSimple(1, Color.ORANGE), PointStyleRectangle(Color.BLUE))
  }
  addPlotPart(sampleXYPlotPart)
}

private class DemoThreePlotParts(screenPlotBoundsSource: Observable[ScreenRectangle], partsToAdd: Iterable[Int]) extends Plot(screenPlotBoundsSource) {
  lazy val plotPart1: XYPlotPart = new XYPlotPart(new XYPlotPartDataProvider {
    def numberOfPoints = 1000

    def xValue(index: Int): Double = index + 20

    def yValue(index: Int): Double = 1000 * Math.sin(index.toDouble / 10)

    def changedSource: Observable[Unit] = Observable.just(())
  }, this) {
    override val linePlottingProperties: LinePlottingProperties = LinePlottingProperties(LineStyleSimple(1, Color.GRAY), PointStyleRectangle(Color.BLACK))

    override val plotTitle = new RichString("simple sinus line")

    override def pick(where: ScreenPoint)(implicit valueFormatterLocale: ValueFormatterLocale): Option[PickInfo] = pickVertical(where)
  }

  lazy val plotPart2: XYPlotPart = new XYPlotPart(new XYPlotPartDataProvider {
    def numberOfPoints = 1000

    def xValue(index: Int): Double = index / 2.0 + 50

    def yValue(index: Int): Double = 200 * Math.cos(index.toDouble / 15) + 100 * Math.sin(index.toDouble / 10)

    def changedSource: Observable[Unit] = Observable.just(())
  }, this) {
    override val linePlottingProperties: LinePlottingProperties = LinePlottingProperties(LineStyleSimple(2, Color.BLUE), PointStyleNone)

    override val plotTitle = new RichString("combined sinus", FontProperties(size = 14, style = Font.BOLD))

    override def pick(where: ScreenPoint)(implicit valueFormatterLocale: ValueFormatterLocale): Option[PickInfo] = pickVertical(where)
  }

  lazy val plotPart3: XYPlotPart = new XYPlotPart(new XYPlotPartDataProvider {
    def numberOfPoints = 1000

    def xValue(index: Int): Double = index + 20

    def yValue(index: Int): Double = 1000 * Math.cos(index.toDouble / 20) + index

    def changedSource: Observable[Unit] = Observable.just(())
  }, this) {
    override val linePlottingProperties: LinePlottingProperties = LinePlottingProperties(LineStyleSimple(1, Color.RED), PointStyleRectangle(Color.MAGENTA))

    override val plotTitle = new RichString("sinus + line", FontProperties(style = Font.ITALIC))

    override def pick(where: ScreenPoint)(implicit valueFormatterLocale: ValueFormatterLocale): Option[PickInfo] = pickVertical(where)
  }

  partsToAdd foreach (index => addPlotPart(Seq(plotPart1, plotPart2, plotPart3)(index)))

  private val sampleTextBoxesPartDataProvider: HorizontalTextBoxPlotPartDataProvider = new HorizontalTextBoxPlotPartDataProvider {
    def textBoxes = List(
      HorizontalTextBox(new RichStringWithBackground("Some note in the plot", SolidRectangleBackground()), 1),
      HorizontalTextBox(new RichStringWithBackground("Another note", SolidRectangleBackground(Color.GREEN)), 50),
      HorizontalTextBox(new RichStringWithBackground("A big note", SolidRectangleBackground(), FontProperties(24)), 800)
    )

    def changedSource: Observable[Unit] = Observable.just(())
  }
  private val sampleTextBoxesPart = new HorizontalTextBoxPlotPart(
    sampleTextBoxesPartDataProvider,
    this,
    LineStyleSimple(1.0f, Color.BLACK)
  )

  addPlotPart(sampleTextBoxesPart)
}

class DemoPlotWithLabelsAndLegend extends Figure()(Helpers.valueFormatterLocale) {
  private val plot = new DemoThreePlotParts(screenBoundsSource, Seq(0, 1, 2)) {
    override protected def title = new RichString("Title of the plot", FontProperties(size = 16, style = Font.BOLD, color = Color.BLUE))

    override protected def xLabel = new RichString("X value", FontProperties(style = Font.BOLD))

    override protected def yLabel = new RichString("Y value", FontProperties(style = Font.BOLD))

    override protected def createAxisX(screenBoundsSource: Observable[ScreenRectangle]): Axis = {
      new Axis(screenBoundsSource, AxisX, LogAxisTransformation())
    }

    override protected def createAxisXPainter: AxisXPainter = new DigitChangeAxisXPainter(this)
  }
  val controller: AutoCADPlotController = addPlotCreateAutoCADControllerAndSubscribeAxes(plot, VerticalPickerPainter)
  controller.fitDataToScreen()
  controller.deleteZoomHistory()
}

class HeightMapDemo extends Figure()(Helpers.valueFormatterLocale) {

  val palette: LinearPalette = LinearPalette(
    Seq((-1.0, Color.CYAN), (0.0, Color.GREEN), (1.0, Color.ORANGE)),
    Color.BLUE, Color.RED, Color.BLACK
  )

  private class DemoHeightMapDataProvider extends HeightMapDataProvider[Int] {
    private[this] val numberOfRows = 1000
    private[this] val numberOfColumns = 1500

    val xRange = new EquidistantRange(0.0, numberOfColumns, 1.0)

    val yRange = new EquidistantRange(0.0, numberOfRows, 1.0)

    def yValue(xIndex: Int): HeightMapColumn[Int] = HeightMapColumn[Int](
      xIndex,
      (yIndex: Int) => {
        if (xIndex < 0)
          throw new ArrayIndexOutOfBoundsException(s"x < 0: $xIndex")
        if (xIndex >= numberOfColumns)
          throw new ArrayIndexOutOfBoundsException(s"x >= $numberOfColumns: $xIndex")
        if (yIndex < 0)
          throw new ArrayIndexOutOfBoundsException(s"y < 0: $yIndex")
        if (yIndex >= numberOfRows)
          throw new ArrayIndexOutOfBoundsException(s"y >= $numberOfRows: $yIndex")

        val value = Math.sin(yIndex.toDouble / 120) + Math.cos(xIndex.toDouble / 80) + 0.2 * Math.sin(yIndex.toDouble / 20) * Math.sin(xIndex.toDouble / 20)
        if (value > 0.7 && value < 0.85)
          Double.NaN
        else
          value
      }
    )

    def changedSource: Observable[Unit] = Observable.just(())
  }

  private class DemoHeightMap(screenPlotBoundsSource: Observable[ScreenRectangle]) extends Plot(screenPlotBoundsSource) {
    val plotPart = new HeightMapPlotPart(
      new DemoHeightMapDataProvider,
      this,
      palette,
      CommonAggregateFunctions.takeFirst
    )
    addPlotPart(plotPart)
  }

  private val plot = new DemoHeightMap(screenBoundsSource) {
    override protected def title = new RichString("Height map plot", FontProperties(size = 16, style = Font.BOLD, color = Color.BLUE))

    override protected def xLabel = new RichString("X value", FontProperties(style = Font.BOLD))

    override protected def yLabel = new RichString("Y value", FontProperties(style = Font.BOLD))
  }

  private[this] val controller = new AutoCADPlotController(plot, CrossHairPickerPainter)
  addPlot(plot, controller)
  plot.axisX.subscribeToDataRangeSource(controller.axisXDataRangeSource)
  plot.axisY.subscribeToDataRangeSource(controller.axisYDataRangeSource)

  controller.fitDataToScreen()
  controller.deleteZoomHistory()
}


class DemoPlotForTouchScreen extends DemoPlotWithLabelsAndLegend {
  lazy val touchActions: TouchActions = new TouchActions(TouchActions.defaultActions)

  override protected def createController() = new FigureControllerTouchDevice(this, touchActions)
}

class DemoChangingDataFigure extends Figure()(Helpers.valueFormatterLocale) {

  val fpsSubject: Subject[String] = Subject[String]()
  val numberOfPointsModel: DefaultBoundedRangeModel = new DefaultBoundedRangeModel(10000, 100, 10, 100000)

  override protected def paintComponent(g: Graphics): Unit = {
    super.paintComponent(g)
    requestRepaintAndUpdateFPS()
  }

  var lastUpdate: Long = new Date().getTime
  var framesSinceLastUpdate = 0

  private def requestRepaintAndUpdateFPS(): Unit = {
    framesSinceLastUpdate += 1
    val now = new Date().getTime
    val timeDifference = (now - lastUpdate) / 1000.0
    if (timeDifference > 1.0) {
      fpsSubject onNext "%.1f".format(framesSinceLastUpdate / timeDifference)
      lastUpdate = now
      framesSinceLastUpdate = 0
    }
    dataProvider.changed()
  }

  private object dataProvider extends DemoSpiralDataProvider {
    override def numberOfPoints: Int = numberOfPointsModel.getValue

    def changed(): Unit = {
      step += 1
      changedSubject.onNext(())
    }

    private val changedSubject = Subject[Unit]()

    def changedSource: Observable[Unit] = changedSubject
  }

  private val plot = new DemoXYPlot(screenBoundsSource, dataProvider) {
    override protected def createAxisXPainter: AxisXPainter = new LimitsAxisXPainter(this)

    override protected def createAxisYPainter: AxisYPainter = new LimitsAxisYPainter(this)
  }

  private[this] val controller = new AutoCADPlotController(plot, CrossHairPickerPainter)
  controller.setKeepSquareAspectRatio(true)

  addPlot(plot, controller)

  def setScrollBars(horizontalScrollBar: JScrollBar, verticalScrollBar: JScrollBar): Unit = {
    Helpers.setScrollBarModel(horizontalScrollBar, controller.subscribeAxisXToAndGetBoundedRangeModel)
    Helpers.setScrollBarModel(verticalScrollBar, controller.subscribeAxisYToAndGetBoundedRangeModel)
    controller.fitDataToScreen()
    controller.deleteZoomHistory()
  }
}

class DemoConnectedPlots extends Figure()(Helpers.valueFormatterLocale) {
  def create(): Unit = {
    val numberOfPlots = 3

    val plots = (0 until numberOfPlots) map (index =>
      new DemoThreePlotParts(
        screenBoundsSource.map(_.verticalSplit(index, numberOfPlots)),
        Set(0, 1, 2) - index
      ))
    val controllers = plots map (plot => new AutoCADPlotController(plot, VerticalPickerPainter))
    plots zip controllers foreach { case (plot, controller) => addPlot(plot, controller) }

    // Axis X is common for all plots
    val commonAxisXDataRangeSource = Observable.from(controllers.map(_.axisXDataRangeSource)).flatten
    plots.foreach(_.axisX.subscribeToDataRangeSource(commonAxisXDataRangeSource))

    // Axis Y is common for second and third plot, first plot has independent axis Y.
    plots.head.axisY.subscribeToDataRangeSource(controllers.head.axisYDataRangeSource)
    val commonAxisYDataRangeSource = Observable.from(controllers.drop(1).map(_.axisYDataRangeSource)).flatten
    plots.drop(1).foreach(_.axisY.subscribeToDataRangeSource(commonAxisYDataRangeSource))

    controllers foreach { controller =>
      controller.fitDataToScreen()
      controller.deleteZoomHistory()
    }

    controllers.head setLinkedControllers Seq(controllers(2))
    controllers(2) setLinkedControllers Seq(controllers.head)
  }

  create()
}

class TrampolineFigure extends Figure()(Helpers.valueFormatterLocale) {
  private[this] val plot: Plot = new Plot(screenBoundsSource) {
    val trampolinePartLimits: VerticalAreaSnakePlotPart = new VerticalAreaSnakePlotPart(new SnakePlotPartDataProvider {
      val xRange = new EquidistantRange(9999999.0, 10000000, -1.0)

      def yValue(index: Int): Double = {
        val y = 200 * Math.cos(index.toDouble / 3000) + 100 * Math.sin(index.toDouble / 1300)
        if (Math.abs(y) > 150) y else Double.NaN
      }

      def changedSource: Observable[Unit] = Observable.just(())

    }, this) {
      override val linePlottingProperties: LinePlottingProperties = LinePlottingProperties(LineStyleSimple(1, Color.ORANGE), PointStyleNone)

      override val plotTitle = new RichString(s"Limits val > 150, ${dataProvider.xRange.numberOfPoints.toString} points")
    }
    addPlotPart(trampolinePartLimits)

    val trampolinePart1: SnakePlotPart = new SnakePlotPart(new SnakePlotPartDataProvider {
      val xRange = new EquidistantRange(9999999.0, 10000000, -1.0)

      def yValue(index: Int): Double = {
        //        if ((index % 500 > 400) || (index % 200 > 195)) {
        if (Math.abs(Math.sin(index)) > 0.2) {
          Double.NaN
        } else {
          500 * Math.sin(index.toDouble / 43) - 200 * Math.cos(index.toDouble / 50)
        }
      }

      def changedSource: Observable[Unit] = Observable.just(())
    }, this) {
      override val linePlottingProperties: LinePlottingProperties = LinePlottingProperties(LineStyleSimple(1, Color.GRAY), PointStyleNone /*PointStyleRectangle(Color.BLACK)*/)

      override val plotTitle = new RichString(s"simple sinus line, ${dataProvider.xRange.numberOfPoints.toString} points")
    }
    addPlotPart(trampolinePart1)

    val trampolinePart2: SnakePlotPart = new SnakePlotPart(new SnakePlotPartDataProvider {
      val xRange = new EquidistantRange(9999999.0, 10000000, -1.0)

      def yValue(index: Int): Double = 200 * Math.cos(index.toDouble / 500) + 100 * Math.sin(index.toDouble / 320)

      def changedSource: Observable[Unit] = Observable.just(())

    }, this) {
      override val linePlottingProperties: LinePlottingProperties = LinePlottingProperties(LineStyleSimple(2, Color.BLUE), PointStyleNone)

      override val plotTitle = new RichString(s"combined sinus, ${dataProvider.xRange.numberOfPoints.toString} points")
    }
    addPlotPart(trampolinePart2)
  }

  private[this] val controller = new AutoCADPlotController(plot, VerticalPickerPainter)
  addPlot(plot, controller)
  plot.axisY.subscribeToDataRangeSource(controller.axisYDataRangeSource)

  def setScrollBar(horizontalScrollBar: JScrollBar): DefaultBoundedRangeModel = {
    val boundedRangeModel = controller.subscribeAxisXToAndGetBoundedRangeModel
    Helpers.setScrollBarModel(horizontalScrollBar, boundedRangeModel)
    controller setXDataRange DataRange(10000000.0, 10000000.0 - 20000.0)
    controller.fitYToScreen()
    controller.deleteZoomHistory()
    boundedRangeModel
  }
}