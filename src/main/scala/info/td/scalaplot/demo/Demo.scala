package info.td.scalaplot.demo

import java.awt.{Dimension, Font}

import info.td.common.rx.NonFailingSubscribeErrorHandler
import info.td.scalaplot.plotparts.HeightMapPalette
import info.td.scalaplot.trampoline.TrampolineWithTapButtons
import javax.swing._
import javax.swing.border.EtchedBorder
import javax.swing.event.ChangeEvent
import net.miginfocom.swing.MigLayout
import rx.lang.scala.Observable

object Demo {
  def create: JPanel = {
    def descriptionText(description: String) = {
      val scrollPane = new JScrollPane()
      val textPane = new JTextPane
      textPane.setContentType("text/html")
      textPane.setEditable(false)
      textPane.setAlignmentX(0.5f)
      textPane.setAlignmentY(0.5f)
      textPane.setAutoscrolls(true)
      textPane.setText(description)
      scrollPane.setViewportView(textPane)
      scrollPane
    }

    def welcomeTab: JPanel = {
      val welcomeTab = new JPanel()
      welcomeTab.setLayout(new MigLayout("fillX,wrap", "[fill]", "[]"))
      welcomeTab.add(descriptionText("<h1>Welcome to Fast scala plots</h1> Click individual tabs to see a demo of what the library has to offer."))
      welcomeTab
    }

    def basicPlotTab: JPanel = {
      val basicPlotTab = new JPanel()
      basicPlotTab.setLayout(new MigLayout("fill,wrap", "[fill]", "[][fill,grow]"))
      basicPlotTab.add(descriptionText("This is a basic plot demonstration. This plot includes a title, labels for X and Y axes, and a legend."))
      basicPlotTab.add(new DemoPlotWithLabelsAndLegend())
      basicPlotTab
    }

    def changingDataTab: JPanel = {
      def benchmarkPanel(fpsSource: Observable[String], numberOfPointsModel: DefaultBoundedRangeModel): JPanel = {
        val panel = new JPanel()
        panel.setLayout(new MigLayout("wrap", "[][][]", "[][]"))
        panel.setBorder(BorderFactory.createTitledBorder("Live data benchmark"))
        val numberOfPointsSlider = new JSlider()
        panel.add(numberOfPointsSlider)
        val numberOfPointsSpinner = new JSpinner()
        val numberOfPointsSpinnerModel = new SpinnerNumberModel(10000, 10, 100000, 100)
        numberOfPointsSpinnerModel.addChangeListener((e: ChangeEvent) => {
          val m: SpinnerNumberModel = e.getSource.asInstanceOf[SpinnerNumberModel]
          numberOfPointsModel.setValue(m.getValue.asInstanceOf[Integer])
        })
        numberOfPointsModel.addChangeListener((e: ChangeEvent) => {
          val m: DefaultBoundedRangeModel = e.getSource.asInstanceOf[DefaultBoundedRangeModel]
          numberOfPointsSpinnerModel.setValue(m.getValue)
        })
        numberOfPointsSpinner.setModel(numberOfPointsSpinnerModel)
        numberOfPointsSlider.setModel(numberOfPointsModel)
        panel.add(numberOfPointsSpinner)
        panel.add(new JLabel("Points"))
        panel.add(new JLabel("FPS"))

        val fpsLabel = new JLabel("0")
        panel.add(fpsLabel)
        fpsSource.subscribe(fpsString => fpsLabel.setText(fpsString), NonFailingSubscribeErrorHandler)
        panel
      }

      def figurePanel(figure: DemoChangingDataFigure) = {
        val panel = new JPanel()
        panel.setLayout(new MigLayout("fill,wrap", "[][fill,grow]", "[fill,grow][]"))
        val verticalScrollBar = new JScrollBar()
        panel.add(verticalScrollBar)
        panel.add(figure)
        val horizontalScrollBar = new JScrollBar(0)
        panel.add(horizontalScrollBar, "skip 1,growX")
        figure.setScrollBars(horizontalScrollBar, verticalScrollBar)
        panel
      }

      val changingDataTab = new JPanel()
      changingDataTab.setLayout(new MigLayout("fill,wrap", "[fill]", "[][][fill,grow]"))
      changingDataTab.add(descriptionText("The data provider periodically publishes an event informing that the data has changed. At each iteration, the spiral starts with a different offset.\nAlso note, that the scrollbars can be used to control the position of the plot and also get dynamically adjusted to it's changing dimensions.\nThis plot also serves as a crude benchmark, just change the number of points to display below and see how it affects FPS.\nFinal interesting thing is that this plot was configured to keep a square aspect ratio."))
      val changingDataFigure = new DemoChangingDataFigure
      changingDataTab.add(benchmarkPanel(changingDataFigure.fpsSubject, changingDataFigure.numberOfPointsModel))
      changingDataTab.add(figurePanel(changingDataFigure))
      changingDataTab
    }

    def connectedPlotsTab = {
      val panel = new JPanel()
      panel.setLayout(new MigLayout("fill,wrap", "[fill]", "[][fill,grow]"))
      panel.add(descriptionText("All the graphs share the same axis X data range. Second and third graph also share the same axis Y data range. Please also note, that the crosshair of the first and the third graph are linked together."))
      val demoConnectedPlots = new DemoConnectedPlots
      panel.add(demoConnectedPlots)
      panel
    }

    def plotWithToolbarTab = {
      val panel = new JPanel()
      panel.setLayout(new MigLayout("fill,wrap", "[fill]", "[][][fill,grow]"))
      val figure = new DemoPlotForTouchScreen
      val toolbar = {
        val toolBar: JToolBar = new JToolBar()
        toolBar.setFloatable(false)
        figure.touchActions.figureControlButtons foreach toolBar.add
        toolBar
      }
      panel.add(descriptionText("The plot can be easily controlled via the three mouse buttons and a wheel. However, on touch devices, the control is much more limited. In such case it is desirable to allow functions like panning, zooming, data picking and dragging to be performed in a modal way via the toolbar actions. The user first selects an action to perform (dragging, panning, ...) from a toolbar, then performs the selected action with left mouse button. For user convenience, the rest of the controls (buttons, wheel) still work the usual way."))
      panel.add(toolbar)
      panel.add(figure)
      panel
    }

    def plotWithTrampolineTab = {
      val panel = new JPanel()
      panel.setLayout(new MigLayout("fill,wrap", "[fill]", "[][fill,grow][][]"))
      val trampolineHorizontalScrollBar = new JScrollBar(0)
      val trampolineFigure = new TrampolineFigure
      val boundedRangeModel = trampolineFigure.setScrollBar(trampolineHorizontalScrollBar)
      val trampoline = new TrampolineWithTapButtons(
        boundedRangeModel,
        parameters => {
          parameters.direction match {
            case TrampolineWithTapButtons.Left => parameters.valueWithSafelyAddedOffset(-100000)
            case TrampolineWithTapButtons.Right => parameters.valueWithSafelyAddedOffset(100000)
          }
        }
      )
      panel.add(descriptionText("Snakes are plots with equal distances between X values. This allows a lot of optimizations, and we can display even very large data quickly. In the sample plot are purposely sections (smaller and bigger) of Not a Number values, so that we can check that these optimizations don't interfere with correct rendering of these holes.\nTrampoline is a control designed especially to aid scrolling in very long plots. The further away the user clicks or drags from the center, the faster the plots scrolls. This control is also practical with touch devices."))
      panel.add(trampolineFigure)
      panel.add(trampoline)
      panel.add(trampolineHorizontalScrollBar)
      panel
    }

    def heightFieldTab: JPanel = {
      val panel = new JPanel()
      panel.setLayout(new MigLayout("fill", "[fill, grow]0[fill, 81:81:81]", "[][fill,grow]"))
      panel.add(descriptionText("Height map is a common plot. A function of two variables (x, y) is rendered as a rectangle area, with each pixel drawn by a color representing its value. Fast scala plots support only height fields with equidistant X and Y step, but that should suffice for most usage scenarios."), "span, wrap")
      val heightMapDemo = new HeightMapDemo()
      panel.add(heightMapDemo)
      panel.add(HeightMapPalette.verticalLegendPanelFor(heightMapDemo.palette)(Helpers.valueFormatterLocale))
      panel
    }

    val topPanel = new JPanel()
    topPanel.setLayout(new MigLayout("fill,wrap", "[fill]", "[][fill,grow]"))
    val headingLabel = new JLabel("Demo")
    headingLabel.setBorder(new EtchedBorder(1))
    headingLabel.setFont(new Font(headingLabel.getFont.getName, Font.PLAIN, 20))
    headingLabel.setHorizontalAlignment(SwingConstants.CENTER)
    topPanel.add(headingLabel)

    val mainTabPane = new JTabbedPane
    mainTabPane.setTabLayoutPolicy(0)
    mainTabPane.setTabPlacement(2)
    topPanel.add(mainTabPane)

    mainTabPane.addTab("Welcome", welcomeTab)
    mainTabPane.addTab("Basic plot", basicPlotTab)
    mainTabPane.addTab("Changing data", changingDataTab)
    mainTabPane.addTab("Connected plots", connectedPlotsTab)
    mainTabPane.addTab("Plot toolbar", plotWithToolbarTab)
    mainTabPane.addTab("Trampoline", plotWithTrampolineTab)
    mainTabPane.addTab("Height map", heightFieldTab)

    mainTabPane.setSelectedIndex(0)
    topPanel
  }

  def main(args: Array[String]): Unit = {
    System.setProperty("awt.useSystemAAFontSettings", "on")
    System.setProperty("swing.aatext", "true")

    val frame: JFrame = new JFrame("Demo")
    frame.setPreferredSize(new Dimension(800, 600))
    frame.setContentPane(create)
    frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE)
    frame.pack()
    frame.setVisible(true)
  }
}