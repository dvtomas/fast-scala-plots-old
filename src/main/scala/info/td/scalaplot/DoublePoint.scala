package info.td.scalaplot

import java.awt.Point
import java.awt.geom.Point2D

trait DoublePoint {
  def x: Double

  def y: Double

  def hasNaN = x.isNaN || y.isNaN
}

case class DataPoint(x: Double, y: Double) extends DoublePoint {
}

case class ScreenPoint(x: Double, y: Double) extends DoublePoint {
  def toPoint2D = new Point2D.Double(x, y)
}

object ScreenPoint {
  def apply(point: Point) = new ScreenPoint(point.x.toDouble, point.y.toDouble)
}