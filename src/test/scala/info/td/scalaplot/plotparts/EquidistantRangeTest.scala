package info.td.scalaplot.plotparts

import info.td.scalaplot._
import info.td.scalaplot.plotparts.EquidistantRange.DataIndicesAtPixel
import rx.lang.scala.Observable
import utest._

object EquidistantRangeTest extends TestSuite {
  private[this] def testAscendingAndDescending(
    orientation: AxisOrientation,
    screenBounds: ScreenRectangle,
    dataRange: DataRange,
    equidistantRange: EquidistantRange,
    expectedResult: Seq[DataIndicesAtPixel]
  ): Unit = {
    {
      val axis = new Axis(Observable.just(screenBounds), orientation, LinearAxisTransformation())
      axis.subscribeToDataRangeSource(Observable.just(dataRange))
      val actualResult = equidistantRange.findDataIndicesPackedByPixels(axis).toList
      assert(actualResult == expectedResult)
    }

    {
      val axis = new Axis(Observable.just(screenBounds), orientation, LinearAxisTransformation())
      axis.subscribeToDataRangeSource(Observable.just(DataRange(dataRange.maximum, dataRange.minimum)))
      val descendingExpectedResult = expectedResult zip expectedResult.reverse map {
        case (ascending, descending) => DataIndicesAtPixel(descending.pixel, ascending.dataIndices)
      }
      val expectedDescendingResult = equidistantRange.findDataIndicesPackedByPixels(axis).toList
      assert(expectedDescendingResult == descendingExpectedResult)
    }
  }

  val tests: Tests = Tests {
    "basic Axis X" - {
      testAscendingAndDescending(
        AxisX,
        ScreenRectangle(0.0, 0.0, 2.0, 1.0),
        DataRange(0.0, 2.0),
        EquidistantRange(0.0, 5, 0.45),
        List(DataIndicesAtPixel(0, 0 to 2), DataIndicesAtPixel(1, 3 to 4))
      )
    }

    "Axis X, non-zero screen range offset" - {
      testAscendingAndDescending(
        AxisX,
        ScreenRectangle(5.001, 0.0, 7.0, 1.0),
        DataRange(0.0, 2.0),
        EquidistantRange(0.0, 5, 0.45),
        List(DataIndicesAtPixel(5, 0 to 2), DataIndicesAtPixel(6, 3 to 4))
      )
    }

    "Axis X, non-zero screen range offset, some data not visible before the visible screen range" - {
      testAscendingAndDescending(
        AxisX,
        ScreenRectangle(5.001, 0.0, 7.0, 1.0),
        DataRange(0.0, 2.0),
        EquidistantRange(-0.9, 7, 0.45),
        List(DataIndicesAtPixel(5, 2 to 4), DataIndicesAtPixel(6, 5 to 6))
      )
    }

    "Axis X, non-zero screen range offset, some data not visible before and after the visible screen range" - {
      testAscendingAndDescending(
        AxisX,
        ScreenRectangle(5.001, 0.0, 7.0, 1.0),
        DataRange(0.0, 2.0),
        EquidistantRange(-0.9, 100, 0.45),
        List(DataIndicesAtPixel(5, 2 to 4), DataIndicesAtPixel(6, 5 to 6))
      )
    }

    "Axis X, non-zero screen range offset, some data missing on both sides" - {
      testAscendingAndDescending(
        AxisX,
        ScreenRectangle(5.001, 0.0, 7.0, 1.0),
        DataRange(5.0, 7.0),
        EquidistantRange(5.5, 3, 0.45),
        List(DataIndicesAtPixel(5, 0 to 1), DataIndicesAtPixel(6, 2 to 2))
      )
    }

    "basic Axis Y" - {
      testAscendingAndDescending(
        AxisY,
        ScreenRectangle(0.0, 0.0, 1.0, 2.0),
        DataRange(0.0, 2.0),
        EquidistantRange(0.0, 5, 0.45),
        List(DataIndicesAtPixel(1, 0 to 2), DataIndicesAtPixel(0, 3 to 4))
      )
    }
  }
}
