package info.td.scalaplot

import java.awt.image.BufferedImage
import java.awt.{Color, Dimension, FlowLayout, Graphics2D}
import java.util.Locale

import info.td.scalaplot.axispainter.{AxisXPainter, AxisYPainter, NullAxisXPainter, NullAxisYPainter}
import info.td.scalaplot.figure.Figure
import info.td.scalaplot.plotcontroller.{AutoCADPlotController, NullPickerPainter}
import info.td.scalaplot.plotparts.{XYPlotPart, XYPlotPartDataProvider}
import info.td.scalaplot.utils.Graphics2DUtils
import javax.swing.JFrame
import org.specs2._
import org.specs2.execute.ResultLike
import org.specs2.matcher.MatchResult
import org.specs2.specification.SpecificationFeatures
import org.specs2.specification.core.SpecStructure
import rx.lang.scala.Observable

abstract class AbstractFigureRenderingTest(useAntiAlias: Boolean) extends SpecificationFeatures with SpecificationLike {

  protected trait TestableFigure {
    this: Figure =>
    setSize(getPreferredSize)

    override def backgroundColor: Color = Color.PINK

    def display(wait: Int = 0): Unit = {
      val frame = new JFrame()
      frame.setBackground(Color.RED)
      frame.getContentPane.setLayout(new FlowLayout())
      frame.getContentPane.add(this)
      frame.pack()
      frame.setVisible(true)
      Thread.sleep(wait)
    }

    def renderedImage: BufferedImage = {
      val image = Graphics2DUtils.createCompatibleImage(getWidth, getHeight)
      paintComponent(image.createGraphics(), forceRepaint = true)
      image
    }

    override protected def antiAliasedRendering: Boolean = useAntiAlias
  }

  protected trait TestablePlot {
    self: Plot =>

    override def backgroundColor: Color = Color.GREEN

    override def dataBorderColor: Color = Color.DARK_GRAY

    override def plotBorderColor: Color = Color.BLACK
  }

  protected def color(image: BufferedImage, x: Int, y: Int) = new Color(image.getRGB(x, y))

  protected def colorMustBe(x: Int, y: Int, expectedColor: Color)(implicit image: BufferedImage): MatchResult[Color] = color(image, x, y) must beEqualTo(expectedColor)
}

abstract class FigureRenderingTest(useAntiAlias: Boolean) extends AbstractFigureRenderingTest(useAntiAlias) {
  implicit val valueFormatterLocale: ValueFormatterLocale = ValueFormatterLocale(Locale.US)

  def is: SpecStructure = "Figure must" ^
    "have initial size equal to preferredSize" ! {
      val figure = new Figure with TestableFigure
      val image = figure.renderedImage
      image.getWidth must beEqualTo(figure.getPreferredSize.getWidth) and (image.getHeight must beEqualTo(figure.getPreferredSize.getHeight))
    } ^ "have background color everywhere when no plots are defined" ! {
    val figure: Figure with TestableFigure = new Figure with TestableFigure
    implicit val image: BufferedImage = figure.renderedImage
    colorMustBe(0, 0, figure.backgroundColor) and
      colorMustBe(10, 10, figure.backgroundColor) and
      colorMustBe(figure.getWidth - 1, figure.getHeight - 1, figure.backgroundColor)
  }
}

abstract class PlotRenderingTest(useAntiAlias: Boolean) extends AbstractFigureRenderingTest(useAntiAlias) {
  implicit val valueFormatterLocale: ValueFormatterLocale = ValueFormatterLocale(Locale.US)

  def is: SpecStructure = "Plot inside a figure must" ^ "" +
    "have plot screen bounds outlined by a border, but the whole border must be inside the reserved area and be just one pixel wide" ! {
    throw new RuntimeException("This test hangs on my computer (JDK 8)!")

    val figure: Figure with TestableFigure = new Figure with TestableFigure
    val plot = new Plot(figure.screenBoundsSource.map(_.expanded(-1, -1))) with TestablePlot
    figure.addPlotCreateAutoCADControllerAndSubscribeAxes(plot, NullPickerPainter)
    implicit val image: BufferedImage = figure.renderedImage
    colorMustBe(0, 0, figure.backgroundColor) and
      colorMustBe(1, 1, Color.BLACK) and
      colorMustBe(2, 2, plot.backgroundColor) and
      colorMustBe(figure.getWidth - 1, figure.getHeight - 1, figure.backgroundColor) and
      colorMustBe(figure.getWidth - 2, figure.getHeight - 2, plot.plotBorderColor) and
      colorMustBe(figure.getWidth - 3, figure.getHeight - 3, plot.backgroundColor)
  } ^ "have plot data bounds outlined by a border, the outline is outside the actual plot data area" ! {
    val figure: Figure with TestableFigure = new Figure with TestableFigure
    val plot: Plot with TestablePlot = new Plot(figure.screenBoundsSource) with TestablePlot {
      override def screenDataBoundsSource: Observable[ScreenRectangle] = screenPlotBoundsSource.map(_.expanded(-3, -3))

      override protected def createAxisXPainter: AxisXPainter = NullAxisXPainter

      override protected def createAxisYPainter: AxisYPainter = NullAxisYPainter
    }
    figure.addPlotCreateAutoCADControllerAndSubscribeAxes(plot, NullPickerPainter)
    implicit val image: BufferedImage = figure.renderedImage
    colorMustBe(0, 0, plot.plotBorderColor) and
      colorMustBe(1, 1, plot.backgroundColor) and
      colorMustBe(2, 2, plot.dataBorderColor) and
      colorMustBe(3, 3, plot.backgroundColor) and
      colorMustBe(figure.getWidth - 1, figure.getHeight - 1, plot.plotBorderColor) and
      colorMustBe(figure.getWidth - 2, figure.getHeight - 2, plot.backgroundColor) and
      colorMustBe(figure.getWidth - 3, figure.getHeight - 3, plot.dataBorderColor) and
      colorMustBe(figure.getWidth - 4, figure.getHeight - 4, plot.backgroundColor)
  }
}

abstract class PlotPartRenderingTest(useAntiAlias: Boolean) extends AbstractFigureRenderingTest(useAntiAlias) {
  implicit val valueFormatterLocale: ValueFormatterLocale = ValueFormatterLocale(Locale.US)

  private abstract class TestingFigureContents {
    val figure: Figure with TestableFigure = new Figure with TestableFigure {
      override def preferredSize = new Dimension(4, 4) // leave a pixel on each side for the plot border and 2x2 pixels inside for the plot data
      setPreferredSize(getPreferredSize)
    }
    val plot: Plot with TestablePlot = new Plot(figure.screenBoundsSource) with TestablePlot {
      override def screenDataBoundsSource: Observable[ScreenRectangle] = screenPlotBoundsSource.map(_.expanded(-1, -1)) // The data bounds are one pixel inside the plot area to compensate the border

      override protected def createAxisXPainter: AxisXPainter = NullAxisXPainter

      override protected def createAxisYPainter: AxisYPainter = NullAxisYPainter

      override def drawZeroAxisX(g: Graphics2D): Unit = {} // We don't want the zero axis to mess with our test
    }
    val provider: XYPlotPartDataProvider = new XYPlotPartDataProvider {
      def xValue(index: Int): Double = (index.toDouble - 0.5) * 1000.0

      def numberOfPoints = 2

      def yValue(index: Int) = 0.5

      def changedSource: Observable[Unit] = Observable.just(())
    }
    val plotLineColor: Color = Color.RED
    val plotPart: XYPlotPart = new XYPlotPart(provider, plot) {
      override def linePlottingProperties: LinePlottingProperties = {
        LinePlottingProperties(LineStyleSimple(1, plotLineColor), PointStyleNone)
      }
    }
    plot.addPlotPart(plotPart)
    val controller: AutoCADPlotController = figure.addPlotCreateAutoCADControllerAndSubscribeAxes(plot, NullPickerPainter)
    controller.setXDataRange(DataRange(-1.0, 1.0))
    controller.setYDataRange(DataRange(-1.0, 1.0))

    implicit val image: BufferedImage = figure.renderedImage

    val tests: ResultLike
  }

  def is = "Plot part must" ^
    """Sample horizontal line of a XY plot is drawn correctly. On a computer screen, there is an area in which the data are drawn. This area is called the Plot Data Bounds. If the vertical data bounds are set e.g. to -1 .. +1 (it works equivalently for horizontal data bounds), it means that the data point +1 falls just exactly between the uppermost visible pixel of the plot data bounds area, and the invisible pixel above it (probably overdrawn by a border). In the same way, the point -1 falls just under the bottommost pixel of the plot data bounds. If we consider the case, where the visible area is just 2 pixels high, a horizontal line drawn of width one at vertical data position 0 would fall just between the top and the bottom pixel. When antialias is off, it would be probably snapped to the bottom pixel. With antialias on, it would draw a blurred line with half of the intensity on the top pixels, and half on the bottom pixels. A horizontal line of width one drawn at vertical data position +0.5 would exactly hit the center of the top pixel, and thus draw a perfectly crisp line with AA turned either on or off.
    """ ! {
      val testingFigureContents = new TestingFigureContents {
        val tests = {
          colorMustBe(1, 1, plotLineColor) and
            colorMustBe(1, 2, plot.backgroundColor)
        }
      }
      testingFigureContents.tests
    } ^
    """The data area must be correctly clipped by the boundaries of the Plot Data Bounds on both sides""" ! {
      val testingFigureContents = new TestingFigureContents {
        val tests = {
          colorMustBe(0, 1, plot.dataBorderColor) and
            colorMustBe(1, 1, plotLineColor) and
            colorMustBe(2, 1, plotLineColor) and
            colorMustBe(3, 1, plot.dataBorderColor)
        }
      }
      testingFigureContents.tests
    }
}

class FigureRenderingTestWithAntiAliasOn extends FigureRenderingTest(true) with SpecificationLike

class FigureRenderingTestWithAntiAliasOff extends FigureRenderingTest(false) with SpecificationLike

class PlotRenderingTestWithAntiAliasOn extends PlotRenderingTest(true) with SpecificationLike

class PlotRenderingTestWithAntiAliasOff extends PlotRenderingTest(false) with SpecificationLike

class PlotPartRenderingTestWithAntiAliasOn extends PlotPartRenderingTest(true) with SpecificationLike

class PlotPartRenderingTestWithAntiAliasOff extends PlotPartRenderingTest(false) with SpecificationLike
