package info.td.scalaplot

import utest._

object ScreenRectangleTest extends utest.TestSuite {
  val tests: Tests = Tests {
    test("clippedLine") {
      val clippingRectangle = ScreenRectangle(x1 = 4, y1 = 2, x2 = 20, y2 = 10)

      def testInside(p1: ScreenPoint, p2: ScreenPoint, expectedP1: ScreenPoint, expectedP2: ScreenPoint): Unit = {
        val (resultP1, resultP2) = clippingRectangle.clippedLine(p1, p2).get
        val ε = 0.001
        assert(
          (resultP1.x - expectedP1.x).abs < ε,
          (resultP1.y - expectedP1.y).abs < ε,
          (resultP2.x - expectedP2.x).abs < ε,
          (resultP2.y - expectedP2.y).abs < ε
        )
      }

      def testOutside(p1: ScreenPoint, p2: ScreenPoint): Unit = {
        assert(clippingRectangle.clippedLine(p1, p2).isEmpty)
      }

      testInside(ScreenPoint(0, 10), ScreenPoint(24, 4), ScreenPoint(4, 9), ScreenPoint(20, 5))
      testInside(ScreenPoint(24, 4), ScreenPoint(0, 10), ScreenPoint(20, 5), ScreenPoint(4, 9))
      testInside(ScreenPoint(2, 16), ScreenPoint(10, 0), ScreenPoint(5, 10), ScreenPoint(9, 2))
      testInside(ScreenPoint(5, 12), ScreenPoint(11, 0), ScreenPoint(6, 10), ScreenPoint(10, 2))
      testInside(ScreenPoint(5, 0), ScreenPoint(11, 12), ScreenPoint(6, 2), ScreenPoint(10, 10))
      testInside(ScreenPoint(15, 12), ScreenPoint(15, 0), ScreenPoint(15, 10), ScreenPoint(15, 2))
      testInside(ScreenPoint(15, 16), ScreenPoint(23, 0), ScreenPoint(18, 10), ScreenPoint(20, 6))
      testInside(ScreenPoint(3, 3), ScreenPoint(21, 3), ScreenPoint(4, 3), ScreenPoint(20, 3))

      testInside(ScreenPoint(9, 6), ScreenPoint(10, 7), ScreenPoint(9, 6), ScreenPoint(10, 7))

      testOutside(ScreenPoint(0, 20), ScreenPoint(25, 20))
      testOutside(ScreenPoint(0, 20), ScreenPoint(0, 0))
    }
  }
}
