package info.td.scalaplot

import org.specs2._

class DataRangeSpec extends Specification {
  private[this] def isCloseTo(range: DataRange, expectedResult: DataRange) = {
    val result = range.expandToNearestTenth
    val delta = 1e-10
    (result.minimum must beCloseTo(expectedResult.minimum, delta)) and
      (result.maximum must beCloseTo(expectedResult.maximum, delta))
  }

  def is = "DataRange should" ^
    "expandToNearestTenth" ! {
      isCloseTo(DataRange(-1.11, 1.11), DataRange(-1.2, 1.2)) and
        isCloseTo(DataRange(1.15, -1.15), DataRange(1.2, -1.2)) and
        isCloseTo(DataRange(-11.9, 11.9), DataRange(-12, 12)) and
        isCloseTo(DataRange(200.1, 300.1), DataRange(200, 310))
    }
}