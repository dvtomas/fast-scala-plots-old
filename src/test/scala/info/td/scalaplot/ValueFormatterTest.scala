package info.td.scalaplot

import java.util.Locale

import info.td.scalaplot.axispainter.AxisPaintingProperties
import org.specs2._

class ValueFormatterTest extends Specification {
  implicit val valueFormatterLocale = ValueFormatterLocale(Locale.US)

  private def valueString(value: Double, digits: Int = 4) = (new AxisPaintingProperties).valueString(value, digits)

  def is =
    "ValueFormatter must properly format valueString" ^
      "zero is whole" ! {
        valueString(0) must beEqualTo("0")
      } ^ "very small number is in scientific notation" ! {
      (valueString(0.0001234) must beEqualTo("1.234E-04")) and
      (valueString(0.0001234, 1) must beEqualTo("1E-04")) and
        (valueString(0.01234) must beEqualTo("0.01234")) and
        (valueString(-0.01234, 1) must beEqualTo("-0.01")) and
        (valueString(0.1234, 3) must beEqualTo("0.123"))
    } ^ "mid-range numbers are in human notation" ! {
      (valueString(0.1234) must beEqualTo("0.1234")) and
      (valueString(-0.1234) must beEqualTo("-0.1234")) and
      (valueString(-0.1234, 3) must beEqualTo("-0.123")) and
        (valueString(-1) must beEqualTo("-1.000")) and
        (valueString(1) must beEqualTo("1.000")) and
        (valueString(123.4) must beEqualTo("123.4")) and
        (valueString(1234) must beEqualTo("1234")) and
        (valueString(12340) must beEqualTo("12340")) and
        (valueString(12340, 1) must beEqualTo("12340"))
    } ^ "big numbers are in scientific notation" ! {
      (valueString(123400) must beEqualTo("1.234E+05")) and
        (valueString(-123400) must beEqualTo("-1.234E+05"))
    }
}
