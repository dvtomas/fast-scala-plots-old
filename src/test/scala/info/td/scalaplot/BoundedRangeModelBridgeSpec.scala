package info.td.scalaplot

import info.td.common.rx.RxHelpers._
import javax.swing.DefaultBoundedRangeModel
import javax.swing.event.ChangeEvent
import org.specs2._
import org.specs2.specification.core.SpecStructure
import rx.lang.scala.subjects.{BehaviorSubject, ReplaySubject}

// This test seems to be non-deterministic for some reason..
class BoundedRangeModelBridgeSpec extends Specification {

  private class TestHelpersAscending {
    val inputVisibleRangeSource: BehaviorSubject[DataRange] = BehaviorSubject[DataRange](DataRange(30, 70))
    val totalRangeSource: BehaviorSubject[DataRange] = BehaviorSubject[DataRange](DataRange(10, 110))
    val bridge: HorizontalBoundedRangeModelBridge = new HorizontalBoundedRangeModelBridge(inputVisibleRangeSource, totalRangeSource) {
      override protected val scrollBarResolution: DataRange = DataRange(0, 1000)
    }
    val range: DefaultBoundedRangeModel = bridge.boundedRangeModel
    val outputVisibleRangeSource: ReplaySubject[DataRange] = bridge.outputVisibleRangeSource.toReplaySubject
    outputVisibleRangeSource.doOnNext(inputVisibleRangeSource.onNext) // Simulate that the input is connected to the output
    var stateChanges = 0
    range.addChangeListener((_: ChangeEvent) => {
      stateChanges += 1
    })
  }

  private class TestHelpersDescending {
    val inputVisibleRangeSource: BehaviorSubject[DataRange] = BehaviorSubject[DataRange](DataRange(70, 30))
    val totalRangeSource: BehaviorSubject[DataRange] = BehaviorSubject[DataRange](DataRange(110, 10))
    val bridge: HorizontalBoundedRangeModelBridge = new HorizontalBoundedRangeModelBridge(inputVisibleRangeSource, totalRangeSource) {
      override protected val scrollBarResolution: DataRange = DataRange(0, 1000)
    }
    val range: DefaultBoundedRangeModel = bridge.boundedRangeModel
    val outputVisibleRangeSource: ReplaySubject[DataRange] = bridge.outputVisibleRangeSource.toReplaySubject
    var stateChanges = 0
    range.addChangeListener((_: ChangeEvent) => {
      stateChanges += 1
    })
  }

  def is: SpecStructure = "VisibleRangeToBoundedRangeBridge should" ^
    "for ascending range: " ^
    "bounded data range bridge is always the same" ! {
      val helpers = new TestHelpersAscending
      val range1 = helpers.bridge.boundedRangeModel
      val range2 = helpers.bridge.boundedRangeModel
      range1 must be(range2)
    } ^ "bounds must be correctly computed" ! {
    val helpers = new TestHelpersAscending
    val range = helpers.range
    (range.getMinimum must beEqualTo(0)) and
      (range.getMaximum must beEqualTo(1000)) and
      (range.getValue must beEqualTo(200)) and
      (range.getExtent must beEqualTo(400)) and
      (helpers.stateChanges must beEqualTo(0))
  } ^ "bounds must be correctly recomputed after change to visible range" ! {
    val helpers = new TestHelpersAscending
    val keepMeImJustTestingThatStateChangesStayLow = helpers.range

    helpers.inputVisibleRangeSource onNext DataRange(20, 80)
    (helpers.range.getValue must beEqualTo(100)) and
      (helpers.range.getExtent must beEqualTo(600)) and
      (helpers.stateChanges must beEqualTo(1)) and
      (keepMeImJustTestingThatStateChangesStayLow.toString != "")
  } ^ "bounds must be correctly recomputed after change to total range" ! {
    val helpers = new TestHelpersAscending
    helpers.totalRangeSource onNext DataRange(0, 100)
    (helpers.range.getValue must beEqualTo(300)) and
      (helpers.range.getExtent must beEqualTo(400)) and
      (helpers.stateChanges must beEqualTo(1))
  } ^ "Change to BoundedRange must inflict a change in visible range" ! {
    val helpers = new TestHelpersAscending
    helpers.range.setValue(500)
    (helpers.outputVisibleRangeSource.first.toBlocking.head must beEqualTo(DataRange(60, 100))) and
      (helpers.stateChanges must beEqualTo(1))
  } ^ "If the visible range is bigger than the total, we must only set the bounded range to total" ! {
    val helpers = new TestHelpersAscending
    helpers.inputVisibleRangeSource onNext DataRange(-100, 2000)
    (helpers.range.getValue must beEqualTo(0)) and
      (helpers.range.getExtent must beEqualTo(1000)) and
      (helpers.stateChanges must beEqualTo(1))
  } ^ p ^
    "descending range" ^
    "bounds must be correctly computed" ! {
      val helpers = new TestHelpersDescending
      val range = helpers.range
      (range.getMinimum must beEqualTo(0)) and
        (range.getMaximum must beEqualTo(1000)) and
        (range.getValue must beEqualTo(400)) and
        (range.getExtent must beEqualTo(400)) and
        (helpers.stateChanges must beEqualTo(0))
    } ^ "bounds must be correctly recomputed after change to visible range" ! {
    val helpers = new TestHelpersDescending
    val keepMeImJustTestingThatStateChangesStayLow = helpers.range
    keepMeImJustTestingThatStateChangesStayLow.toString

    helpers.inputVisibleRangeSource onNext DataRange(80, 20)
    (helpers.range.getValue must beEqualTo(300)) and
      (helpers.range.getExtent must beEqualTo(600)) and
      (helpers.stateChanges must beEqualTo(1))
  } ^ "bounds must be correctly recomputed after change to total range" ! {
    val helpers = new TestHelpersDescending
    helpers.totalRangeSource onNext DataRange(100, 0)
    (helpers.range.getValue must beEqualTo(300)) and
      (helpers.range.getExtent must beEqualTo(400)) and
      (helpers.stateChanges must beEqualTo(1))
  } ^ "Change to BoundedRange must inflict a change in visible range" ! {
    val helpers = new TestHelpersDescending
    helpers.range.setValue(500)
    (helpers.outputVisibleRangeSource.first.toBlocking.head must beEqualTo(DataRange(60, 20))) and
      (helpers.stateChanges must beEqualTo(1))
  } ^ "If the visible range is bigger than the total, we must only set the bounded range to total" ! {
    val helpers = new TestHelpersDescending
    helpers.inputVisibleRangeSource onNext DataRange(2000, -100)
    (helpers.range.getValue must beEqualTo(0)) and
      (helpers.range.getExtent must beEqualTo(1000)) and
      (helpers.stateChanges must beEqualTo(1))
  }
}
