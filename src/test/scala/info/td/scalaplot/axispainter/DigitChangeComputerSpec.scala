package info.td.scalaplot.axispainter

import info.td.scalaplot.utils.{DigitChange, DigitChangeComputer}
import org.specs2._
import org.specs2.specification.core.SpecStructure

class DigitChangeComputerSpec extends Specification {
  private def digitChange(a: Double, b: Double) = new DigitChangeComputer {
    override protected def maximumDigitsToCheck: Int = 3
  }.digitChange(a, b)

  private def assertDigitChange(a: Double, b: Double, expectedValue: Double, expectedMagnitudeChange: Int) = {
    digitChange(a, b) must beLike { case DigitChange(value, magnitudeChange, _) =>
      value must beCloseTo(expectedValue, Math.abs(value) / 10000.0) and (magnitudeChange must beEqualTo(expectedMagnitudeChange))
    }
  }

  private def digitChangeSpec(a: Double, b: Double, expectedValue: Double, expectedMagnitudeChange: Int) =
    s"$a, $b must change in magnitude $expectedMagnitudeChange, the breaking value is $expectedValue" ^
      ("smaller number and bigger number" ! assertDigitChange(a, b, expectedValue, expectedMagnitudeChange) ^
        "bigger number and smaller number" ! assertDigitChange(b, a, expectedValue, expectedMagnitudeChange) ^
        "negative smaller number and bigger number" ! assertDigitChange(-a, -b, -expectedValue, expectedMagnitudeChange) ^
        "negative bigger number and smaller number" ! assertDigitChange(-b, -a, -expectedValue, expectedMagnitudeChange)) ^ p

  /*private def magnitudeChangeSpec(a: Double, b: Double, expectedValue: Double, expectedMagnitudeChange: Int) =
    ("a" ! ok) ^ ("b" ! ok)
*/
  def is: SpecStructure = "MagnitudeChangeComputer should" ^
    "return zero when one value is positive and the other negative" ! {
      (digitChange(-3.5, 1.0).value must beEqualTo(0.0)) and
        (digitChange(-3.5, 1.0).value must beEqualTo(0.0)) and
        (digitChange(0.0, 0.0).value must beEqualTo(0.0))
    } ^ "return minimumOrderToCheck - 1 set in the MagnitudeChangeComputer when the values are equal" ^
    "for positive value" ! (digitChange(1.0, 1.0) must beEqualTo(DigitChange(1.0, -4, None))) ^
    "and negative value" ! (digitChange(-23.1, -23.1) must beEqualTo(DigitChange(-23.1, -4, None))) ^
    p ^
    digitChangeSpec(1.9, 2.1, 2.0, 0) ^
    digitChangeSpec(150.0, 250.0, 200.0, 2) ^
    digitChangeSpec(0.015, 0.025, 0.02, -2) ^
    digitChangeSpec(1234.5, 1244.5, 1240.0, 1) ^
    digitChangeSpec(0.12345, 0.12445, 0.1240, -3) ^
    digitChangeSpec(0.123456789, 0.123456889, 0.123, -4) ^ // currentExponent smaller than minimum indicates that the order change has not been found
    digitChangeSpec(1.23e100, 1.33e100, 1.3e100, 99) ^
    digitChangeSpec(1.23e-100, 1.33e-100, 1.3e-100, -101) ^
    digitChangeSpec(1.835e-100, 1.845e-100, 1.84e-100, -102) ^
    digitChangeSpec(7.012, 7.022, 7.02, -2) // must not get confused by zeroes in the middle of the number
}