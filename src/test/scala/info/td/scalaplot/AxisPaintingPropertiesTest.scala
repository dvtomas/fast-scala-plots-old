package info.td.scalaplot

import info.td.scalaplot.axispainter.LinearAxisXPainter
import org.specs2._
import org.specs2.specification.core.SpecStructure

class AxisPaintingPropertiesTest extends Specification {

  private case class PositionOfDigitThatChangesHelper(first: Double, step: Double) extends LinearAxisXPainter(null) {
    def positionOfDigitThatChanges: Int = TicksPlacement(first, first + step, step).positionOfDigitThatChanges
  }

  private def positionOfDigitThatChanges(first: Double, step: Double) = PositionOfDigitThatChangesHelper(first, step).positionOfDigitThatChanges

  def is: SpecStructure =
    "AxisPaintingProperties must properly compute positionOfDigitThatChanges" ! {
      (positionOfDigitThatChanges(1000, 1) should beEqualTo(4)) and
        (positionOfDigitThatChanges(1000, 0.1) should beEqualTo(5)) and
        (positionOfDigitThatChanges(-1000, 0.1) should beEqualTo(5)) and
        (positionOfDigitThatChanges(-1000, 99) should beEqualTo(3)) and
        (positionOfDigitThatChanges(-1000, 100) should beEqualTo(2)) and
        (positionOfDigitThatChanges(-1000, 110) should beEqualTo(2))
    }
}
