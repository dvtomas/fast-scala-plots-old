
 - preparing the Height field plot. In order to achieve this, a lot of common functionality has been factored out from SnakePlotPart to EquidistantRange. Some more tests have been written for EquidistantRange.
 - A lot of work has been done on supporting axes with descending data ranges. E.g Snake plots can now have data range 1000 to 100 by -1.0, and an axis can show data range 200 to 150, and everythin should work OK: rendering, scrollbars, trampolines, picking etc..
