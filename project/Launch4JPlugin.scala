import SbtProguard.ProguardKeys
import sbt.Keys._
import sbt._

object Launch4JPlugin extends Plugin {
  val launch4jDirectory = settingKey[File]("Directory, where the launch4j result will be placed")
  val launch4jPrefix = settingKey[String]("Prefix of the resulting launch4j directory")
  val filesToCopy = settingKey[Seq[File]]("Directories to be copied to the result directory")
  val launch4j = taskKey[Unit]("Runs Launch4J to make an executable for the Skokan")

  private def launch4JConfig(name: String, version: String, mainClass: String, jarFile: String, targetDir: File, baseDir: File) = {
    // versionForWindows ends with .1 for SNAPSHOT versions, and with .0 for non-snapshot versions.
    val versionForWindows = if (version endsWith "-SNAPSHOT") (version dropRight "-SNAPSHOT".length) + ".1" else version + ".0"
    s"""
      |<launch4jConfig>
      |  <dontWrapJar>true</dontWrapJar>
      |  <headerType>console</headerType>
      |  <jar>$jarFile</jar>
      |  <outfile>${targetDir / name}-$version.exe</outfile>
      |  <errTitle></errTitle>
      |  <cmdLine></cmdLine>
      |  <chdir></chdir>
      |  <priority>normal</priority>
      |  <downloadUrl>http://java.com/download</downloadUrl>
      |  <supportUrl></supportUrl>
      |  <stayAlive>false</stayAlive>
      |  <manifest></manifest>
      |  <icon>$baseDir/src/main/resources/icons/$name.ico</icon>
      |  <classPath>
      |    <mainClass>${mainClass}</mainClass>
      |  </classPath>
      |  <singleInstance>
      |    <mutexName>launch4jmutex_${mainClass}</mutexName>
      |  </singleInstance>
      |  <jre>
      |    <path></path>
      |    <bundledJre64Bit>false</bundledJre64Bit>
      |    <minVersion>1.8.0</minVersion>
      |    <maxVersion></maxVersion>
      |    <jdkPreference>preferJre</jdkPreference>
      |    <opt>-Dlogback.configurationFile=config/logback.xml</opt>
      |  </jre>
      |  <versionInfo>
      |    <productName>${name}</productName>
      |    <internalName>${name}</internalName>
      |    <originalFilename>${name}.exe</originalFilename>
      |    <fileVersion>$versionForWindows</fileVersion>
      |    <txtFileVersion>${version}</txtFileVersion>
      |    <productVersion>$versionForWindows</productVersion>
      |    <txtProductVersion>${version}</txtProductVersion>
      |    <fileDescription>${name}</fileDescription>
      |    <companyName>MEAS Prog s.r.o.</companyName>
      |    <copyright>MEAS Prog s.r.o.</copyright>
      |  </versionInfo>
      |</launch4jConfig>
      |      """.stripMargin
  }

  case class ConfigTemplateParameters(name: String, mainClass: String)

  def launch4jTask(configs: Seq[ConfigTemplateParameters]) =
    (ProguardKeys.proguard in SbtProguard.Proguard, version, launch4jDirectory, launch4jPrefix, filesToCopy, baseDirectory in packageBin, baseDirectory, streams) map {
      (jarFiles, theVersion, targetDirWithoutVersion, prefix, _filesToCopy, packageDir, baseDir, s) => {
        val jarFile = jarFiles.head
        val finalJarName = s"${prefix}-${theVersion}.jar"
        val targetDir = targetDirWithoutVersion / s"$prefix-$theVersion"

        for (config <- configs) {
          IO.createDirectory(targetDir)
          val configFile = targetDir / s"launch4j-${config.name}.xml"
          val configContents = launch4JConfig(config.name, theVersion, config.mainClass, finalJarName, targetDir, baseDir)
          s.log.info(s"Creating launch4j config: $configFile")
          IO.write(configFile, configContents)
          val exitCode = Process("launch4j" :: configFile.getAbsolutePath :: Nil, Some(targetDir)) ! s.log
          if (exitCode != 0) {
            s.log.error("Launch4j failed with exit code [%s]" format exitCode)
          } else {
            val referenceConfigFile = targetDirWithoutVersion / configFile.getName
            configFile renameTo referenceConfigFile
            s.log.info(s"Successfully ran launch4j. Launch4J config file moved to $referenceConfigFile.")
          }
        }

        IO.copyFile(jarFile, targetDir / finalJarName)

        _filesToCopy foreach { file => {
          if (file.isDirectory) {
            IO.copyDirectory(file, targetDir / file.getName, overwrite = true)
          } else if (file.isFile) {
            IO.copyFile(file, targetDir)
          } else {
            s.log.error(s"Cannot copy %file, not a file or directory")
          }
        }
        }
        s.log.info(s"Deployed result stored in ${targetDir}")
      }
    }

}
