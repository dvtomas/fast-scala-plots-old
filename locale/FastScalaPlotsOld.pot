# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-06-08 07:33+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/main/scala/info/td/scalaplot/plotcontroller/AutoCADPlotController.scala:207
msgid "Toggle legend"
msgstr ""

#: src/main/scala/info/td/scalaplot/plotcontroller/AutoCADPlotController.scala:214
msgid "Fit graph"
msgstr ""

#: src/main/scala/info/td/scalaplot/plotcontroller/AutoCADPlotController.scala:215
msgid "Fit graph along Y axis"
msgstr ""

#: src/main/scala/info/td/scalaplot/plotcontroller/AutoCADPlotController.scala:216
msgid "Undo zoom"
msgstr ""

#: src/main/scala/info/td/scalaplot/utils/ErrorDialog.scala:12
msgid "error"
msgstr ""

#: src/main/scala/info/td/scalaplot/utils/FileDialogUtils.scala:17
msgid "Image files"
msgstr ""

#: src/main/scala/info/td/scalaplot/utils/FileDialogUtils.scala:31
#: src/main/scala/info/td/scalaplot/utils/FileDialogUtils.scala:35
msgid "Error saving image"
msgstr ""

#: src/main/scala/info/td/scalaplot/figure/Figure.scala:79
msgid "Antialiased rendering"
msgstr ""

#: src/main/scala/info/td/scalaplot/figure/FigureController.scala:68
msgid "Copy to clipboard"
msgstr ""

#: src/main/scala/info/td/scalaplot/figure/FigureController.scala:73
msgid "Save image to file..."
msgstr ""

#: src/main/scala/info/td/scalaplot/figure/FigureControllerTouchDevice.scala:45
msgid "Drag"
msgstr ""

#: src/main/scala/info/td/scalaplot/figure/FigureControllerTouchDevice.scala:51
msgid "Zoom rectangle"
msgstr ""

#: src/main/scala/info/td/scalaplot/figure/FigureControllerTouchDevice.scala:57
msgid "Zoom in"
msgstr ""

#: src/main/scala/info/td/scalaplot/figure/FigureControllerTouchDevice.scala:63
msgid "Zoom in horizontal axis"
msgstr ""

#: src/main/scala/info/td/scalaplot/figure/FigureControllerTouchDevice.scala:69
msgid "Zoom in vertical axis"
msgstr ""

#: src/main/scala/info/td/scalaplot/figure/FigureControllerTouchDevice.scala:75
msgid "Zoom out"
msgstr ""

#: src/main/scala/info/td/scalaplot/figure/FigureControllerTouchDevice.scala:81
msgid "Zoom out horizontal axis"
msgstr ""

#: src/main/scala/info/td/scalaplot/figure/FigureControllerTouchDevice.scala:87
msgid "Zoom out vertical axis"
msgstr ""

#: src/main/scala/info/td/scalaplot/figure/FigureControllerTouchDevice.scala:165
msgid "Active mode"
msgstr ""

#: src/main/scala/info/td/scalaplot/figure/PickInfoSettings.scala:10
msgid "Draw picker"
msgstr ""

#: src/main/scala/info/td/scalaplot/figure/PickInfoSettings.scala:12
msgid "Draw picker position"
msgstr ""

#: src/main/scala/info/td/scalaplot/figure/PickInfoSettings.scala:14
msgid "Highlight point"
msgstr ""

#: src/main/scala/info/td/scalaplot/figure/PickInfoSettings.scala:16
msgid "Draw pick value"
msgstr ""

#: src/main/scala/info/td/scalaplot/figure/PickInfoSettings.scala:18
msgid "Also display X value"
msgstr ""

#: src/main/scala/info/td/scalaplot/figure/PickInfoSettings.scala:45
msgid "Pick info"
msgstr ""
