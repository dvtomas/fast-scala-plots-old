#!/usr/bin/python3
# Generates the *.pot files

import glob
import subprocess


basename='FastScalaPlotsOld'
find_globs = [f'../src/main/scala/**/*.scala']
print(f"Generating {basename} from {find_globs}")
input_files = []
for find_glob in find_globs:
	len_before = len(input_files)
	input_files = input_files + glob.glob(find_glob, recursive=True)
	if len(input_files) == len_before:
		print(f"WARNING: no files were added for {find_glob}")

subprocess.run(
	[
		'xgettext',
		'--foreign-user',
		'--msgid-bugs-address=kzv-devel@gmail.com',
		'--copyright-holder=KZV s.r.o.',
		f'--package-name={basename}',
		'--force-po',
		'--no-wrap',
		'--from-code=utf-8',
		'--add-comments=TRANSLATORS',
		'-kt', '-kmark_t', '-ktf', '-ktc:1c,2', '-ktcf:1c,2',
		'-o', f'{basename}.pot',
	] + input_files,
	capture_output=True,
	check=True
)

print("Success, exiting")
