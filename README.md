# Fast scala plots old

This library features a Swing Component for plotting data completely implemented in the Scala programming language. Primarily, it is being developed for a commercial automotive measuring and automation project, but I have decided to also publish it for a wider audience. The library has been successfully used in production in several measurement applications.

There was an attempt to rewrite this library using FRP approach (using the Monix library), for the status please see https://bitbucket.org/dvtomas/fast-scala-plots. However, with the tragic death of my boss, the future of both this and the new version of library is uncertain. If you want to follow up with the development, I'll be happy to help, as I think this is currently the best library for realtime drawing of simple plots.

The examples require Java 8 to run. Grab the [latest examples in the Downloads section](https://bitbucket.org/dvtomas/fast-scala-plots-old/downloads). The archive contains windows executable wrapper and a linux shell script to run the demo.

## Design goals and features

 - Allow fast display of growing data (new data are acquired in real-time and the user wants to see them ASAP) even on slower (embedded) machines.
 - Focus mainly on line plots and height maps. Don't add dozens of different graph types (yet), keep it simple. For line plots, the library should support XY plots and function plots with equidistant X step (aka _snakes_). 
 - At least for snakes, allow display of very large data (millions of points).
 - Look nice.
 - Convenient user interface for both desktop computers and touch devices.
 - Proper support for localization, right now the library is localized to English and Czech.
 - Low memory footprint. Just use getters to retrieve the points, don't force the user to wrap the points into some specific structure.

## Main features

 - Executable demo
 - XY graphs with configurable point/line styles
 - Speed optimized function plots with equidistant X step (_snakes_)
 - Height map
 - The swing Component, called Figure, can contain multiple plots
 - Mouse control: LMB draws a zoom rectangle, MMB drags, RMB pops up a menu, wheel zooms in/out
 - Touch device control: Toolbar buttons change mode of operation (zoom, drag, pick, ...)
 - Vertical/XY picker of data
 - Legend
 - Swing scroll-bars navigation support
 - Trampoline control (see demo) for moving in very long graphs
 - User can choose between antialiased rendering and high performance rendering.
 - Speed optimized and memory-canny. All point collections are virtual, so memory use can be very low. I have seen plotting libraries where each point had to be an instance of Point2D(x, y) **and** a Publisher, that is definitely not the case of this library.
 - Copy image to clipboard, save image to file features
 - Fit to screen, Fit Y to screen features
 - Linear/Logarithmic/Mirrored logarithmic axes

**Note**: The snakes only work for left-to-right (or right-to-left) direction. They cannot be rendered vertically now. The height-maps are also optimized towards horizontal scrolling.

## What will probably not be implemented

 - Polar axes/picker
 - Allow several plots in a figure to overlap (but you can have several lines/points/etc in one plot, don't worry)

